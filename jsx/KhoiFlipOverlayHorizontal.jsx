#target photoshop
//
// KhoiFlipOverlayHorizontal.jsx
//

//
// Generated Thu Aug 15 2019 00:48:47 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Khoi Flip Overlay Horizontal ==============
//
$._ext_khoi04={
run : function KhoiFlipOverlayHorizontal() {
  // Flip
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('Axis'), cTID('Ornt'), cTID('Hrzn'));
    executeAction(cTID('Flip'), desc1, dialogMode);
  };

  step1();      // Flip
},
};



//=========================================
//                    KhoiFlipOverlayHorizontal.main
//=========================================
//

//KhoiFlipOverlayHorizontal.main = function () {
  //KhoiFlipOverlayHorizontal();
//};

//KhoiFlipOverlayHorizontal.main();

// EOF

//"KhoiFlipOverlayHorizontal.jsx"
// EOF
