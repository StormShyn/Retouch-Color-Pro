#target photoshop
//
// LIGHT5.jsx
//

//
// Generated Wed Jul 31 2019 00:33:06 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== LIGHT 5 ==============
//
$._ext_E010={
run : function LIGHT5() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "ROSEWOOD I");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Scrn'));
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), 75.96);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Dmnd'));
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 397);
    var desc4 = new ActionDescriptor();
    desc4.putString(cTID('Nm  '), "ROSEWOOD I");
    desc4.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc4.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc5 = new ActionDescriptor();
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Rd  '), 247.996215820313);
    desc6.putDouble(cTID('Grn '), 199.996948242188);
    desc6.putDouble(cTID('Bl  '), 185.997161865234);
    desc5.putObject(cTID('Clr '), sTID("RGBColor"), desc6);
    desc5.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc5.putInteger(cTID('Lctn'), 0);
    desc5.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc5);
    var desc7 = new ActionDescriptor();
    var desc8 = new ActionDescriptor();
    desc8.putDouble(cTID('Rd  '), 249.000091552734);
    desc8.putDouble(cTID('Grn '), 137.997894287109);
    desc8.putDouble(cTID('Bl  '), 143.997802734375);
    desc7.putObject(cTID('Clr '), sTID("RGBColor"), desc8);
    desc7.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc7.putInteger(cTID('Lctn'), 340);
    desc7.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc7);
    var desc9 = new ActionDescriptor();
    var desc10 = new ActionDescriptor();
    desc10.putDouble(cTID('Rd  '), 213.000640869141);
    desc10.putDouble(cTID('Grn '), 67.0028686523438);
    desc10.putDouble(cTID('Bl  '), 77.9988098144531);
    desc9.putObject(cTID('Clr '), sTID("RGBColor"), desc10);
    desc9.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc9.putInteger(cTID('Lctn'), 4096);
    desc9.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc9);
    desc4.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc11 = new ActionDescriptor();
    desc11.putUnitDouble(cTID('Opct'), cTID('#Prc'), 5.88235294117647);
    desc11.putInteger(cTID('Lctn'), 0);
    desc11.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc11);
    var desc12 = new ActionDescriptor();
    desc12.putUnitDouble(cTID('Opct'), cTID('#Prc'), 90.1960784313726);
    desc12.putInteger(cTID('Lctn'), 4096);
    desc12.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc12);
    desc4.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc4);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "ROSEWOOD II");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('HrdL'));
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), -23.96);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Rflc'));
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 90);
    var desc4 = new ActionDescriptor();
    desc4.putString(cTID('Nm  '), "ROSEWOOD II");
    desc4.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc4.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc5 = new ActionDescriptor();
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Rd  '), 249.000091552734);
    desc6.putDouble(cTID('Grn '), 137.997894287109);
    desc6.putDouble(cTID('Bl  '), 143.997802734375);
    desc5.putObject(cTID('Clr '), sTID("RGBColor"), desc6);
    desc5.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc5.putInteger(cTID('Lctn'), 0);
    desc5.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc5);
    var desc7 = new ActionDescriptor();
    var desc8 = new ActionDescriptor();
    desc8.putDouble(cTID('Rd  '), 243.000183105469);
    desc8.putDouble(cTID('Grn '), 219.000549316406);
    desc8.putDouble(cTID('Bl  '), 205.996856689453);
    desc7.putObject(cTID('Clr '), sTID("RGBColor"), desc8);
    desc7.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc7.putInteger(cTID('Lctn'), 1415);
    desc7.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc7);
    var desc9 = new ActionDescriptor();
    var desc10 = new ActionDescriptor();
    desc10.putDouble(cTID('Rd  '), 230.000001490116);
    desc10.putDouble(cTID('Grn '), 150.626455843449);
    desc10.putDouble(cTID('Bl  '), 157.264593243599);
    desc9.putObject(cTID('Clr '), sTID("RGBColor"), desc10);
    desc9.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc9.putInteger(cTID('Lctn'), 4096);
    desc9.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc9);
    desc4.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc11 = new ActionDescriptor();
    desc11.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc11.putInteger(cTID('Lctn'), 0);
    desc11.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc11);
    var desc12 = new ActionDescriptor();
    desc12.putUnitDouble(cTID('Opct'), cTID('#Prc'), 90.1960784313726);
    desc12.putInteger(cTID('Lctn'), 4096);
    desc12.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc12);
    desc4.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc4);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Select
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "ROSEWOOD I");
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(sTID("selectionModifier"), sTID("selectionModifierType"), sTID("addToSelectionContinuous"));
    desc1.putBoolean(cTID('MkVs'), false);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Make
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("layerSection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('From'), ref2);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Rosewood");
    desc1.putObject(cTID('Usng'), sTID("layerSection"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putClass(cTID('Nw  '), cTID('Chnl'));
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Msk '));
    desc1.putReference(cTID('At  '), ref1);
    desc1.putEnumerated(cTID('Usng'), cTID('UsrM'), cTID('RvlA'));
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  step1();      // Make
  step2();      // Make
  step3();      // Select
  step4();      // Make
  step5();      // Make
},
};



//=========================================
//                    LIGHT5.main
//=========================================
//

//LIGHT5.main = function () {
  //LIGHT5();
//};

//LIGHT5.main();

// EOF

//"LIGHT5.jsx"
// EOF
