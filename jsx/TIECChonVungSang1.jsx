#target photoshop
//
// ChonVungSang1.jsx
//

//
// Generated Mon Aug 05 2019 20:15:57 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Chon Vung Sang 1 ==============
//
$._ext_TTC017={
run : function ChonVungSang1() {
  // Set
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), sTID("RGB"));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Set
},
};



//=========================================
//                    ChonVungSang1.main
//=========================================
//

//ChonVungSang1.main = function () {
 // ChonVungSang1();
//};

//ChonVungSang1.main();

// EOF

//"ChonVungSang1.jsx"
// EOF
