﻿#target photoshop
//
// MuaRoiTuDong.jsx
//

//
// Generated Sun Aug 11 2019 18:15:34 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Mua Roi Tu Dong ==============
//
$._ext_mua00={
run : function MuaRoiTuDong() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('Lyr '));
    desc1.putReference(cTID('null'), ref1);
    desc1.putInteger(cTID('LyrI'), 3);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Set
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Mưa Rơi");
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Fill
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Usng'), cTID('FlCn'), cTID('Blck'));
    desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    executeAction(cTID('Fl  '), desc1, dialogMode);
  };

  // Add Noise
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Dstr'), cTID('Dstr'), cTID('Unfr'));
    desc1.putUnitDouble(cTID('Nose'), cTID('#Prc'), 123.48);
    desc1.putBoolean(cTID('Mnch'), true);
    desc1.putInteger(cTID('FlRs'), 1749682);
    executeAction(sTID('addNoise'), desc1, dialogMode);
  };

  // Motion Blur
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Angl'), 79);
    desc1.putUnitDouble(cTID('Dstn'), cTID('#Pxl'), 100);
    executeAction(sTID('motionBlur'), desc1, dialogMode);
  };

  // Levels
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    list2.putInteger(87);
    list2.putInteger(255);
    desc2.putList(cTID('Inpt'), list2);
    desc2.putDouble(cTID('Gmm '), 5.76);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Transform
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('FTcs'), cTID('QCSt'), sTID("QCSAverage"));
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 38);
    desc2.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 16);
    desc1.putObject(cTID('Ofst'), cTID('Ofst'), desc2);
    desc1.putUnitDouble(cTID('Wdth'), cTID('#Prc'), 105.324074074074);
    desc1.putUnitDouble(cTID('Hght'), cTID('#Prc'), 105.787037037037);
    desc1.putEnumerated(cTID('Intr'), cTID('Intp'), cTID('Bcbc'));
    executeAction(cTID('Trnf'), desc1, dialogMode);
  };

  // Set
  function step8(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Scrn'));
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Make
  function step9(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putBoolean(sTID("useLegacy"), false);
    desc2.putObject(cTID('Type'), cTID('BrgC'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Set
  function step10(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('AdjL'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putInteger(cTID('Brgh'), -48);
    desc2.putInteger(cTID('Cntr'), 100);
    desc2.putBoolean(sTID("useLegacy"), false);
    desc1.putObject(cTID('T   '), cTID('BrgC'), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Make
  function step11(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindDefault"));
    desc3.putBoolean(cTID('Clrz'), false);
    desc2.putObject(cTID('Type'), cTID('HStr'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Set
  function step12(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('AdjL'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    var list1 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putInteger(cTID('H   '), 0);
    desc3.putInteger(cTID('Strt'), -42);
    desc3.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc3);
    desc2.putList(cTID('Adjs'), list1);
    desc1.putObject(cTID('T   '), cTID('HStr'), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Select
  function step13(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Mưa Rơi");
    desc1.putReference(cTID('null'), ref1);
    desc1.putBoolean(cTID('MkVs'), false);
    var list1 = new ActionList();
    list1.putInteger(3);
    desc1.putList(cTID('LyrI'), list1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Convert to Smart Object
  function step14(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('newPlacedLayer'), undefined, dialogMode);
  };

  // Gaussian Blur
  function step15(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 1.5);
    executeAction(sTID('gaussianBlur'), desc1, dialogMode);
  };

  // Layer Via Copy
  function step16(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('copyToLayer'), undefined, dialogMode);
  };

  // Set
  function step17(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Transform
  function step18(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('FTcs'), cTID('QCSt'), sTID("QCSAverage"));
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 0.02369008736423);
    desc2.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), -0.10062608118596);
    desc1.putObject(cTID('Ofst'), cTID('Ofst'), desc2);
    desc1.putUnitDouble(cTID('Wdth'), cTID('#Prc'), 100.001194325705);
    desc1.putUnitDouble(cTID('Hght'), cTID('#Prc'), 99.9989208612192);
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("warpStyle"), sTID("warpStyle"), sTID("warpCustom"));
    desc3.putDouble(sTID("warpValue"), 0);
    desc3.putDouble(sTID("warpPerspective"), 0);
    desc3.putDouble(sTID("warpPerspectiveOther"), 0);
    desc3.putEnumerated(sTID("warpRotate"), cTID('Ornt'), cTID('Hrzn'));
    var desc4 = new ActionDescriptor();
    desc4.putUnitDouble(cTID('Top '), cTID('#Pxl'), -84);
    desc4.putUnitDouble(cTID('Left'), cTID('#Pxl'), -100);
    desc4.putUnitDouble(cTID('Btom'), cTID('#Pxl'), 3572);
    desc4.putUnitDouble(cTID('Rght'), cTID('#Pxl'), 5360);
    desc3.putObject(sTID("bounds"), cTID('Rctn'), desc4);
    desc3.putInteger(sTID("uOrder"), 4);
    desc3.putInteger(sTID("vOrder"), 4);
    var desc5 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc6 = new ActionDescriptor();
    desc6.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), -100);
    desc6.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), -84);
    list1.putObject(sTID("rationalPoint"), desc6);
    var desc7 = new ActionDescriptor();
    desc7.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 1446.26209486219);
    desc7.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), -139.919802236384);
    list1.putObject(sTID("rationalPoint"), desc7);
    var desc8 = new ActionDescriptor();
    desc8.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 3480.6598142656);
    desc8.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), -95.7776540127506);
    list1.putObject(sTID("rationalPoint"), desc8);
    var desc9 = new ActionDescriptor();
    desc9.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 5360);
    desc9.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), -84);
    list1.putObject(sTID("rationalPoint"), desc9);
    var desc10 = new ActionDescriptor();
    desc10.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), -100);
    desc10.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 1134.66666666667);
    list1.putObject(sTID("rationalPoint"), desc10);
    var desc11 = new ActionDescriptor();
    desc11.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), -1489.98267146093);
    desc11.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 908.493516339338);
    list1.putObject(sTID("rationalPoint"), desc11);
    var desc12 = new ActionDescriptor();
    desc12.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 5323.90446678294);
    desc12.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 1540.32309314666);
    list1.putObject(sTID("rationalPoint"), desc12);
    var desc13 = new ActionDescriptor();
    desc13.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 5378.9352912862);
    desc13.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 1134.16012841675);
    list1.putObject(sTID("rationalPoint"), desc13);
    var desc14 = new ActionDescriptor();
    desc14.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), -99.9999999999997);
    desc14.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 2353.33333333333);
    list1.putObject(sTID("rationalPoint"), desc14);
    var desc15 = new ActionDescriptor();
    desc15.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 1451.25177141118);
    desc15.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 2987.28708432469);
    list1.putObject(sTID("rationalPoint"), desc15);
    var desc16 = new ActionDescriptor();
    desc16.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 4212.49357616134);
    desc16.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 3247.51339186467);
    list1.putObject(sTID("rationalPoint"), desc16);
    var desc17 = new ActionDescriptor();
    desc17.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 5148.59319139623);
    desc17.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 2361.16120281139);
    list1.putObject(sTID("rationalPoint"), desc17);
    var desc18 = new ActionDescriptor();
    desc18.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), -100);
    desc18.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 3572);
    list1.putObject(sTID("rationalPoint"), desc18);
    var desc19 = new ActionDescriptor();
    desc19.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 1668.01943713533);
    desc19.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 3573.85302635997);
    list1.putObject(sTID("rationalPoint"), desc19);
    var desc20 = new ActionDescriptor();
    desc20.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 1460.77748541335);
    desc20.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 3646.12105439875);
    list1.putObject(sTID("rationalPoint"), desc20);
    var desc21 = new ActionDescriptor();
    desc21.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 5205.65498398546);
    desc21.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 3577.50216018148);
    list1.putObject(sTID("rationalPoint"), desc21);
    desc5.putList(sTID("meshPoints"), list1);
    desc3.putObject(sTID("customEnvelopeWarp"), sTID("customEnvelopeWarp"), desc5);
    desc1.putObject(cTID('warp'), cTID('warp'), desc3);
    executeAction(cTID('Trnf'), desc1, dialogMode);
  };

  // Hide
  function step19(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    list1.putReference(ref1);
    desc1.putList(cTID('null'), list1);
    executeAction(cTID('Hd  '), desc1, dialogMode);
  };

  // Select
  function step20(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Hue/Saturation 1");
    desc1.putReference(cTID('null'), ref1);
    desc1.putBoolean(cTID('MkVs'), false);
    var list1 = new ActionList();
    list1.putInteger(5);
    desc1.putList(cTID('LyrI'), list1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Select
  function step21(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Mưa Rơi");
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(sTID("selectionModifier"), sTID("selectionModifierType"), sTID("addToSelectionContinuous"));
    desc1.putBoolean(cTID('MkVs'), false);
    var list1 = new ActionList();
    list1.putInteger(6);
    list1.putInteger(7);
    list1.putInteger(4);
    list1.putInteger(5);
    desc1.putList(cTID('LyrI'), list1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Make
  function step22(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("layerSection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('From'), ref2);
    desc1.putInteger(sTID("layerSectionStart"), 8);
    desc1.putInteger(sTID("layerSectionEnd"), 9);
    desc1.putString(cTID('Nm  '), "Group 1");
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Set
  function step23(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Rain");
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Make
  function step24(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putClass(cTID('Nw  '), cTID('Chnl'));
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Msk '));
    desc1.putReference(cTID('At  '), ref1);
    desc1.putEnumerated(cTID('Usng'), cTID('UsrM'), cTID('RvlA'));
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Select
  function step25(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('PbTl'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  step1();      // Make
  step2();      // Set
  step3();      // Fill
  step4();      // Add Noise
  step5();      // Motion Blur
  step6();      // Levels
  step7();      // Transform
  step8();      // Set
  step9();      // Make
  step10();      // Set
  step11();      // Make
  step12();      // Set
  step13();      // Select
  step14();      // Convert to Smart Object
  step15();      // Gaussian Blur
  step16();      // Layer Via Copy
  step17();      // Set
  step18();      // Transform
  step19();      // Hide
  step20();      // Select
  step21();      // Select
  step22();      // Make
  step23();      // Set
  step24();      // Make
  step25();      // Select
},
};



//=========================================
//                    MuaRoiTuDong.main
//=========================================
//

//MuaRoiTuDong.main = function () {
 // MuaRoiTuDong();
//};

//MuaRoiTuDong.main();

// EOF

//"MuaRoiTuDong.jsx"
// EOF
