#target photoshop
//
// AtheGiamBongMoAnh.jsx
//

//
// Generated Tue Aug 13 2019 19:12:48 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Giam Bong Mo Anh ==============
//
$._ext_ATN04={
run : function GiamBongMoAnh() {
  // Color Range
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 39);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 26.1);
    desc2.putDouble(cTID('A   '), -4.73);
    desc2.putDouble(cTID('B   '), -46.44);
    desc1.putObject(cTID('Mnm '), cTID('LbCl'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Lmnc'), 38.6);
    desc3.putDouble(cTID('A   '), 1);
    desc3.putDouble(cTID('B   '), -27.64);
    desc1.putObject(cTID('Mxm '), cTID('LbCl'), desc3);
    desc1.putInteger(sTID("colorModel"), 0);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Feather
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 1.59999084472656);
    desc1.putBoolean(sTID("selectionModifyEffectAtCanvasBounds"), false);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Average
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Avrg'), undefined, dialogMode);
  };

  // Fade
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 75);
    desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    executeAction(cTID('Fade'), desc1, dialogMode);
  };

  // Set
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1(true, true);      // Color Range
  step2();      // Feather
  step3();      // Average
  step4();      // Fade
  step5();      // Set
},
};



//=========================================
//                    GiamBongMoAnh.main
//=========================================
//

//GiamBongMoAnh.main = function () {
  //GiamBongMoAnh();
//};

//GiamBongMoAnh.main();

// EOF

//"AtheGiamBongMoAnh.jsx"
// EOF
