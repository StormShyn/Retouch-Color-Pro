#target photoshop
//
// HDR_Adjust_Sharpness.jsx
//

//
// Generated Wed Aug 14 2019 01:33:59 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== HDR [Adjust] Sharpness ==============
//
$._ext_HDR008={
run : function HDR_Adjust_Sharpness() {
  // Show
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "HDR Details");
    list1.putReference(ref1);
    desc1.putList(cTID('null'), list1);
    executeAction(cTID('Shw '), desc1, dialogMode);
  };

  // Select
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "HDR Details");
    desc1.putReference(cTID('null'), ref1);
    desc1.putBoolean(cTID('MkVs'), false);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Set
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putIndex(sTID("filterFX"), 2);
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 1);
    desc3.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 56);
    desc3.putInteger(cTID('Thsh'), 0);
    desc2.putObject(cTID('Fltr'), cTID('UnsM'), desc3);
    desc1.putObject(sTID("filterFX"), sTID("filterFX"), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Show
  step2();      // Select
  step3(true, true);      // Set
},
};



//=========================================
//                    HDR_Adjust_Sharpness.main
//=========================================
//

//HDR_Adjust_Sharpness.main = function () {
  //HDR_Adjust_Sharpness();
//};

//HDR_Adjust_Sharpness.main();

// EOF

//"HDR_Adjust_Sharpness.jsx"
// EOF
