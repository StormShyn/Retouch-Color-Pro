﻿#target photoshop
//
// LAM BONG NET TOC.jsx
//

//
// Generated Tue Jul 30 2019 15:50:18 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== LAM BONG NET TOC ==============
//
$._ext_078={
run : function LAMBONGNETTOC() {
  // Make
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Blonde Shine | Enhance Contrast");
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindDefault"));
    desc2.putObject(cTID('Type'), cTID('Crvs'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Set
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('AdjL'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc3 = new ActionDescriptor();
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc3.putReference(cTID('Chnl'), ref2);
    var list2 = new ActionList();
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 0);
    desc4.putDouble(cTID('Vrtc'), 87);
    list2.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Hrzn'), 86);
    desc5.putDouble(cTID('Vrtc'), 115);
    list2.putObject(cTID('Pnt '), desc5);
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Hrzn'), 162);
    desc6.putDouble(cTID('Vrtc'), 186);
    list2.putObject(cTID('Pnt '), desc6);
    var desc7 = new ActionDescriptor();
    desc7.putDouble(cTID('Hrzn'), 255);
    desc7.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc7);
    desc3.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc3);
    desc2.putList(cTID('Adjs'), list1);
    desc1.putObject(cTID('T   '), cTID('Crvs'), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Select
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), sTID("RGB"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putBoolean(cTID('MkVs'), false);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Set
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 50);
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 500);
    desc2.putObject(cTID('Lefx'), cTID('Lefx'), desc3);
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Delete
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Msk '));
    ref1.putName(cTID('Lyr '), "Blonde Shine | Enhance Contrast");
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('Dlt '), desc1, dialogMode);
  };

  // Make
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('Lyr '));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Blonde Shine | Highlight Enhancement");
    desc1.putObject(cTID('Usng'), cTID('Lyr '), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Merge Visible
  function step8(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('Dplc'), true);
    executeAction(sTID('mergeVisible'), desc1, dialogMode);
  };

  // High Pass
  function step9(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 250);
    executeAction(sTID('highPass'), desc1, dialogMode);
  };

  // Gaussian Blur
  function step10(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 15);
    executeAction(sTID('gaussianBlur'), desc1, dialogMode);
  };

  // Selective Color
  function step11(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Whts'));
    desc2.putUnitDouble(cTID('Blck'), cTID('#Prc'), -100);
    list1.putObject(cTID('ClrC'), desc2);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Curves
  function step12(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(130);
    list2.putInteger(131);
    list2.putInteger(132);
    list2.putInteger(133);
    list2.putInteger(134);
    list2.putInteger(135);
    list2.putInteger(136);
    list2.putInteger(137);
    list2.putInteger(138);
    list2.putInteger(139);
    list2.putInteger(140);
    list2.putInteger(141);
    list2.putInteger(142);
    list2.putInteger(143);
    list2.putInteger(144);
    list2.putInteger(145);
    list2.putInteger(146);
    list2.putInteger(147);
    list2.putInteger(148);
    list2.putInteger(149);
    list2.putInteger(150);
    list2.putInteger(151);
    list2.putInteger(152);
    list2.putInteger(153);
    list2.putInteger(154);
    list2.putInteger(155);
    list2.putInteger(156);
    list2.putInteger(157);
    list2.putInteger(158);
    list2.putInteger(159);
    list2.putInteger(160);
    list2.putInteger(161);
    list2.putInteger(162);
    list2.putInteger(163);
    list2.putInteger(164);
    list2.putInteger(165);
    list2.putInteger(166);
    list2.putInteger(167);
    list2.putInteger(168);
    list2.putInteger(169);
    list2.putInteger(170);
    list2.putInteger(171);
    list2.putInteger(172);
    list2.putInteger(173);
    list2.putInteger(174);
    list2.putInteger(175);
    list2.putInteger(176);
    list2.putInteger(177);
    list2.putInteger(178);
    list2.putInteger(179);
    list2.putInteger(180);
    list2.putInteger(181);
    list2.putInteger(182);
    list2.putInteger(183);
    list2.putInteger(184);
    list2.putInteger(185);
    list2.putInteger(186);
    list2.putInteger(187);
    list2.putInteger(188);
    list2.putInteger(189);
    list2.putInteger(190);
    list2.putInteger(191);
    list2.putInteger(192);
    list2.putInteger(193);
    list2.putInteger(194);
    list2.putInteger(195);
    list2.putInteger(196);
    list2.putInteger(197);
    list2.putInteger(198);
    list2.putInteger(199);
    list2.putInteger(200);
    list2.putInteger(201);
    list2.putInteger(202);
    list2.putInteger(203);
    list2.putInteger(204);
    list2.putInteger(205);
    list2.putInteger(206);
    list2.putInteger(207);
    list2.putInteger(208);
    list2.putInteger(209);
    list2.putInteger(210);
    list2.putInteger(211);
    list2.putInteger(212);
    list2.putInteger(213);
    list2.putInteger(214);
    list2.putInteger(215);
    list2.putInteger(216);
    list2.putInteger(217);
    list2.putInteger(218);
    list2.putInteger(219);
    list2.putInteger(220);
    list2.putInteger(221);
    list2.putInteger(222);
    list2.putInteger(223);
    list2.putInteger(224);
    list2.putInteger(225);
    list2.putInteger(226);
    list2.putInteger(227);
    list2.putInteger(228);
    list2.putInteger(229);
    list2.putInteger(230);
    list2.putInteger(231);
    list2.putInteger(232);
    list2.putInteger(233);
    list2.putInteger(234);
    list2.putInteger(235);
    list2.putInteger(236);
    list2.putInteger(237);
    list2.putInteger(238);
    list2.putInteger(239);
    list2.putInteger(240);
    list2.putInteger(241);
    list2.putInteger(242);
    list2.putInteger(243);
    list2.putInteger(244);
    list2.putInteger(245);
    list2.putInteger(246);
    list2.putInteger(247);
    list2.putInteger(248);
    list2.putInteger(249);
    list2.putInteger(250);
    list2.putInteger(251);
    list2.putInteger(252);
    list2.putInteger(253);
    list2.putInteger(254);
    list2.putInteger(255);
    desc2.putList(cTID('Mpng'), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Set
  function step13(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Select
  function step14(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Blonde Shine | Enhance Contrast");
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(sTID("selectionModifier"), sTID("selectionModifierType"), sTID("addToSelectionContinuous"));
    desc1.putBoolean(cTID('MkVs'), false);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Make
  function step15(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("layerSection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('From'), ref2);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Làm Bóng,Nét Tóc");
    desc2.putEnumerated(cTID('Clr '), cTID('Clr '), cTID('Bl  '));
    desc1.putObject(cTID('Usng'), sTID("layerSection"), desc2);
    desc1.putInteger(sTID("layerSectionStart"), 27);
    desc1.putInteger(sTID("layerSectionEnd"), 28);
    desc1.putString(cTID('Nm  '), "Làm Bóng,Nét Tóc");
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step16(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putClass(cTID('Nw  '), cTID('Chnl'));
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Msk '));
    desc1.putReference(cTID('At  '), ref1);
    desc1.putEnumerated(cTID('Usng'), cTID('UsrM'), cTID('HdAl'));
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Select
  function step17(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('PbTl'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Reset
  function step18(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Clr '), cTID('Clrs'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('Rset'), desc1, dialogMode);
  };

  // Set
  function step19(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step2();      // Make
  step3();      // Set
  step4();      // Select
  step5();      // Set
  step6();      // Delete
  step7();      // Make
  step8();      // Merge Visible
  step9();      // High Pass
  step10();      // Gaussian Blur
  step11();      // Selective Color
  step12();      // Curves
  step13();      // Set
  step14();      // Select
  step15();      // Make
  step16();      // Make
  step17();      // Select
  step18();      // Reset
  step19();      // Set
},
};



//=========================================
//                    LAMBONGNETTOC.main
//=========================================
//

//LAMBONGNETTOC.main = function () {
  //LAMBONGNETTOC();
//};

//LAMBONGNETTOC.main();

// EOF

//"LAM BONG NET TOC.jsx"
// EOF
