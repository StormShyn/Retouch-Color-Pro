#target photoshop
//
// TiecTangSang.jsx
//

//
// Generated Sun Aug 04 2019 19:20:25 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Tiec Tang Sang ==============
//
$._ext_TTS1={
run : function TiecTangSang() {
  // Apply Image
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), sTID("RGB"));
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Mrgd'));
    desc2.putReference(cTID('T   '), ref1);
    desc2.putEnumerated(cTID('Clcl'), cTID('Clcn'), cTID('Scrn'));
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 20);
    desc2.putBoolean(cTID('PrsT'), true);
    desc1.putObject(cTID('With'), cTID('Clcl'), desc2);
    executeAction(sTID('applyImageEvent'), desc1, dialogMode);
  };

  step1();      // Apply Image
},
};



//=========================================
//                    TiecTangSang.main
//=========================================
//

//TiecTangSang.main = function () {
  //TiecTangSang();
//};

//TiecTangSang.main();

// EOF

//"TiecTangSang.jsx"
// EOF
