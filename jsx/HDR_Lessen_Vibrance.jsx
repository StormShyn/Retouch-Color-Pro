#target photoshop
//
// HDR_Lessen_Vibrance.jsx
//

//
// Generated Wed Aug 14 2019 01:33:46 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== HDR [Lessen] Vibrance ==============
//
$._ext_HDR007={
run : function HDR_Lessen_Vibrance() {
  // Show
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Neutral Colour");
    list1.putReference(ref1);
    desc1.putList(cTID('null'), list1);
    executeAction(cTID('Shw '), desc1, dialogMode);
  };

  step1();      // Show
},
};



//=========================================
//                    HDR_Lessen_Vibrance.main
//=========================================
//

//HDR_Lessen_Vibrance.main = function () {
  //HDR_Lessen_Vibrance();
//};

//HDR_Lessen_Vibrance.main();

// EOF

//"HDR_Lessen_Vibrance.jsx"
// EOF
