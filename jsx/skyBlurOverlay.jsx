#target photoshop
//
// skyBlurOverlay.jsx
//

//
// Generated Sun Aug 11 2019 11:54:06 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== sky Blur Overlay ==============
//
$._ext_sky05={
run : function skyBlurOverlay() {
  // Gaussian Blur
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 11);
    executeAction(sTID('gaussianBlur'), desc1, dialogMode);
  };

  // Select
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Background");
    desc1.putReference(cTID('null'), ref1);
    desc1.putBoolean(cTID('MkVs'), false);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  step1();      // Gaussian Blur
  step2();      // Select
},
};



//=========================================
//                    skyBlurOverlay.main
//=========================================
//

//skyBlurOverlay.main = function () {
  //skyBlurOverlay();
//};

//skyBlurOverlay.main();

// EOF

//"skyBlurOverlay.jsx"
// EOF
