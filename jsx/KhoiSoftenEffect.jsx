#target photoshop
//
// KhoiSoftenEffect.jsx
//

//
// Generated Thu Aug 15 2019 00:49:27 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Khoi Soften Effect ==============
//
$._ext_khoi07={
run : function KhoiSoftenEffect() {
  // Set
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 51);
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Set
},
};



//=========================================
//                    KhoiSoftenEffect.main
//=========================================
//

//KhoiSoftenEffect.main = function () {
 // KhoiSoftenEffect();
//};

//KhoiSoftenEffect.main();

// EOF

//"KhoiSoftenEffect.jsx"
// EOF
