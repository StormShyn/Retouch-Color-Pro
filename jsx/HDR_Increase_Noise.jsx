#target photoshop
//
// HDR_Increase_Noise.jsx
//

//
// Generated Wed Aug 14 2019 01:34:10 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== HDR [Increase] Noise ==============
//
$._ext_HDR009={
run : function HDR_Increase_Noise() {
  // Show
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Add Noise");
    list1.putReference(ref1);
    desc1.putList(cTID('null'), list1);
    executeAction(cTID('Shw '), desc1, dialogMode);
  };

  step1();      // Show
},
};



//=========================================
//                    HDR_Increase_Noise.main
//=========================================
//

//HDR_Increase_Noise.main = function () {
  //HDR_Increase_Noise();
//};

//HDR_Increase_Noise.main();

// EOF

//"HDR_Increase_Noise.jsx"
// EOF
