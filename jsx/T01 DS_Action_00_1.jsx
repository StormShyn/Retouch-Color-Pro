#target photoshop
//
// T01 DS_Action_00_1.jsx
//

//
// Generated Thu Aug 01 2019 00:54:19 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== DS.Action-00-1 ==============
//
$._ext_T01={
run : function DS_Action_00_1() {
  // Curves
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Hrzn'), 0);
    desc3.putDouble(cTID('Vrtc'), 0);
    list2.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 113);
    desc4.putDouble(cTID('Vrtc'), 143);
    list2.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Hrzn'), 255);
    desc5.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc5);
    desc2.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Color Balance
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(-7);
    list2.putInteger(0);
    list2.putInteger(0);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Channel Mixer
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Rd  '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Rd  '), cTID('ChMx'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Grn '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Grn '), cTID('ChMx'), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putUnitDouble(cTID('Rd  '), cTID('#Prc'), 4);
    desc4.putUnitDouble(cTID('Bl  '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Bl  '), cTID('ChMx'), desc4);
    executeAction(sTID('channelMixer'), desc1, dialogMode);
  };

  // Color Balance
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(-9);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Levels
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    list2.putInteger(14);
    list2.putInteger(255);
    desc2.putList(cTID('Inpt'), list2);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Bl  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Inverse
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invs'), undefined, dialogMode);
  };

  // Curves
  function step8(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Hrzn'), 0);
    desc3.putDouble(cTID('Vrtc'), 0);
    list2.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 111);
    desc4.putDouble(cTID('Vrtc'), 151);
    list2.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Hrzn'), 255);
    desc5.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc5);
    desc2.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Curves
  function step9(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Hrzn'), 9);
    desc3.putDouble(cTID('Vrtc'), 0);
    list2.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 255);
    desc4.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc4);
    desc2.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Set
  function step10(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Levels
  function step11(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 0.9);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step12(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('Al  '));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Copy
  function step13(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('copy'), undefined, dialogMode);
  };

  // Assign Profile
  function step14(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Dcmn'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putString(sTID("profile"), "Lam Tung");
    executeAction(sTID('assignProfile'), desc1, dialogMode);
  };

  // Convert to Profile
  function step15(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Dcmn'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putClass(cTID('TMd '), sTID("RGBColorMode"));
    desc1.putEnumerated(cTID('Inte'), cTID('Inte'), cTID('Img '));
    desc1.putBoolean(cTID('MpBl'), true);
    desc1.putBoolean(cTID('Dthr'), true);
    executeAction(sTID('convertToProfile'), desc1, dialogMode);
  };

  // Paste
  function step16(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('AntA'), cTID('Annt'), cTID('Anno'));
    executeAction(cTID('past'), desc1, dialogMode);
  };

  // Select
  function step17(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Lyr '), cTID('Bckg'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Set
  function step18(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Lyr '), cTID('Bckg'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Move
  function step19(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Nxt '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('move'), desc1, dialogMode);
  };

  // Color Range
  function step20(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 16);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 86.08);
    desc2.putDouble(cTID('A   '), 0.66);
    desc2.putDouble(cTID('B   '), -9.3);
    desc1.putObject(cTID('Mnm '), cTID('LbCl'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Lmnc'), 99.51);
    desc3.putDouble(cTID('A   '), 9.48);
    desc3.putDouble(cTID('B   '), 4.63);
    desc1.putObject(cTID('Mxm '), cTID('LbCl'), desc3);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Feather
  function step21(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 35);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Delete
  function step22(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Dlt '), undefined, dialogMode);
  };

  // Set
  function step23(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Color Range
  function step24(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 16);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 0);
    desc2.putDouble(cTID('A   '), -6.23);
    desc2.putDouble(cTID('B   '), -25.55);
    desc1.putObject(cTID('Mnm '), cTID('LbCl'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Lmnc'), 61.73);
    desc3.putDouble(cTID('A   '), 14.78);
    desc3.putDouble(cTID('B   '), 15.47);
    desc1.putObject(cTID('Mxm '), cTID('LbCl'), desc3);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Feather
  function step25(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 35);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Delete
  function step26(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Dlt '), undefined, dialogMode);
  };

  // Delete
  function step27(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Dlt '), undefined, dialogMode);
  };

  // Set
  function step28(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Color Range
  function step29(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Shdw'));
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Feather
  function step30(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 20);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Delete
  function step31(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Dlt '), undefined, dialogMode);
  };

  // Set
  function step32(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Color Range
  function step33(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 16);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 86.16);
    desc2.putDouble(cTID('A   '), 9.45);
    desc2.putDouble(cTID('B   '), 0.49);
    desc1.putObject(cTID('Mnm '), cTID('LbCl'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Lmnc'), 92.76);
    desc3.putDouble(cTID('A   '), 14.15);
    desc3.putDouble(cTID('B   '), 6.05);
    desc1.putObject(cTID('Mxm '), cTID('LbCl'), desc3);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Feather
  function step34(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 50);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Levels
  function step35(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 0.73);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step36(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Flatten Image
  function step37(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('flattenImage'), undefined, dialogMode);
  };

  // Set
  function step38(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('Al  '));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Copy
  function step39(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('copy'), undefined, dialogMode);
  };

  // Set
  function step40(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Levels
  function step41(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 0.96);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Color Balance
  function step42(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(2);
    list2.putInteger(0);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Channel Mixer
  function step43(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Rd  '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Rd  '), cTID('ChMx'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Grn '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Grn '), cTID('ChMx'), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putUnitDouble(cTID('Rd  '), cTID('#Prc'), 5);
    desc4.putUnitDouble(cTID('Bl  '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Bl  '), cTID('ChMx'), desc4);
    executeAction(sTID('channelMixer'), desc1, dialogMode);
  };

  // Color Balance
  function step44(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(2);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Set
  function step45(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Bl  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Inverse
  function step46(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invs'), undefined, dialogMode);
  };

  // Levels
  function step47(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 1.08);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step48(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Selective Color
  function step49(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Ylws'));
    desc2.putUnitDouble(cTID('Ylw '), cTID('#Prc'), -19);
    list1.putObject(cTID('ClrC'), desc2);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Fade
  function step50(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 79);
    desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    executeAction(cTID('Fade'), desc1, dialogMode);
  };

  // Curves
  function step51(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Bl  '));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Hrzn'), 0);
    desc3.putDouble(cTID('Vrtc'), 0);
    list2.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 128);
    desc4.putDouble(cTID('Vrtc'), 132);
    list2.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Hrzn'), 255);
    desc5.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc5);
    desc2.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Set
  function step52(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Rd  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Levels
  function step53(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 0.92);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step54(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Levels
  function step55(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 0.97);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step56(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Bl  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Inverse
  function step57(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invs'), undefined, dialogMode);
  };

  // Levels
  function step58(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 1.08);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step59(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step60(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 220.700565936999);
    desc3.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.831840512638);
    list1.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 220.700565936999);
    desc4.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.831840512638);
    list1.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 219.885462493326);
    desc5.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.831840512638);
    list1.putObject(cTID('Pnt '), desc5);
    var desc6 = new ActionDescriptor();
    desc6.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 219.678804057662);
    desc6.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.831840512638);
    list1.putObject(cTID('Pnt '), desc6);
    var desc7 = new ActionDescriptor();
    desc7.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 219.054394020288);
    desc7.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.831840512638);
    list1.putObject(cTID('Pnt '), desc7);
    var desc8 = new ActionDescriptor();
    desc8.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 218.85483115323);
    desc8.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.831840512638);
    list1.putObject(cTID('Pnt '), desc8);
    var desc9 = new ActionDescriptor();
    desc9.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 218.739528163374);
    desc9.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.831840512638);
    list1.putObject(cTID('Pnt '), desc9);
    var desc10 = new ActionDescriptor();
    desc10.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 218.524000266951);
    desc10.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.831840512638);
    list1.putObject(cTID('Pnt '), desc10);
    var desc11 = new ActionDescriptor();
    desc11.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 218.316454885211);
    desc11.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.831840512638);
    list1.putObject(cTID('Pnt '), desc11);
    var desc12 = new ActionDescriptor();
    desc12.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 218.061014415376);
    desc12.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.902782128871);
    list1.putObject(cTID('Pnt '), desc12);
    var desc13 = new ActionDescriptor();
    desc13.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.966111185264);
    desc13.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 186.950667719829);
    list1.putObject(cTID('Pnt '), desc13);
    var desc14 = new ActionDescriptor();
    desc14.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.858790710091);
    desc14.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 187.00564747241);
    list1.putObject(cTID('Pnt '), desc14);
    var desc15 = new ActionDescriptor();
    desc15.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.60689802456);
    desc15.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 187.129795300819);
    list1.putObject(cTID('Pnt '), desc15);
    var desc16 = new ActionDescriptor();
    desc16.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.511994794447);
    desc16.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 187.177680891776);
    list1.putObject(cTID('Pnt '), desc16);
    var desc17 = new ActionDescriptor();
    desc17.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.404674319274);
    desc17.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 187.232660644357);
    list1.putObject(cTID('Pnt '), desc17);
    var desc18 = new ActionDescriptor();
    desc18.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.124399359317);
    desc18.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 187.626386614454);
    list1.putObject(cTID('Pnt '), desc18);
    var desc19 = new ActionDescriptor();
    desc19.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.057878403631);
    desc19.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 187.759402144891);
    list1.putObject(cTID('Pnt '), desc19);
    var desc20 = new ActionDescriptor();
    desc20.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.99224439402);
    desc20.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 187.888870594518);
    list1.putObject(cTID('Pnt '), desc20);
    var desc21 = new ActionDescriptor();
    desc21.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.946123198078);
    desc21.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 187.980207925418);
    list1.putObject(cTID('Pnt '), desc21);
    var desc22 = new ActionDescriptor();
    desc22.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.892019487453);
    desc22.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 188.084846809363);
    list1.putObject(cTID('Pnt '), desc22);
    var desc23 = new ActionDescriptor();
    desc23.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.386460224239);
    desc23.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 189.215478818085);
    list1.putObject(cTID('Pnt '), desc23);
    var desc24 = new ActionDescriptor();
    desc24.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.20197544047);
    desc24.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 189.630487273051);
    list1.putObject(cTID('Pnt '), desc24);
    var desc25 = new ActionDescriptor();
    desc25.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.137228376935);
    desc25.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 189.775030816127);
    list1.putObject(cTID('Pnt '), desc25);
    var desc26 = new ActionDescriptor();
    desc26.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.074255205553);
    desc26.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 189.916914048594);
    list1.putObject(cTID('Pnt '), desc26);
    var desc27 = new ActionDescriptor();
    desc27.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.042325146823);
    desc27.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 189.98874243503);
    list1.putObject(cTID('Pnt '), desc27);
    var desc28 = new ActionDescriptor();
    desc28.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.97935197544);
    desc28.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 190.127965356889);
    list1.putObject(cTID('Pnt '), desc28);
    var desc29 = new ActionDescriptor();
    desc29.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.59175654031);
    desc29.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 190.91807760769);
    list1.putObject(cTID('Pnt '), desc29);
    var desc30 = new ActionDescriptor();
    desc30.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.495966364122);
    desc30.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 191.10961997152);
    list1.putObject(cTID('Pnt '), desc30);
    var desc31 = new ActionDescriptor();
    desc31.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.415254271223);
    desc31.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 191.270125378248);
    list1.putObject(cTID('Pnt '), desc31);
    var desc32 = new ActionDescriptor();
    desc32.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.36292445275);
    desc32.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 191.37387749199);
    list1.putObject(cTID('Pnt '), desc32);
    var desc33 = new ActionDescriptor();
    desc33.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.305272957822);
    desc33.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 191.486497307761);
    list1.putObject(cTID('Pnt '), desc33);
    var desc34 = new ActionDescriptor();
    desc34.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.137640149493);
    desc34.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 192.961196155215);
    list1.putObject(cTID('Pnt '), desc34);
    var desc35 = new ActionDescriptor();
    desc35.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.09151895355);
    desc35.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 193.514540761837);
    list1.putObject(cTID('Pnt '), desc35);
    var desc36 = new ActionDescriptor();
    desc36.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.07466697811);
    desc36.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 193.707856666073);
    list1.putObject(cTID('Pnt '), desc36);
    var desc37 = new ActionDescriptor();
    desc37.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.064910571276);
    desc37.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 193.818702941438);
    list1.putObject(cTID('Pnt '), desc37);
    var desc38 = new ActionDescriptor();
    desc38.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.050719434063);
    desc38.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 193.989849590602);
    list1.putObject(cTID('Pnt '), desc38);
    var desc39 = new ActionDescriptor();
    desc39.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.026771890016);
    desc39.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 194.213315681737);
    list1.putObject(cTID('Pnt '), desc39);
    var desc40 = new ActionDescriptor();
    desc40.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc40.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 194.834054823781);
    list1.putObject(cTID('Pnt '), desc40);
    var desc41 = new ActionDescriptor();
    desc41.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc41.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 195.033578119438);
    list1.putObject(cTID('Pnt '), desc41);
    var desc42 = new ActionDescriptor();
    desc42.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc42.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 195.148858245817);
    list1.putObject(cTID('Pnt '), desc42);
    var desc43 = new ActionDescriptor();
    desc43.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc43.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 195.364343405126);
    list1.putObject(cTID('Pnt '), desc43);
    var desc44 = new ActionDescriptor();
    desc44.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc44.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 195.575394713421);
    list1.putObject(cTID('Pnt '), desc44);
    var desc45 = new ActionDescriptor();
    desc45.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc45.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 196.82042007832);
    list1.putObject(cTID('Pnt '), desc45);
    var desc46 = new ActionDescriptor();
    desc46.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc46.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 197.175128159487);
    list1.putObject(cTID('Pnt '), desc46);
    var desc47 = new ActionDescriptor();
    desc47.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc47.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 197.446479841581);
    list1.putObject(cTID('Pnt '), desc47);
    var desc48 = new ActionDescriptor();
    desc48.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc48.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 197.642456056426);
    list1.putObject(cTID('Pnt '), desc48);
    var desc49 = new ActionDescriptor();
    desc49.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc49.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 197.843752892488);
    list1.putObject(cTID('Pnt '), desc49);
    var desc50 = new ActionDescriptor();
    desc50.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc50.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 198.182499110004);
    list1.putObject(cTID('Pnt '), desc50);
    var desc51 = new ActionDescriptor();
    desc51.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc51.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 198.310194019224);
    list1.putObject(cTID('Pnt '), desc51);
    var desc52 = new ActionDescriptor();
    desc52.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc52.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 198.537207191171);
    list1.putObject(cTID('Pnt '), desc52);
    var desc53 = new ActionDescriptor();
    desc53.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.024111051789);
    desc53.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 198.753579120683);
    list1.putObject(cTID('Pnt '), desc53);
    var desc54 = new ActionDescriptor();
    desc54.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.364698344901);
    desc54.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 199.885097899608);
    list1.putObject(cTID('Pnt '), desc54);
    var desc55 = new ActionDescriptor();
    desc55.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.492418579818);
    desc55.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 200.268182627269);
    list1.putObject(cTID('Pnt '), desc55);
    var desc56 = new ActionDescriptor();
    desc56.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.54297450614);
    desc56.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 200.418046791563);
    list1.putObject(cTID('Pnt '), desc56);
    var desc57 = new ActionDescriptor();
    desc57.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.605947677523);
    desc57.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 200.606042074582);
    list1.putObject(cTID('Pnt '), desc57);
    var desc58 = new ActionDescriptor();
    desc58.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 215.670694741057);
    desc58.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 200.794924127803);
    list1.putObject(cTID('Pnt '), desc58);
    var desc59 = new ActionDescriptor();
    desc59.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.159402028831);
    desc59.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 201.47419010324);
    list1.putObject(cTID('Pnt '), desc59);
    var desc60 = new ActionDescriptor();
    desc60.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.287122263748);
    desc60.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 201.633808739765);
    list1.putObject(cTID('Pnt '), desc60);
    var desc61 = new ActionDescriptor();
    desc61.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.352756273358);
    desc61.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 201.715391598434);
    list1.putObject(cTID('Pnt '), desc61);
    var desc62 = new ActionDescriptor();
    desc62.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.507971836626);
    desc62.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 201.90870750267);
    list1.putObject(cTID('Pnt '), desc62);
    var desc63 = new ActionDescriptor();
    desc63.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 216.526597704218);
    desc63.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 201.932650298149);
    list1.putObject(cTID('Pnt '), desc63);
    var desc64 = new ActionDescriptor();
    desc64.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.181163908169);
    desc64.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 202.949775720897);
    list1.putObject(cTID('Pnt '), desc64);
    var desc65 = new ActionDescriptor();
    desc65.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.41176988788);
    desc65.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 203.318672125311);
    list1.putObject(cTID('Pnt '), desc65);
    var desc66 = new ActionDescriptor();
    desc66.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.540377068873);
    desc66.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 203.523516042186);
    list1.putObject(cTID('Pnt '), desc66);
    var desc67 = new ActionDescriptor();
    desc67.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.636167245061);
    desc67.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 203.676927287291);
    list1.putObject(cTID('Pnt '), desc67);
    var desc68 = new ActionDescriptor();
    desc68.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 217.680514548852);
    desc68.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 203.745208592916);
    list1.putObject(cTID('Pnt '), desc68);
    var desc69 = new ActionDescriptor();
    desc69.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 218.316454885211);
    desc69.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 204.198348166607);
    list1.putObject(cTID('Pnt '), desc69);
    var desc70 = new ActionDescriptor();
    desc70.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 218.458366257341);
    desc70.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 204.283478106088);
    list1.putObject(cTID('Pnt '), desc70);
    var desc71 = new ActionDescriptor();
    desc71.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 218.591408168713);
    desc71.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 204.36328742435);
    list1.putObject(cTID('Pnt '), desc71);
    var desc72 = new ActionDescriptor();
    desc72.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 218.698728643887);
    desc72.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 204.428021649163);
    list1.putObject(cTID('Pnt '), desc72);
    var desc73 = new ActionDescriptor();
    desc73.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 218.775892952483);
    desc73.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 204.475907240121);
    list1.putObject(cTID('Pnt '), desc73);
    var desc74 = new ActionDescriptor();
    desc74.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 219.792333155366);
    desc74.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 205.21990744037);
    list1.putObject(cTID('Pnt '), desc74);
    var desc75 = new ActionDescriptor();
    desc75.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 220.18968499733);
    desc75.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 205.517862228551);
    list1.putObject(cTID('Pnt '), desc75);
    var desc76 = new ActionDescriptor();
    desc76.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 220.315631340096);
    desc76.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 205.612746640263);
    list1.putObject(cTID('Pnt '), desc76);
    var desc77 = new ActionDescriptor();
    desc77.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 220.386587026161);
    desc77.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 205.665952852439);
    list1.putObject(cTID('Pnt '), desc77);
    var desc78 = new ActionDescriptor();
    desc78.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 220.555993726642);
    desc78.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 205.794534531862);
    list1.putObject(cTID('Pnt '), desc78);
    var desc79 = new ActionDescriptor();
    desc79.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 220.589697677523);
    desc79.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 205.819364097544);
    list1.putObject(cTID('Pnt '), desc79);
    var desc80 = new ActionDescriptor();
    desc80.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.154682327816);
    desc80.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 206.354973300107);
    list1.putObject(cTID('Pnt '), desc80);
    var desc81 = new ActionDescriptor();
    desc81.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.332071542979);
    desc81.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 206.532327340691);
    list1.putObject(cTID('Pnt '), desc81);
    var desc82 = new ActionDescriptor();
    desc82.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.500591297384);
    desc82.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 206.700813679245);
    list1.putObject(cTID('Pnt '), desc82);
    var desc83 = new ActionDescriptor();
    desc83.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc83.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 206.924279770381);
    list1.putObject(cTID('Pnt '), desc83);
    var desc84 = new ActionDescriptor();
    desc84.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc84.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 207.390720897116);
    list1.putObject(cTID('Pnt '), desc84);
    var desc85 = new ActionDescriptor();
    desc85.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc85.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 207.66207257921);
    list1.putObject(cTID('Pnt '), desc85);
    var desc86 = new ActionDescriptor();
    desc86.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc86.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 207.858048794055);
    list1.putObject(cTID('Pnt '), desc86);
    var desc87 = new ActionDescriptor();
    desc87.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc87.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 208.059345630117);
    list1.putObject(cTID('Pnt '), desc87);
    var desc88 = new ActionDescriptor();
    desc88.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc88.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 208.298773584906);
    list1.putObject(cTID('Pnt '), desc88);
    var desc89 = new ActionDescriptor();
    desc89.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc89.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 208.525786756853);
    list1.putObject(cTID('Pnt '), desc89);
    var desc90 = new ActionDescriptor();
    desc90.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc90.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 208.742158686365);
    list1.putObject(cTID('Pnt '), desc90);
    var desc91 = new ActionDescriptor();
    desc91.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc91.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 209.760170879316);
    list1.putObject(cTID('Pnt '), desc91);
    var desc92 = new ActionDescriptor();
    desc92.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc92.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 209.987184051264);
    list1.putObject(cTID('Pnt '), desc92);
    var desc93 = new ActionDescriptor();
    desc93.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc93.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 210.114878960484);
    list1.putObject(cTID('Pnt '), desc93);
    var desc94 = new ActionDescriptor();
    desc94.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc94.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 210.341892132431);
    list1.putObject(cTID('Pnt '), desc94);
    var desc95 = new ActionDescriptor();
    desc95.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc95.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 210.558264061944);
    list1.putObject(cTID('Pnt '), desc95);
    var desc96 = new ActionDescriptor();
    desc96.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc96.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 210.895236739053);
    list1.putObject(cTID('Pnt '), desc96);
    var desc97 = new ActionDescriptor();
    desc97.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc97.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 211.022931648273);
    list1.putObject(cTID('Pnt '), desc97);
    var desc98 = new ActionDescriptor();
    desc98.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc98.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 211.249944820221);
    list1.putObject(cTID('Pnt '), desc98);
    var desc99 = new ActionDescriptor();
    desc99.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.608798718633);
    desc99.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 211.466316749733);
    list1.putObject(cTID('Pnt '), desc99);
    var desc100 = new ActionDescriptor();
    desc100.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.722327816337);
    desc100.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 212.370822356711);
    list1.putObject(cTID('Pnt '), desc100);
    var desc101 = new ActionDescriptor();
    desc101.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.750710090763);
    desc101.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 212.569458882164);
    list1.putObject(cTID('Pnt '), desc101);
    var desc102 = new ActionDescriptor();
    desc102.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.766675120128);
    desc102.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 212.681191927732);
    list1.putObject(cTID('Pnt '), desc102);
    var desc103 = new ActionDescriptor();
    desc103.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.791509610251);
    desc103.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 212.847904725881);
    list1.putObject(cTID('Pnt '), desc103);
    var desc104 = new ActionDescriptor();
    desc104.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 221.824326615056);
    desc104.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 213.052748642755);
    list1.putObject(cTID('Pnt '), desc104);
    var desc105 = new ActionDescriptor();
    desc105.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 222.06291510945);
    desc105.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 213.846407974368);
    list1.putObject(cTID('Pnt '), desc105);
    var desc106 = new ActionDescriptor();
    desc106.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 222.119679658302);
    desc106.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 214.016667853329);
    list1.putObject(cTID('Pnt '), desc106);
    var desc107 = new ActionDescriptor();
    desc107.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 222.188861452216);
    desc107.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 214.222398540406);
    list1.putObject(cTID('Pnt '), desc107);
    var desc108 = new ActionDescriptor();
    desc108.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 222.257156300053);
    desc108.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 214.418374755251);
    list1.putObject(cTID('Pnt '), desc108);
    var desc109 = new ActionDescriptor();
    desc109.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 222.360928990924);
    desc109.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 214.811213955144);
    list1.putObject(cTID('Pnt '), desc109);
    var desc110 = new ActionDescriptor();
    desc110.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 222.394632941805);
    desc110.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 214.946003025988);
    list1.putObject(cTID('Pnt '), desc110);
    var desc111 = new ActionDescriptor();
    desc111.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 222.415919647624);
    desc111.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 215.028472654859);
    list1.putObject(cTID('Pnt '), desc111);
    var desc112 = new ActionDescriptor();
    desc112.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 222.440754137747);
    desc112.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 215.12601737718);
    list1.putObject(cTID('Pnt '), desc112);
    var desc113 = new ActionDescriptor();
    desc113.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 222.493970902296);
    desc113.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 215.32642744304);
    list1.putObject(cTID('Pnt '), desc113);
    var desc114 = new ActionDescriptor();
    desc114.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 222.971147891084);
    desc114.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 216.116539693841);
    list1.putObject(cTID('Pnt '), desc114);
    var desc115 = new ActionDescriptor();
    desc115.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 223.113059263214);
    desc115.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 216.329364542542);
    list1.putObject(cTID('Pnt '), desc115);
    var desc116 = new ActionDescriptor();
    desc116.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 223.222153630539);
    desc116.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 216.492530259879);
    list1.putObject(cTID('Pnt '), desc116);
    var desc117 = new ActionDescriptor();
    desc117.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 223.312622130272);
    desc117.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 216.62643256052);
    list1.putObject(cTID('Pnt '), desc117);
    var desc118 = new ActionDescriptor();
    desc118.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 223.356082487987);
    desc118.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 216.69028001513);
    list1.putObject(cTID('Pnt '), desc118);
    var desc119 = new ActionDescriptor();
    desc119.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 224.106438868126);
    desc119.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 217.251605553578);
    list1.putObject(cTID('Pnt '), desc119);
    var desc120 = new ActionDescriptor();
    desc120.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 224.319305926321);
    desc120.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 217.393488786045);
    list1.putObject(cTID('Pnt '), desc120);
    var desc121 = new ActionDescriptor();
    desc121.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 224.482504004271);
    desc121.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 217.502561521004);
    list1.putObject(cTID('Pnt '), desc121);
    var desc122 = new ActionDescriptor();
    desc122.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 224.616432861719);
    desc122.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 217.593012081702);
    list1.putObject(cTID('Pnt '), desc122);
    var desc123 = new ActionDescriptor();
    desc123.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 224.680292979178);
    desc123.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 217.636463821645);
    list1.putObject(cTID('Pnt '), desc123);
    var desc124 = new ActionDescriptor();
    desc124.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 224.957907100908);
    desc124.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 217.762385190459);
    list1.putObject(cTID('Pnt '), desc124);
    var desc125 = new ActionDescriptor();
    desc125.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 225.163678590497);
    desc125.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 217.831553266287);
    list1.putObject(cTID('Pnt '), desc125);
    var desc126 = new ActionDescriptor();
    desc126.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 225.359693673251);
    desc126.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 217.899834571912);
    list1.putObject(cTID('Pnt '), desc126);
    var desc127 = new ActionDescriptor();
    desc127.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 225.610699412707);
    desc127.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 218.003586685653);
    list1.putObject(cTID('Pnt '), desc127);
    var desc128 = new ActionDescriptor();
    desc128.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 225.705602642819);
    desc128.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 218.051472276611);
    list1.putObject(cTID('Pnt '), desc128);
    var desc129 = new ActionDescriptor();
    desc129.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 225.812923117993);
    desc129.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 218.106452029192);
    list1.putObject(cTID('Pnt '), desc129);
    var desc130 = new ActionDescriptor();
    desc130.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 226.064815803524);
    desc130.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 218.230599857601);
    list1.putObject(cTID('Pnt '), desc130);
    var desc131 = new ActionDescriptor();
    desc131.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 226.159719033636);
    desc131.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 218.278485448558);
    list1.putObject(cTID('Pnt '), desc131);
    var desc132 = new ActionDescriptor();
    desc132.putUnitDouble(cTID('Hrzn'), cTID('#Rlt'), 226.267039508809);
    desc132.putUnitDouble(cTID('Vrtc'), cTID('#Rlt'), 218.333465201139);
    list1.putObject(cTID('Pnt '), desc132);
    desc2.putList(cTID('Pts '), list1);
    desc1.putObject(cTID('T   '), cTID('Plgn'), desc2);
    desc1.putBoolean(cTID('AntA'), true);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step61(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Selective Color
  function step62(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Ylws'));
    desc2.putUnitDouble(cTID('Ylw '), cTID('#Prc'), -23);
    list1.putObject(cTID('ClrC'), desc2);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Levels
  function step63(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 0.97);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step64(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Rd  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Levels
  function step65(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 0.95);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step66(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step67(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Rd  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Levels
  function step68(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 0.95);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step69(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step70(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Rd  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Levels
  function step71(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 0.92);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Set
  function step72(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Color Balance
  function step73(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(-6);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Paste
  function step74(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('AntA'), cTID('Annt'), cTID('Anno'));
    executeAction(cTID('past'), desc1, dialogMode);
  };

  // Color Range
  function step75(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 11);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 82.59);
    desc2.putDouble(cTID('A   '), 1.14);
    desc2.putDouble(cTID('B   '), -14.97);
    desc1.putObject(cTID('Mnm '), cTID('LbCl'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Lmnc'), 97.63);
    desc3.putDouble(cTID('A   '), 7.25);
    desc3.putDouble(cTID('B   '), -2.77);
    desc1.putObject(cTID('Mxm '), cTID('LbCl'), desc3);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Inverse
  function step76(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invs'), undefined, dialogMode);
  };

  // Feather
  function step77(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 50);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Delete
  function step78(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Dlt '), undefined, dialogMode);
  };

  // Delete
  function step79(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Dlt '), undefined, dialogMode);
  };

  // Set
  function step80(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Flatten Image
  function step81(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('flattenImage'), undefined, dialogMode);
  };

  // Set
  function step82(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Rd  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Color Balance
  function step83(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(-9);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Set
  function step84(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Selective Color
  function step85(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Blks'));
    desc2.putUnitDouble(cTID('Blck'), cTID('#Prc'), 10);
    list1.putObject(cTID('ClrC'), desc2);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Color Range
  function step86(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 15);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 69.8);
    desc2.putDouble(cTID('A   '), 14);
    desc2.putDouble(cTID('B   '), -9);
    desc1.putObject(cTID('Mnm '), cTID('LbCl'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Lmnc'), 85.88);
    desc3.putDouble(cTID('A   '), 31);
    desc3.putDouble(cTID('B   '), 10);
    desc1.putObject(cTID('Mxm '), cTID('LbCl'), desc3);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Set
  function step87(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Select
  function step88(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('HstS'), cTID('Ordn'), cTID('Prvs'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Feather
  function step89(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 30);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Color Balance
  function step90(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(-5);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Set
  function step91(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step92(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Rd  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Color Balance
  function step93(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(-25);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Hue/Saturation
  function step94(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('Clrz'), false);
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putInteger(cTID('LclR'), 1);
    desc2.putInteger(cTID('BgnR'), 315);
    desc2.putInteger(cTID('BgnS'), 345);
    desc2.putInteger(cTID('EndS'), 15);
    desc2.putInteger(cTID('EndR'), 45);
    desc2.putInteger(cTID('H   '), 0);
    desc2.putInteger(cTID('Strt'), 5);
    desc2.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(sTID('hueSaturation'), desc1, dialogMode);
  };

  // Selective Color
  function step95(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Ylws'));
    desc2.putUnitDouble(cTID('Ylw '), cTID('#Prc'), -10);
    list1.putObject(cTID('ClrC'), desc2);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Set
  function step96(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step97(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Rd  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Channel Mixer
  function step98(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Rd  '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Rd  '), cTID('ChMx'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Grn '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Grn '), cTID('ChMx'), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putUnitDouble(cTID('Rd  '), cTID('#Prc'), 2);
    desc4.putUnitDouble(cTID('Bl  '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Bl  '), cTID('ChMx'), desc4);
    executeAction(sTID('channelMixer'), desc1, dialogMode);
  };

  // Set
  function step99(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Curves
  function step100(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Hrzn'), 0);
    desc3.putDouble(cTID('Vrtc'), 0);
    list2.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 171);
    desc4.putDouble(cTID('Vrtc'), 175);
    list2.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Hrzn'), 255);
    desc5.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc5);
    desc2.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Curves
  function step101(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Hrzn'), 0);
    desc3.putDouble(cTID('Vrtc'), 0);
    list2.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 121);
    desc4.putDouble(cTID('Vrtc'), 135);
    list2.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Hrzn'), 255);
    desc5.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc5);
    desc2.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Color Balance
  function step102(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(11);
    list2.putInteger(0);
    list2.putInteger(0);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(9);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Hue/Saturation
  function step103(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('Clrz'), false);
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putInteger(cTID('H   '), 0);
    desc2.putInteger(cTID('Strt'), 3);
    desc2.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(sTID('hueSaturation'), desc1, dialogMode);
  };

  // Color Balance
  function step104(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(0);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(6);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Channel Mixer
  function step105(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Rd  '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Rd  '), cTID('ChMx'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Grn '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Grn '), cTID('ChMx'), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putUnitDouble(cTID('Rd  '), cTID('#Prc'), 4);
    desc4.putUnitDouble(cTID('Grn '), cTID('#Prc'), 4);
    desc4.putUnitDouble(cTID('Bl  '), cTID('#Prc'), 100);
    desc1.putObject(cTID('Bl  '), cTID('ChMx'), desc4);
    executeAction(sTID('channelMixer'), desc1, dialogMode);
  };

  // Fade
  function step106(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 50);
    desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    executeAction(cTID('Fade'), desc1, dialogMode);
  };

  // Fade
  function step107(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 41);
    desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    executeAction(cTID('Fade'), desc1, dialogMode);
  };

  step1();      // Curves
  step2();      // Color Balance
  step3();      // Channel Mixer
  step4();      // Color Balance
  step5();      // Levels
  step6();      // Set
  step7();      // Inverse
  step8();      // Curves
  step9();      // Curves
  step10();      // Set
  step11();      // Levels
  step12();      // Set
  step13();      // Copy
  step14();      // Assign Profile
  step15();      // Convert to Profile
  step16();      // Paste
  step17();      // Select
  step18();      // Set
  step19();      // Move
  step20();      // Color Range
  step21();      // Feather
  step22();      // Delete
  step23();      // Set
  step24();      // Color Range
  step25();      // Feather
  step26();      // Delete
  step27();      // Delete
  step28();      // Set
  step29();      // Color Range
  step30();      // Feather
  step31();      // Delete
  step32();      // Set
  step33();      // Color Range
  step34();      // Feather
  step35();      // Levels
  step36();      // Set
  step37();      // Flatten Image
  step38();      // Set
  step39();      // Copy
  step40();      // Set
  step41();      // Levels
  step42();      // Color Balance
  step43();      // Channel Mixer
  step44();      // Color Balance
  step45();      // Set
  step46();      // Inverse
  step47();      // Levels
  step48();      // Set
  step49();      // Selective Color
  step50();      // Fade
  step51();      // Curves
  step52();      // Set
  step53();      // Levels
  step54();      // Set
  step55();      // Levels
  step56();      // Set
  step57();      // Inverse
  step58();      // Levels
  step59();      // Set
  step60();      // Set
  step61();      // Set
  step62();      // Selective Color
  step63();      // Levels
  step64();      // Set
  step65();      // Levels
  step66();      // Set
  step67();      // Set
  step68();      // Levels
  step69();      // Set
  step70();      // Set
  step71();      // Levels
  step72();      // Set
  step73();      // Color Balance
  step74();      // Paste
  step75();      // Color Range
  step76();      // Inverse
  step77();      // Feather
  step78();      // Delete
  step79();      // Delete
  step80();      // Set
  step81();      // Flatten Image
  step82();      // Set
  step83();      // Color Balance
  step84();      // Set
  step85();      // Selective Color
  step86();      // Color Range
  step87();      // Set
  step88();      // Select
  step89();      // Feather
  step90();      // Color Balance
  step91();      // Set
  step92();      // Set
  step93();      // Color Balance
  step94();      // Hue/Saturation
  step95();      // Selective Color
  step96();      // Set
  step97();      // Set
  step98();      // Channel Mixer
  step99();      // Set
  step100();      // Curves
  step101();      // Curves
  step102();      // Color Balance
  step103();      // Hue/Saturation
  step104();      // Color Balance
  step105();      // Channel Mixer
  step106();      // Fade
  step107();      // Fade
},
};



//=========================================
//                    DS_Action_00_1.main
//=========================================
//

//DS_Action_00_1.main = function () {
  //DS_Action_00_1();
//};

//DS_Action_00_1.main();

// EOF

//"T01 DS_Action_00_1.jsx"
// EOF
