#target photoshop
//
// ChonVungSang2.jsx
//

//
// Generated Mon Aug 05 2019 20:16:12 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Chon Vung Sang 2 ==============
//
$._ext_TTC018={
run : function ChonVungSang2() {
  // Set
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Grn '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Set
},
};



//=========================================
//                    ChonVungSang2.main
//=========================================
//

//ChonVungSang2.main = function () {
 // ChonVungSang2();
//};

//ChonVungSang2.main();

// EOF

//"ChonVungSang2.jsx"
// EOF
