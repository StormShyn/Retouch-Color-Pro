#target photoshop
//
// 01 ChonVungSkin.jsx
//

//
// Generated Thu Aug 01 2019 09:51:34 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Chon Vung  Skin ==============
//
$._ext_01={
run : function ChonVungSkin() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('Lyr '));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Color Range
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 40);
    desc1.putEnumerated(cTID('Clrs'), cTID('Clrs'), sTID("skinTone"));
    desc1.putBoolean(sTID("UseFacesKey"), true);
    desc1.putInteger(sTID("colorModel"), 0);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Delete
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('Dlt '), desc1, dialogMode);
  };

  step1();      // Make
  step2();      // Color Range
  step3();      // Delete
},
};



//=========================================
//                    ChonVungSkin.main
//=========================================
//

//ChonVungSkin.main = function () {
  //ChonVungSkin();
//};

//ChonVungSkin.main();

// EOF

//"01 ChonVungSkin.jsx"
// EOF
