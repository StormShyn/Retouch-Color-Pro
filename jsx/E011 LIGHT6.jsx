#target photoshop
//
// LIGHT6.jsx
//

//
// Generated Wed Jul 31 2019 00:32:47 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== LIGHT 6 ==============
//
$._ext_E011={
run : function LIGHT6() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Salmon");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Scrn'));
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), 75.96);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Dmnd'));
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 500);
    var desc4 = new ActionDescriptor();
    desc4.putString(cTID('Nm  '), "Salmon");
    desc4.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc4.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc5 = new ActionDescriptor();
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Rd  '), 67.0038945972919);
    desc6.putDouble(cTID('Grn '), 45.003892108798);
    desc6.putDouble(cTID('Bl  '), 58.0000003427267);
    desc5.putObject(cTID('Clr '), sTID("RGBColor"), desc6);
    desc5.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc5.putInteger(cTID('Lctn'), 0);
    desc5.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc5);
    var desc7 = new ActionDescriptor();
    var desc8 = new ActionDescriptor();
    desc8.putDouble(cTID('Rd  '), 88.0000023543835);
    desc8.putDouble(cTID('Grn '), 48.0000009387732);
    desc8.putDouble(cTID('Bl  '), 59.003891274333);
    desc7.putObject(cTID('Clr '), sTID("RGBColor"), desc8);
    desc7.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc7.putInteger(cTID('Lctn'), 425);
    desc7.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc7);
    var desc9 = new ActionDescriptor();
    var desc10 = new ActionDescriptor();
    desc10.putDouble(cTID('Rd  '), 98.000001758337);
    desc10.putDouble(cTID('Grn '), 66.0000036656857);
    desc10.putDouble(cTID('Bl  '), 77.0038940012455);
    desc9.putObject(cTID('Clr '), sTID("RGBColor"), desc10);
    desc9.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc9.putInteger(cTID('Lctn'), 638);
    desc9.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc9);
    var desc11 = new ActionDescriptor();
    var desc12 = new ActionDescriptor();
    desc12.putDouble(cTID('Rd  '), 108.000001162291);
    desc12.putDouble(cTID('Grn '), 72.0000033080578);
    desc12.putDouble(cTID('Bl  '), 76.0000030696392);
    desc11.putObject(cTID('Clr '), sTID("RGBColor"), desc12);
    desc11.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc11.putInteger(cTID('Lctn'), 985);
    desc11.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc11);
    var desc13 = new ActionDescriptor();
    var desc14 = new ActionDescriptor();
    desc14.putDouble(cTID('Rd  '), 117.00389161706);
    desc14.putDouble(cTID('Grn '), 51.0038917511702);
    desc14.putDouble(cTID('Bl  '), 55.0038915127516);
    desc13.putObject(cTID('Clr '), sTID("RGBColor"), desc14);
    desc13.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc13.putInteger(cTID('Lctn'), 1488);
    desc13.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc13);
    var desc15 = new ActionDescriptor();
    var desc16 = new ActionDescriptor();
    desc16.putDouble(cTID('Rd  '), 157.99611479044);
    desc16.putDouble(cTID('Grn '), 74.0000031888485);
    desc16.putDouble(cTID('Bl  '), 74.0000031888485);
    desc15.putObject(cTID('Clr '), sTID("RGBColor"), desc16);
    desc15.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc15.putInteger(cTID('Lctn'), 1690);
    desc15.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc15);
    var desc17 = new ActionDescriptor();
    var desc18 = new ActionDescriptor();
    desc18.putDouble(cTID('Rd  '), 139.000006914139);
    desc18.putDouble(cTID('Grn '), 99.0038926899433);
    desc18.putDouble(cTID('Bl  '), 100.000001639128);
    desc17.putObject(cTID('Clr '), sTID("RGBColor"), desc18);
    desc17.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc17.putInteger(cTID('Lctn'), 2082);
    desc17.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc17);
    var desc19 = new ActionDescriptor();
    var desc20 = new ActionDescriptor();
    desc20.putDouble(cTID('Rd  '), 123.003891259432);
    desc20.putDouble(cTID('Grn '), 59.003891274333);
    desc20.putDouble(cTID('Bl  '), 59.003891274333);
    desc19.putObject(cTID('Clr '), sTID("RGBColor"), desc20);
    desc19.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc19.putInteger(cTID('Lctn'), 2373);
    desc19.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc19);
    var desc21 = new ActionDescriptor();
    var desc22 = new ActionDescriptor();
    desc22.putDouble(cTID('Rd  '), 149.996115267277);
    desc22.putDouble(cTID('Grn '), 69.0038944780827);
    desc22.putDouble(cTID('Bl  '), 66.0000036656857);
    desc21.putObject(cTID('Clr '), sTID("RGBColor"), desc22);
    desc21.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc21.putInteger(cTID('Lctn'), 2552);
    desc21.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc21);
    var desc23 = new ActionDescriptor();
    var desc24 = new ActionDescriptor();
    desc24.putDouble(cTID('Rd  '), 141.996115744114);
    desc24.putDouble(cTID('Grn '), 115.003891736269);
    desc24.putDouble(cTID('Bl  '), 106.0000012815);
    desc23.putObject(cTID('Clr '), sTID("RGBColor"), desc24);
    desc23.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc23.putInteger(cTID('Lctn'), 2652);
    desc23.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc23);
    var desc25 = new ActionDescriptor();
    var desc26 = new ActionDescriptor();
    desc26.putDouble(cTID('Rd  '), 147.996115386486);
    desc26.putDouble(cTID('Grn '), 115.003891736269);
    desc26.putDouble(cTID('Bl  '), 96.0000018775463);
    desc25.putObject(cTID('Clr '), sTID("RGBColor"), desc26);
    desc25.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc25.putInteger(cTID('Lctn'), 2977);
    desc25.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc25);
    var desc27 = new ActionDescriptor();
    var desc28 = new ActionDescriptor();
    desc28.putDouble(cTID('Rd  '), 149.000006318092);
    desc28.putDouble(cTID('Grn '), 83.0038936436176);
    desc28.putDouble(cTID('Bl  '), 71.0038943588734);
    desc27.putObject(cTID('Clr '), sTID("RGBColor"), desc28);
    desc27.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc27.putInteger(cTID('Lctn'), 3257);
    desc27.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc27);
    var desc29 = new ActionDescriptor();
    var desc30 = new ActionDescriptor();
    desc30.putDouble(cTID('Rd  '), 177.996113598347);
    desc30.putDouble(cTID('Grn '), 75.0038941204548);
    desc30.putDouble(cTID('Bl  '), 58.0000003427267);
    desc29.putObject(cTID('Clr '), sTID("RGBColor"), desc30);
    desc29.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc29.putInteger(cTID('Lctn'), 3559);
    desc29.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc29);
    var desc31 = new ActionDescriptor();
    var desc32 = new ActionDescriptor();
    desc32.putDouble(cTID('Rd  '), 175.996113717556);
    desc32.putDouble(cTID('Grn '), 49.0038918703794);
    desc32.putDouble(cTID('Bl  '), 42.000001296401);
    desc31.putObject(cTID('Clr '), sTID("RGBColor"), desc32);
    desc31.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc31.putInteger(cTID('Lctn'), 3738);
    desc31.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc31);
    var desc33 = new ActionDescriptor();
    var desc34 = new ActionDescriptor();
    desc34.putDouble(cTID('Rd  '), 167.000005245209);
    desc34.putDouble(cTID('Grn '), 43.0038922280073);
    desc34.putDouble(cTID('Bl  '), 35.0038927048445);
    desc33.putObject(cTID('Clr '), sTID("RGBColor"), desc34);
    desc33.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc33.putInteger(cTID('Lctn'), 4096);
    desc33.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc33);
    desc4.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc35 = new ActionDescriptor();
    desc35.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc35.putInteger(cTID('Lctn'), 0);
    desc35.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc35);
    var desc36 = new ActionDescriptor();
    desc36.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc36.putInteger(cTID('Lctn'), 4096);
    desc36.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc36);
    desc4.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc4);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  step1();      // Make
},
};



//=========================================
//                    LIGHT6.main
//=========================================
//

//LIGHT6.main = function () {
  //LIGHT6();
//};

//LIGHT6.main();

// EOF

//"LIGHT6.jsx"
// EOF
