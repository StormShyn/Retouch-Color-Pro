#target photoshop
//
// ATheNenTrang.jsx
//

//
// Generated Tue Aug 13 2019 19:04:13 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Nen Trang ==============
//
$._ext_ATN01={
run : function NenTrang() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    desc2.putBoolean(sTID("artboard"), false);
    desc2.putClass(cTID('Md  '), sTID("RGBColorMode"));
    desc2.putUnitDouble(cTID('Wdth'), cTID('#Rlt'), 368.4);
    desc2.putUnitDouble(cTID('Hght'), cTID('#Rlt'), 510.24);
    desc2.putUnitDouble(cTID('Rslt'), cTID('#Rsl'), 300);
    desc2.putDouble(sTID("pixelScaleFactor"), 1);
    desc2.putEnumerated(cTID('Fl  '), cTID('Fl  '), cTID('Wht '));
    desc2.putInteger(cTID('Dpth'), 8);
    desc2.putString(sTID("profile"), "Adobe RGB (1998)");
    var list1 = new ActionList();
    desc2.putList(cTID('Gdes'), list1);
    desc1.putObject(cTID('Nw  '), cTID('Dcmn'), desc2);
    desc1.putInteger(cTID('DocI'), 213);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  step1();      // Make
},
};



//=========================================
//                    NenTrang.main
//=========================================
//

//NenTrang.main = function () {
//  NenTrang();
//};

//NenTrang.main();

// EOF

//"ATheNenTrang.jsx"
// EOF
