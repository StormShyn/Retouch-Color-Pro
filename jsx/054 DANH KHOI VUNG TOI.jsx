﻿#target photoshop
//
// DANH KHOI VUNG TOI.jsx
//

//
// Generated Tue Jul 30 2019 11:07:00 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== DANH KHOI VUNG TOI ==============
//
$._ext_054={
run : function DANHKHOIVUNGTOI() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('Lyr '));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "TỐI (BRUN)");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Ovrl'));
    desc2.putBoolean(cTID('FlNt'), true);
    desc2.putEnumerated(cTID('Clr '), cTID('Clr '), cTID('Grn '));
    desc1.putObject(cTID('Usng'), cTID('Lyr '), desc2);
    desc1.putInteger(cTID('LyrI'), 125);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Select
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('SrTl'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Select
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('BrTl'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  step1();      // Make
  step2();      // Select
  step3();      // Select
},
};



//=========================================
//                    DANHKHOIVUNGTOI.main
//=========================================
//

//DANHKHOIVUNGTOI.main = function () {
  //DANHKHOIVUNGTOI();
//};

//DANHKHOIVUNGTOI.main();

// EOF

//"DANH KHOI VUNG TOI.jsx"
// EOF
