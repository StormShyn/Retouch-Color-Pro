#target photoshop
//
// KhoiFlipOverlayVertical.jsx
//

//
// Generated Thu Aug 15 2019 00:49:06 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Khoi Flip Overlay Vertical ==============
//
$._ext_khoi05={
run : function KhoiFlipOverlayVertical() {
  // Flip
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('Axis'), cTID('Ornt'), cTID('Vrtc'));
    executeAction(cTID('Flip'), desc1, dialogMode);
  };

  step1();      // Flip}
},
};



//=========================================
//                    KhoiFlipOverlayVertical.main
//=========================================
//

//KhoiFlipOverlayVertical.main = function () {
  //KhoiFlipOverlayVertical();
//};

//KhoiFlipOverlayVertical.main();

// EOF

//"KhoiFlipOverlayVertical.jsx"
// EOF
