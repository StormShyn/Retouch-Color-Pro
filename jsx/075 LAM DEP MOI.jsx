﻿#target photoshop
//
// LAM DEP MOI.jsx
//

//
// Generated Tue Jul 30 2019 15:51:15 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== LAM DEP MOI ==============
//
$._ext_075={
run : function LAMDEPMOI() {

  // Make
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('Lyr '));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "TEMP - Stamped Copy");
    desc1.putObject(cTID('Usng'), cTID('Lyr '), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Merge Visible
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('Dplc'), true);
    executeAction(sTID('mergeVisible'), desc1, dialogMode);
  };

  // Duplicate
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putString(cTID('Nm  '), "Luscious Lips | Highlights");
    desc1.putInteger(cTID('Vrsn'), 5);
    executeAction(cTID('Dplc'), desc1, dialogMode);
  };

  // Levels
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 1.49);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Gaussian Blur
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 10);
    executeAction(sTID('gaussianBlur'), desc1, dialogMode);
  };

  // High Pass
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 150);
    executeAction(sTID('highPass'), desc1, dialogMode);
  };

  // Desaturate
  function step8(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Dstt'), undefined, dialogMode);
  };

  // Curves
  function step9(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(130);
    list2.putInteger(131);
    list2.putInteger(132);
    list2.putInteger(133);
    list2.putInteger(134);
    list2.putInteger(135);
    list2.putInteger(136);
    list2.putInteger(137);
    list2.putInteger(138);
    list2.putInteger(139);
    list2.putInteger(140);
    list2.putInteger(141);
    list2.putInteger(142);
    list2.putInteger(143);
    list2.putInteger(144);
    list2.putInteger(145);
    list2.putInteger(146);
    list2.putInteger(147);
    list2.putInteger(148);
    list2.putInteger(149);
    list2.putInteger(150);
    list2.putInteger(151);
    list2.putInteger(152);
    list2.putInteger(153);
    list2.putInteger(154);
    list2.putInteger(155);
    list2.putInteger(156);
    list2.putInteger(157);
    list2.putInteger(158);
    list2.putInteger(159);
    list2.putInteger(160);
    list2.putInteger(161);
    list2.putInteger(162);
    list2.putInteger(163);
    list2.putInteger(164);
    list2.putInteger(165);
    list2.putInteger(166);
    list2.putInteger(167);
    list2.putInteger(168);
    list2.putInteger(169);
    list2.putInteger(170);
    list2.putInteger(171);
    list2.putInteger(172);
    list2.putInteger(173);
    list2.putInteger(174);
    list2.putInteger(175);
    list2.putInteger(176);
    list2.putInteger(177);
    list2.putInteger(178);
    list2.putInteger(179);
    list2.putInteger(180);
    list2.putInteger(181);
    list2.putInteger(182);
    list2.putInteger(183);
    list2.putInteger(184);
    list2.putInteger(185);
    list2.putInteger(186);
    list2.putInteger(187);
    list2.putInteger(188);
    list2.putInteger(189);
    list2.putInteger(190);
    list2.putInteger(191);
    list2.putInteger(192);
    list2.putInteger(193);
    list2.putInteger(194);
    list2.putInteger(195);
    list2.putInteger(196);
    list2.putInteger(197);
    list2.putInteger(198);
    list2.putInteger(199);
    list2.putInteger(200);
    list2.putInteger(201);
    list2.putInteger(202);
    list2.putInteger(203);
    list2.putInteger(204);
    list2.putInteger(205);
    list2.putInteger(206);
    list2.putInteger(207);
    list2.putInteger(208);
    list2.putInteger(209);
    list2.putInteger(210);
    list2.putInteger(211);
    list2.putInteger(212);
    list2.putInteger(213);
    list2.putInteger(214);
    list2.putInteger(215);
    list2.putInteger(216);
    list2.putInteger(217);
    list2.putInteger(218);
    list2.putInteger(219);
    list2.putInteger(220);
    list2.putInteger(221);
    list2.putInteger(222);
    list2.putInteger(223);
    list2.putInteger(224);
    list2.putInteger(225);
    list2.putInteger(226);
    list2.putInteger(227);
    list2.putInteger(228);
    list2.putInteger(229);
    list2.putInteger(230);
    list2.putInteger(231);
    list2.putInteger(232);
    list2.putInteger(233);
    list2.putInteger(234);
    list2.putInteger(235);
    list2.putInteger(236);
    list2.putInteger(237);
    list2.putInteger(238);
    list2.putInteger(239);
    list2.putInteger(240);
    list2.putInteger(241);
    list2.putInteger(242);
    list2.putInteger(243);
    list2.putInteger(244);
    list2.putInteger(245);
    list2.putInteger(246);
    list2.putInteger(247);
    list2.putInteger(248);
    list2.putInteger(249);
    list2.putInteger(250);
    list2.putInteger(251);
    list2.putInteger(252);
    list2.putInteger(253);
    list2.putInteger(254);
    list2.putInteger(255);
    desc2.putList(cTID('Mpng'), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Set
  function step10(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 70);
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Ovrl'));
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 100);
    desc2.putObject(cTID('Lefx'), cTID('Lefx'), desc3);
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Curves
  function step11(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Hrzn'), 0);
    desc3.putDouble(cTID('Vrtc'), 0);
    list2.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 120);
    desc4.putDouble(cTID('Vrtc'), 120);
    list2.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Hrzn'), 129);
    desc5.putDouble(cTID('Vrtc'), 129);
    list2.putObject(cTID('Pnt '), desc5);
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Hrzn'), 170);
    desc6.putDouble(cTID('Vrtc'), 243);
    list2.putObject(cTID('Pnt '), desc6);
    var desc7 = new ActionDescriptor();
    desc7.putDouble(cTID('Hrzn'), 186);
    desc7.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc7);
    desc2.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Make
  function step12(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Luscious Lips | Deep Color");
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindDefault"));
    desc2.putObject(cTID('Type'), cTID('Lvls'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Set
  function step13(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('AdjL'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    var list1 = new ActionList();
    var desc3 = new ActionDescriptor();
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc3.putReference(cTID('Chnl'), ref2);
    var list2 = new ActionList();
    list2.putInteger(80);
    list2.putInteger(255);
    desc3.putList(cTID('Inpt'), list2);
    desc3.putDouble(cTID('Gmm '), 1.4);
    list1.putObject(cTID('LvlA'), desc3);
    desc2.putList(cTID('Adjs'), list1);
    desc1.putObject(cTID('T   '), cTID('Lvls'), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Make
  function step14(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('Lyr '));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Luscious Lips | Darkening");
    desc1.putObject(cTID('Usng'), cTID('Lyr '), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Merge Visible
  function step15(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('Dplc'), true);
    executeAction(sTID('mergeVisible'), desc1, dialogMode);
  };

  // Delete
  function step16(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Luscious Lips | Deep Color");
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('Dlt '), desc1, dialogMode);
  };

  // Delete
  function step17(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Luscious Lips | Highlights");
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('Dlt '), desc1, dialogMode);
  };

  // Apply Image
  function step18(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), sTID("RGB"));
    ref1.putName(cTID('Lyr '), "TEMP - Stamped Copy");
    desc2.putReference(cTID('T   '), ref1);
    desc2.putEnumerated(cTID('Clcl'), cTID('Clcn'), cTID('Sbtr'));
    desc2.putDouble(cTID('Scl '), 1);
    desc2.putInteger(cTID('Ofst'), 128);
    desc1.putObject(cTID('With'), cTID('Clcl'), desc2);
    executeAction(sTID('applyImageEvent'), desc1, dialogMode);
  };

  // Delete
  function step19(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "TEMP - Stamped Copy");
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('Dlt '), desc1, dialogMode);
  };

  // Duplicate
  function step20(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putString(cTID('Nm  '), "Luscious Lips | Lightening");
    desc1.putInteger(cTID('Vrsn'), 5);
    executeAction(cTID('Dplc'), desc1, dialogMode);
  };

  // Curves
  function step21(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(130);
    list2.putInteger(131);
    list2.putInteger(132);
    list2.putInteger(133);
    list2.putInteger(134);
    list2.putInteger(135);
    list2.putInteger(136);
    list2.putInteger(137);
    list2.putInteger(138);
    list2.putInteger(139);
    list2.putInteger(140);
    list2.putInteger(141);
    list2.putInteger(142);
    list2.putInteger(143);
    list2.putInteger(144);
    list2.putInteger(145);
    list2.putInteger(146);
    list2.putInteger(147);
    list2.putInteger(148);
    list2.putInteger(149);
    list2.putInteger(150);
    list2.putInteger(151);
    list2.putInteger(152);
    list2.putInteger(153);
    list2.putInteger(154);
    list2.putInteger(155);
    list2.putInteger(156);
    list2.putInteger(157);
    list2.putInteger(158);
    list2.putInteger(159);
    list2.putInteger(160);
    list2.putInteger(161);
    list2.putInteger(162);
    list2.putInteger(163);
    list2.putInteger(164);
    list2.putInteger(165);
    list2.putInteger(166);
    list2.putInteger(167);
    list2.putInteger(168);
    list2.putInteger(169);
    list2.putInteger(170);
    list2.putInteger(171);
    list2.putInteger(172);
    list2.putInteger(173);
    list2.putInteger(174);
    list2.putInteger(175);
    list2.putInteger(176);
    list2.putInteger(177);
    list2.putInteger(178);
    list2.putInteger(179);
    list2.putInteger(180);
    list2.putInteger(181);
    list2.putInteger(182);
    list2.putInteger(183);
    list2.putInteger(184);
    list2.putInteger(185);
    list2.putInteger(186);
    list2.putInteger(187);
    list2.putInteger(188);
    list2.putInteger(189);
    list2.putInteger(190);
    list2.putInteger(191);
    list2.putInteger(192);
    list2.putInteger(193);
    list2.putInteger(194);
    list2.putInteger(195);
    list2.putInteger(196);
    list2.putInteger(197);
    list2.putInteger(198);
    list2.putInteger(199);
    list2.putInteger(200);
    list2.putInteger(201);
    list2.putInteger(202);
    list2.putInteger(203);
    list2.putInteger(204);
    list2.putInteger(205);
    list2.putInteger(206);
    list2.putInteger(207);
    list2.putInteger(208);
    list2.putInteger(209);
    list2.putInteger(210);
    list2.putInteger(211);
    list2.putInteger(212);
    list2.putInteger(213);
    list2.putInteger(214);
    list2.putInteger(215);
    list2.putInteger(216);
    list2.putInteger(217);
    list2.putInteger(218);
    list2.putInteger(219);
    list2.putInteger(220);
    list2.putInteger(221);
    list2.putInteger(222);
    list2.putInteger(223);
    list2.putInteger(224);
    list2.putInteger(225);
    list2.putInteger(226);
    list2.putInteger(227);
    list2.putInteger(228);
    list2.putInteger(229);
    list2.putInteger(230);
    list2.putInteger(231);
    list2.putInteger(232);
    list2.putInteger(233);
    list2.putInteger(234);
    list2.putInteger(235);
    list2.putInteger(236);
    list2.putInteger(237);
    list2.putInteger(238);
    list2.putInteger(239);
    list2.putInteger(240);
    list2.putInteger(241);
    list2.putInteger(242);
    list2.putInteger(243);
    list2.putInteger(244);
    list2.putInteger(245);
    list2.putInteger(246);
    list2.putInteger(247);
    list2.putInteger(248);
    list2.putInteger(249);
    list2.putInteger(250);
    list2.putInteger(251);
    list2.putInteger(252);
    list2.putInteger(253);
    list2.putInteger(254);
    list2.putInteger(255);
    desc2.putList(cTID('Mpng'), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Set
  function step22(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), sTID("linearLight"));
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Select
  function step23(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Luscious Lips | Darkening");
    desc1.putReference(cTID('null'), ref1);
    desc1.putBoolean(cTID('MkVs'), false);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Curves
  function step24(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(1);
    list2.putInteger(2);
    list2.putInteger(3);
    list2.putInteger(4);
    list2.putInteger(5);
    list2.putInteger(6);
    list2.putInteger(7);
    list2.putInteger(8);
    list2.putInteger(9);
    list2.putInteger(10);
    list2.putInteger(11);
    list2.putInteger(12);
    list2.putInteger(13);
    list2.putInteger(14);
    list2.putInteger(15);
    list2.putInteger(16);
    list2.putInteger(17);
    list2.putInteger(18);
    list2.putInteger(19);
    list2.putInteger(20);
    list2.putInteger(21);
    list2.putInteger(22);
    list2.putInteger(23);
    list2.putInteger(24);
    list2.putInteger(25);
    list2.putInteger(26);
    list2.putInteger(27);
    list2.putInteger(28);
    list2.putInteger(29);
    list2.putInteger(30);
    list2.putInteger(31);
    list2.putInteger(32);
    list2.putInteger(33);
    list2.putInteger(34);
    list2.putInteger(35);
    list2.putInteger(36);
    list2.putInteger(37);
    list2.putInteger(38);
    list2.putInteger(39);
    list2.putInteger(40);
    list2.putInteger(41);
    list2.putInteger(42);
    list2.putInteger(43);
    list2.putInteger(44);
    list2.putInteger(45);
    list2.putInteger(46);
    list2.putInteger(47);
    list2.putInteger(48);
    list2.putInteger(49);
    list2.putInteger(50);
    list2.putInteger(51);
    list2.putInteger(52);
    list2.putInteger(53);
    list2.putInteger(54);
    list2.putInteger(55);
    list2.putInteger(56);
    list2.putInteger(57);
    list2.putInteger(58);
    list2.putInteger(59);
    list2.putInteger(60);
    list2.putInteger(61);
    list2.putInteger(62);
    list2.putInteger(63);
    list2.putInteger(64);
    list2.putInteger(65);
    list2.putInteger(66);
    list2.putInteger(67);
    list2.putInteger(68);
    list2.putInteger(69);
    list2.putInteger(70);
    list2.putInteger(71);
    list2.putInteger(72);
    list2.putInteger(73);
    list2.putInteger(74);
    list2.putInteger(75);
    list2.putInteger(76);
    list2.putInteger(77);
    list2.putInteger(78);
    list2.putInteger(79);
    list2.putInteger(80);
    list2.putInteger(81);
    list2.putInteger(82);
    list2.putInteger(83);
    list2.putInteger(84);
    list2.putInteger(85);
    list2.putInteger(86);
    list2.putInteger(87);
    list2.putInteger(88);
    list2.putInteger(89);
    list2.putInteger(90);
    list2.putInteger(91);
    list2.putInteger(92);
    list2.putInteger(93);
    list2.putInteger(94);
    list2.putInteger(95);
    list2.putInteger(96);
    list2.putInteger(97);
    list2.putInteger(98);
    list2.putInteger(99);
    list2.putInteger(100);
    list2.putInteger(101);
    list2.putInteger(102);
    list2.putInteger(103);
    list2.putInteger(104);
    list2.putInteger(105);
    list2.putInteger(106);
    list2.putInteger(107);
    list2.putInteger(108);
    list2.putInteger(109);
    list2.putInteger(110);
    list2.putInteger(111);
    list2.putInteger(112);
    list2.putInteger(113);
    list2.putInteger(114);
    list2.putInteger(115);
    list2.putInteger(116);
    list2.putInteger(117);
    list2.putInteger(118);
    list2.putInteger(119);
    list2.putInteger(120);
    list2.putInteger(121);
    list2.putInteger(122);
    list2.putInteger(123);
    list2.putInteger(124);
    list2.putInteger(125);
    list2.putInteger(126);
    list2.putInteger(127);
    list2.putInteger(128);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    list2.putInteger(129);
    desc2.putList(cTID('Mpng'), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Set
  function step25(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step26(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 80);
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Select
  function step27(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Luscious Lips | Lightening");
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(sTID("selectionModifier"), sTID("selectionModifierType"), sTID("addToSelectionContinuous"));
    desc1.putBoolean(cTID('MkVs'), false);
    var list1 = new ActionList();
    list1.putInteger(20);
    list1.putInteger(21);
    list1.putInteger(23);
    desc1.putList(cTID('LyrI'), list1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Make
  function step28(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("layerSection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('From'), ref2);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Làm đẹp môi");
    desc2.putEnumerated(cTID('Clr '), cTID('Clr '), cTID('Orng'));
    desc1.putObject(cTID('Usng'), sTID("layerSection"), desc2);
    desc1.putInteger(sTID("layerSectionStart"), 23);
    desc1.putInteger(sTID("layerSectionEnd"), 24);
    desc1.putString(cTID('Nm  '), "Làm đẹp môi");
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step29(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putClass(cTID('Nw  '), cTID('Chnl'));
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Msk '));
    desc1.putReference(cTID('At  '), ref1);
    desc1.putEnumerated(cTID('Usng'), cTID('UsrM'), cTID('HdAl'));
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Select
  function step30(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('PbTl'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Reset
  function step31(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Clr '), cTID('Clrs'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('Rset'), desc1, dialogMode);
  };

  // Set
  function step32(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 50);
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step2();      // Make
  step3();      // Merge Visible
  step4();      // Duplicate
  step5();      // Levels
  step6();      // Gaussian Blur
  step7();      // High Pass
  step8();      // Desaturate
  step9();      // Curves
  step10();      // Set
  step11();      // Curves
  step12();      // Make
  step13();      // Set
  step14();      // Make
  step15();      // Merge Visible
  step16();      // Delete
  step17();      // Delete
  step18();      // Apply Image
  step19();      // Delete
  step20();      // Duplicate
  step21();      // Curves
  step22();      // Set
  step23();      // Select
  step24();      // Curves
  step25();      // Set
  step26();      // Set
  step27();      // Select
  step28();      // Make
  step29();      // Make
  step30();      // Select
  step31();      // Reset
  step32();      // Set
},
};



//=========================================
//                    LAMDEPMOI.main
//=========================================
//

//LAMDEPMOI.main = function () {
  //LAMDEPMOI();
//};

//LAMDEPMOI.main();

// EOF

//"LAM DEP MOI.jsx"
// EOF
