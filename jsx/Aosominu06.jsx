#target photoshop
//
// Aovest01.jsx
//

//
// Generated Tue Aug 13 2019 00:29:57 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Action 8 ==============
//
$._ext_TAsn006={
run :  function Action8() {
  // Place
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Idnt'), 11);
    desc1.putPath(cTID('null'), new File("/c/Program Files (x86)/Common Files/Adobe/CEP/extensions/DS Phu Retouch Color Pro RCP 2019/Anh The/Ao So Mi Nu/06.psd"));
    desc1.putEnumerated(cTID('FTcs'), cTID('QCSt'), sTID("QCSAverage"));
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 0);
    desc2.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 0);
    desc1.putObject(cTID('Ofst'), cTID('Ofst'), desc2);
    executeAction(cTID('Plc '), desc1, dialogMode);
  };

  step1();      // Place
},
};



//=========================================
//                    Action8.main
//=========================================
//

//Action8.main = function () {
  //Action8();
//};

//Action8.main();

// EOF

//"Aovest01.jsx"
// EOF
