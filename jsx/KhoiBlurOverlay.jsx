#target photoshop
//
// KhoiBlurOverlay.jsx
//

//
// Generated Thu Aug 15 2019 00:27:31 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Khoi Blur Overlay ==============
//
$._ext_khoi03={
run : function KhoiBlurOverlay() {
  // Gaussian Blur
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 14.8);
    executeAction(sTID('gaussianBlur'), desc1, dialogMode);
  };

  step1();      // Gaussian Blur
},
};



//=========================================
//                    KhoiBlurOverlay.main
//=========================================
//

//KhoiBlurOverlay.main = function () {
  //KhoiBlurOverlay();
//};

//KhoiBlurOverlay.main();

// EOF

//"KhoiBlurOverlay.jsx"
// EOF
