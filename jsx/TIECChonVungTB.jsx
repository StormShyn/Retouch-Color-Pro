#target photoshop
//
// ChonVungTB.jsx
//

//
// Generated Mon Aug 05 2019 20:14:58 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Chon Vung TB ==============
//
$._ext_TTC016={
run : function ChonVungTB() {
  // Set
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Bl  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Set
},
};



//=========================================
//                    ChonVungTB.main
//=========================================
//

//ChonVungTB.main = function () {
 // ChonVungTB();
//};

//ChonVungTB.main();

// EOF

//"ChonVungTB.jsx"
// EOF
