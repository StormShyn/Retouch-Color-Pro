
#target photoshop
//
// Make_It_Glow.jsx
//

//
// Generated Tue Oct 18 2016 23:57:57 GMT+0500
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Make-It-Glow ==============
//
$._ext_HDR06={
run : function Make_It_Glow() {
  // Select
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), sTID("RGB"));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Set
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Make
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('Lyr '));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Smart Glow");
    desc1.putObject(cTID('Usng'), cTID('Lyr '), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Move
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Frnt'));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('move'), desc1, dialogMode);
  };

  // Move
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Frnt'));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('move'), desc1, dialogMode);
  };

  // Merge Visible
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('Dplc'), true);
    executeAction(sTID('mergeVisible'), desc1, dialogMode);
  };

  // Convert to Smart Object
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('newPlacedLayer'), undefined, dialogMode);
  };

  // Gaussian Blur
  function step8(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 35);
    executeAction(sTID('gaussianBlur'), desc1, dialogMode);
  };

  // Set
  function step9(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putIndex(sTID("filterFX"), 1);
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc3.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
    desc2.putObject(sTID("blendOptions"), sTID("blendOptions"), desc3);
    desc1.putObject(sTID("filterFX"), sTID("filterFX"), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Shadow/Highlight
  function step10(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 35);
    desc2.putUnitDouble(cTID('Wdth'), cTID('#Prc'), 30);
    desc2.putInteger(cTID('Rds '), 30);
    desc1.putObject(cTID('sdwM'), sTID("adaptCorrectTones"), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 0);
    desc3.putUnitDouble(cTID('Wdth'), cTID('#Prc'), 50);
    desc3.putInteger(cTID('Rds '), 30);
    desc1.putObject(cTID('hglM'), sTID("adaptCorrectTones"), desc3);
    desc1.putDouble(cTID('BlcC'), 0.01);
    desc1.putDouble(cTID('WhtC'), 0.01);
    desc1.putInteger(cTID('Cntr'), 0);
    desc1.putInteger(cTID('ClrC'), 20);
    executeAction(sTID('adaptCorrect'), desc1, dialogMode);
  };

  step1();      // Select
  step2();      // Set
  step3();      // Make
  step4();      // Move
  step5();      // Move
  step6();      // Merge Visible
  step7();      // Convert to Smart Object
  step8(true, true);      // Gaussian Blur
  step9();      // Set
  step10();      // Shadow/Highlight
},
};



//=========================================
//                    Make_It_Glow.main
//=========================================
//

//Make_It_Glow.main = function () {
 // Make_It_Glow();
//};

//Make_It_Glow.main();

// EOF

//"Make_It_Glow.jsx"
// EOF
    
         