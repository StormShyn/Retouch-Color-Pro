#target photoshop
//
// skyBlueSkyFill.jsx
//

//
// Generated Sun Aug 11 2019 15:35:20 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== sky Blue Sky Fill ==============
//
$._ext_sky15={
run : function skyBlueSkyFill() {
  // Select
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Background");
    desc1.putReference(cTID('null'), ref1);
    desc1.putBoolean(cTID('MkVs'), false);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Make
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putBoolean(cTID('Rvrs'), true);
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), 90);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Lnr '));
    var desc4 = new ActionDescriptor();
    desc4.putString(cTID('Nm  '), "Custom");
    desc4.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc4.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc5 = new ActionDescriptor();
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Rd  '), 171.505833864212);
    desc6.putDouble(cTID('Grn '), 209.529177546501);
    desc6.putDouble(cTID('Bl  '), 222.000001966953);
    desc5.putObject(cTID('Clr '), sTID("RGBColor"), desc6);
    desc5.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc5.putInteger(cTID('Lctn'), 0);
    desc5.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc5);
    var desc7 = new ActionDescriptor();
    var desc8 = new ActionDescriptor();
    desc8.putDouble(cTID('Rd  '), 231.315171718597);
    desc8.putDouble(cTID('Grn '), 237.000001072884);
    desc8.putDouble(cTID('Bl  '), 140.342414081097);
    desc7.putObject(cTID('Clr '), sTID("RGBColor"), desc8);
    desc7.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc7.putInteger(cTID('Lctn'), 4096);
    desc7.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc7);
    desc4.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc9 = new ActionDescriptor();
    desc9.putUnitDouble(cTID('Opct'), cTID('#Prc'), 78.8235294117647);
    desc9.putInteger(cTID('Lctn'), 0);
    desc9.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc9);
    var desc10 = new ActionDescriptor();
    desc10.putUnitDouble(cTID('Opct'), cTID('#Prc'), 12.156862745098);
    desc10.putInteger(cTID('Lctn'), 1526);
    desc10.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc10);
    var desc11 = new ActionDescriptor();
    desc11.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc11.putInteger(cTID('Lctn'), 4096);
    desc11.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc11);
    desc4.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc4);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Set
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(sTID("contentLayer"), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putBoolean(cTID('Rvrs'), true);
    desc2.putUnitDouble(cTID('Angl'), cTID('#Ang'), 90);
    desc2.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Lnr '));
    var desc3 = new ActionDescriptor();
    desc3.putString(cTID('Nm  '), "Custom");
    desc3.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc3.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc4 = new ActionDescriptor();
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Rd  '), 181.828800737858);
    desc5.putDouble(cTID('Grn '), 224.883272051811);
    desc5.putDouble(cTID('Bl  '), 239.000000953674);
    desc4.putObject(cTID('Clr '), sTID("RGBColor"), desc5);
    desc4.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc4.putInteger(cTID('Lctn'), 0);
    desc4.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc4);
    var desc6 = new ActionDescriptor();
    var desc7 = new ActionDescriptor();
    desc7.putDouble(cTID('Rd  '), 255);
    desc7.putDouble(cTID('Grn '), 255);
    desc7.putDouble(cTID('Bl  '), 255);
    desc6.putObject(cTID('Clr '), sTID("RGBColor"), desc7);
    desc6.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc6.putInteger(cTID('Lctn'), 4096);
    desc6.putInteger(cTID('Mdpn'), 33);
    list1.putObject(cTID('Clrt'), desc6);
    desc3.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc8 = new ActionDescriptor();
    desc8.putUnitDouble(cTID('Opct'), cTID('#Prc'), 78.8235294117647);
    desc8.putInteger(cTID('Lctn'), 0);
    desc8.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc8);
    var desc9 = new ActionDescriptor();
    desc9.putUnitDouble(cTID('Opct'), cTID('#Prc'), 12.156862745098);
    desc9.putInteger(cTID('Lctn'), 1270);
    desc9.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc9);
    var desc10 = new ActionDescriptor();
    desc10.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc10.putInteger(cTID('Lctn'), 4096);
    desc10.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc10);
    desc3.putList(cTID('Trns'), list2);
    desc2.putObject(cTID('Grad'), cTID('Grdn'), desc3);
    desc1.putObject(cTID('T   '), sTID("gradientLayer"), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Mltp'));
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 82);
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Blue Sky Fill");
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Move
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Frnt'));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('move'), desc1, dialogMode);
  };

  step1();      // Select
  step2();      // Make
  step3();      // Set
  step4();      // Set
  step5();      // Set
  step6();      // Set
  step7();      // Move
},
};



//=========================================
//                    skyBlueSkyFill.main
//=========================================
//

//skyBlueSkyFill.main = function () {
 // skyBlueSkyFill();
//};

//skyBlueSkyFill.main();

// EOF

//"skyBlueSkyFill.jsx"
// EOF
