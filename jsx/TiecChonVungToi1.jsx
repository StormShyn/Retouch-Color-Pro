#target photoshop
//
// ChonVungToi1.jsx
//

//
// Generated Tue Aug 06 2019 00:29:32 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Chon Vung Toi 1 ==============
//
$._ext_TTC035={
run : function ChonVungToi1() {
  // Set
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putName(cTID('Chnl'), "New Selection");
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Set
},
};



//=========================================
//                    ChonVungToi1.main
//=========================================
//

//ChonVungToi1.main = function () {
  //ChonVungToi1();
//};

//ChonVungToi1.main();

// EOF

//"ChonVungToi1.jsx"
// EOF
