#target photoshop
//
// TIEC tanghat.jsx
//

//
// Generated Sun Aug 04 2019 20:58:24 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== tang hat ==============
//
$._ext_TTH2={
run : function tanghat() {
  // Custom
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Scl '), 47);
    desc1.putInteger(cTID('Ofst'), 0);
    var list1 = new ActionList();
    list1.putInteger(-4);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(-4);
    list1.putInteger(0);
    list1.putInteger(-4);
    list1.putInteger(0);
    list1.putInteger(-4);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(80);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(-4);
    list1.putInteger(0);
    list1.putInteger(-4);
    list1.putInteger(0);
    list1.putInteger(-4);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(-4);
    desc1.putList(cTID('Mtrx'), list1);
    executeAction(cTID('Cstm'), desc1, dialogMode);
  };

  step1();      // Custom
},
};



//=========================================
//                    tanghat.main
//=========================================
//

//tanghat.main = function () {
  //tanghat();
//};

//tanghat.main();

// EOF

//"TIEC tanghat.jsx"
// EOF
