#target photoshop
//
// AtheThemHatPhong.jsx
//

//
// Generated Tue Aug 13 2019 19:13:08 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Them Hat Phong ==============
//
$._ext_ATN05={
run : function ThemHatPhong() {
  // Set
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Trsp'));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Add Noise
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Dstr'), cTID('Dstr'), cTID('Gsn '));
    desc1.putUnitDouble(cTID('Nose'), cTID('#Prc'), 2);
    desc1.putBoolean(cTID('Mnch'), true);
    desc1.putInteger(cTID('FlRs'), 8476431);
    executeAction(sTID('addNoise'), desc1, dialogMode);
  };

  // Set
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Set
  step2();      // Add Noise
  step3();      // Set
},
};



//=========================================
//                    ThemHatPhong.main
//=========================================
//

//ThemHatPhong.main = function () {
  //ThemHatPhong();
//};

//ThemHatPhong.main();

// EOF

//"AtheThemHatPhong.jsx"
// EOF
