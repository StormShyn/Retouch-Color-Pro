#target photoshop
//
// KhoiBlurOverlaySlightly.jsx
//

//
// Generated Thu Aug 15 2019 00:27:18 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Khoi Blur Overlay Slightly ==============
//
$._ext_khoi02={
run : function KhoiBlurOverlaySlightly() {
  // Gaussian Blur
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 5);
    executeAction(sTID('gaussianBlur'), desc1, dialogMode);
  };

  step1();      // Gaussian Blur
},
};



//=========================================
//                    KhoiBlurOverlaySlightly.main
//=========================================
//

//KhoiBlurOverlaySlightly.main = function () {
  //KhoiBlurOverlaySlightly();
//};

//KhoiBlurOverlaySlightly.main();

// EOF

//"KhoiBlurOverlaySlightly.jsx"
// EOF
