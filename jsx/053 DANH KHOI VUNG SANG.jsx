﻿#target photoshop
//
// DANH KHOI VUNG SANG.jsx
//

//
// Generated Tue Jul 30 2019 11:07:40 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== DANH KHOI VUNG SANG ==============
//
$._ext_053={
run : function DANHKHOIVUNGSANG() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('Lyr '));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "SÁNG(DOG)");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Ovrl'));
    desc2.putBoolean(cTID('FlNt'), true);
    desc2.putEnumerated(cTID('Clr '), cTID('Clr '), cTID('Grn '));
    desc1.putObject(cTID('Usng'), cTID('Lyr '), desc2);
    desc1.putInteger(cTID('LyrI'), 126);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Select
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('DdTl'));
    desc1.putReference(cTID('null'), ref1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Set
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "SÁNG(DODG)");
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Make
  step2();      // Select
  step3();      // Set
},
};



//=========================================
//                    DANHKHOIVUNGSANG.main
//=========================================
//

//DANHKHOIVUNGSANG.main = function () {
  //DANHKHOIVUNGSANG();
//};

//DANHKHOIVUNGSANG.main();

// EOF

//"DANH KHOI VUNG SANG.jsx"
// EOF
