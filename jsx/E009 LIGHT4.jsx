#target photoshop
//
// LIGHT4.jsx
//

//
// Generated Wed Jul 31 2019 00:32:28 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== LIGHT 4 ==============
//
$._ext_E009={
run : function LIGHT4() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Jaded");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Scrn'));
    var desc3 = new ActionDescriptor();
    desc3.putBoolean(cTID('Rvrs'), true);
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), -4.09);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Rflc'));
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 150);
    var desc4 = new ActionDescriptor();
    desc4.putString(cTID('Nm  '), "Jaded");
    desc4.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc4.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc5 = new ActionDescriptor();
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Rd  '), 217.000002264977);
    desc6.putDouble(cTID('Grn '), 108.420235812664);
    desc6.putDouble(cTID('Bl  '), 87.0038934051991);
    desc5.putObject(cTID('Clr '), sTID("RGBColor"), desc6);
    desc5.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc5.putInteger(cTID('Lctn'), 0);
    desc5.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc5);
    var desc7 = new ActionDescriptor();
    var desc8 = new ActionDescriptor();
    desc8.putDouble(cTID('Rd  '), 123.198440819979);
    desc8.putDouble(cTID('Grn '), 187.000004053116);
    desc8.putDouble(cTID('Bl  '), 178.739292919636);
    desc7.putObject(cTID('Clr '), sTID("RGBColor"), desc8);
    desc7.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc7.putInteger(cTID('Lctn'), 1319);
    desc7.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc7);
    var desc9 = new ActionDescriptor();
    var desc10 = new ActionDescriptor();
    desc10.putDouble(cTID('Rd  '), 33.054475709796);
    desc10.putDouble(cTID('Grn '), 26.0000003501773);
    desc10.putDouble(cTID('Bl  '), 86.0000024735928);
    desc9.putObject(cTID('Clr '), sTID("RGBColor"), desc10);
    desc9.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc9.putInteger(cTID('Lctn'), 2851);
    desc9.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc9);
    var desc11 = new ActionDescriptor();
    var desc12 = new ActionDescriptor();
    desc12.putDouble(cTID('Rd  '), 41.9993591308594);
    desc12.putDouble(cTID('Grn '), 41.9993591308594);
    desc12.putDouble(cTID('Bl  '), 77.9988098144531);
    desc11.putObject(cTID('Clr '), sTID("RGBColor"), desc12);
    desc11.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc11.putInteger(cTID('Lctn'), 4096);
    desc11.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc11);
    desc4.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc13 = new ActionDescriptor();
    desc13.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc13.putInteger(cTID('Lctn'), 702);
    desc13.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc13);
    var desc14 = new ActionDescriptor();
    desc14.putUnitDouble(cTID('Opct'), cTID('#Prc'), 65.0980392156863);
    desc14.putInteger(cTID('Lctn'), 4096);
    desc14.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc14);
    desc4.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc4);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  step1();      // Make
},
};



//=========================================
//                    LIGHT4.main
//=========================================
//

//LIGHT4.main = function () {
  //LIGHT4();
//};

//LIGHT4.main();

// EOF

//"LIGHT4.jsx"
// EOF
