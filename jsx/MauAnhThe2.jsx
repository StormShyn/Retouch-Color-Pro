#target photoshop
//
// MauAnhThe2.jsx
//

//
// Generated Tue Aug 13 2019 01:56:40 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Mau Anh The 2 ==============
//
$._ext_AT03={
run : function MauAnhThe2() {
  // Selective Color
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Ylws'));
    desc2.putUnitDouble(cTID('Mgnt'), cTID('#Prc'), 41);
    desc2.putUnitDouble(cTID('Ylw '), cTID('#Prc'), -15);
    desc2.putUnitDouble(cTID('Blck'), cTID('#Prc'), -77);
    list1.putObject(cTID('ClrC'), desc2);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Curves
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Hrzn'), 0);
    desc3.putDouble(cTID('Vrtc'), 0);
    list2.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 118);
    desc4.putDouble(cTID('Vrtc'), 149);
    list2.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Hrzn'), 255);
    desc5.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc5);
    desc2.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Unsharp Mask
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Amnt'), cTID('#Prc'), 97);
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 3.1);
    desc1.putInteger(cTID('Thsh'), 0);
    executeAction(sTID('unsharpMask'), desc1, dialogMode);
  };

  // Fade
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 84);
    desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    executeAction(cTID('Fade'), desc1, dialogMode);
  };

  // Assign Profile
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Dcmn'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putString(sTID("profile"), "Adobe RGB (1998)");
    executeAction(sTID('assignProfile'), desc1, dialogMode);
  };

  // Curves
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Hrzn'), 0);
    desc3.putDouble(cTID('Vrtc'), 0);
    list2.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 147);
    desc4.putDouble(cTID('Vrtc'), 153);
    list2.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Hrzn'), 255);
    desc5.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc5);
    desc2.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  step1();      // Selective Color
  step2();      // Curves
  step3();      // Unsharp Mask
  step4();      // Fade
  step5();      // Assign Profile
  step6();      // Curves
},
};



//=========================================
//                    MauAnhThe2.main
//=========================================
//

//MauAnhThe2.main = function () {
  //MauAnhThe2();
//};

//MauAnhThe2.main();

// EOF

//"MauAnhThe2.jsx"
// EOF
