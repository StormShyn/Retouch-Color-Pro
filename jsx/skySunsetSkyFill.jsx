#target photoshop
//
// skySunsetSkyFill.jsx
//

//
// Generated Sun Aug 11 2019 15:37:36 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== sky Sunset Sky Fill ==============
//
$._ext_sky16={
run : function skySunsetSkyFill() {
  // Select
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Background");
    desc1.putReference(cTID('null'), ref1);
    desc1.putBoolean(cTID('MkVs'), false);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Make
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putBoolean(cTID('Rvrs'), true);
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), 90);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Lnr '));
    var desc4 = new ActionDescriptor();
    desc4.putString(cTID('Nm  '), "Custom");
    desc4.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc4.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc5 = new ActionDescriptor();
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Rd  '), 164.789876639843);
    desc6.putDouble(cTID('Grn '), 195.968875586987);
    desc6.putDouble(cTID('Bl  '), 203.000003099442);
    desc5.putObject(cTID('Clr '), sTID("RGBColor"), desc6);
    desc5.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc5.putInteger(cTID('Lctn'), 20);
    desc5.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc5);
    var desc7 = new ActionDescriptor();
    var desc8 = new ActionDescriptor();
    desc8.putDouble(cTID('Rd  '), 195.023349523544);
    desc8.putDouble(cTID('Grn '), 173.050587773323);
    desc8.putDouble(cTID('Bl  '), 197.000003457069);
    desc7.putObject(cTID('Clr '), sTID("RGBColor"), desc8);
    desc7.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc7.putInteger(cTID('Lctn'), 840);
    desc7.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc7);
    var desc9 = new ActionDescriptor();
    var desc10 = new ActionDescriptor();
    desc10.putDouble(cTID('Rd  '), 222.000001966953);
    desc10.putDouble(cTID('Grn '), 184.260700643063);
    desc10.putDouble(cTID('Bl  '), 146.260702908039);
    desc9.putObject(cTID('Clr '), sTID("RGBColor"), desc10);
    desc9.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc9.putInteger(cTID('Lctn'), 1403);
    desc9.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc9);
    var desc11 = new ActionDescriptor();
    var desc12 = new ActionDescriptor();
    desc12.putDouble(cTID('Rd  '), 255);
    desc12.putDouble(cTID('Grn '), 223.277478218079);
    desc12.putDouble(cTID('Bl  '), 132.842893302441);
    desc11.putObject(cTID('Clr '), sTID("RGBColor"), desc12);
    desc11.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc11.putInteger(cTID('Lctn'), 2068);
    desc11.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc11);
    var desc13 = new ActionDescriptor();
    var desc14 = new ActionDescriptor();
    desc14.putDouble(cTID('Rd  '), 254.000000059605);
    desc14.putDouble(cTID('Grn '), 239.723740518093);
    desc14.putDouble(cTID('Bl  '), 196.229571998119);
    desc13.putObject(cTID('Clr '), sTID("RGBColor"), desc14);
    desc13.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc13.putInteger(cTID('Lctn'), 4096);
    desc13.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc13);
    desc4.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc15 = new ActionDescriptor();
    desc15.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc15.putInteger(cTID('Lctn'), 0);
    desc15.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc15);
    var desc16 = new ActionDescriptor();
    desc16.putUnitDouble(cTID('Opct'), cTID('#Prc'), 23.921568627451);
    desc16.putInteger(cTID('Lctn'), 2120);
    desc16.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc16);
    var desc17 = new ActionDescriptor();
    desc17.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc17.putInteger(cTID('Lctn'), 4096);
    desc17.putInteger(cTID('Mdpn'), 24);
    list2.putObject(cTID('TrnS'), desc17);
    desc4.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc4);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Set
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(sTID("contentLayer"), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putBoolean(cTID('Rvrs'), true);
    desc2.putUnitDouble(cTID('Angl'), cTID('#Ang'), 90);
    desc2.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Lnr '));
    var desc3 = new ActionDescriptor();
    desc3.putString(cTID('Nm  '), "Custom");
    desc3.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc3.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc4 = new ActionDescriptor();
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Rd  '), 141.59800350666);
    desc5.putDouble(cTID('Grn '), 155.985840260983);
    desc5.putDouble(cTID('Bl  '), 181.80799305439);
    desc4.putObject(cTID('Clr '), sTID("RGBColor"), desc5);
    desc4.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc4.putInteger(cTID('Lctn'), 0);
    desc4.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc4);
    var desc6 = new ActionDescriptor();
    var desc7 = new ActionDescriptor();
    desc7.putDouble(cTID('Rd  '), 190.336407423019);
    desc7.putDouble(cTID('Grn '), 167.25095897913);
    desc7.putDouble(cTID('Bl  '), 192.160385549068);
    desc6.putObject(cTID('Clr '), sTID("RGBColor"), desc7);
    desc6.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc6.putInteger(cTID('Lctn'), 666);
    desc6.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc6);
    var desc8 = new ActionDescriptor();
    var desc9 = new ActionDescriptor();
    desc9.putDouble(cTID('Rd  '), 221.312162876129);
    desc9.putDouble(cTID('Grn '), 182.774721980095);
    desc9.putDouble(cTID('Bl  '), 139.530944824219);
    desc8.putObject(cTID('Clr '), sTID("RGBColor"), desc9);
    desc8.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc8.putInteger(cTID('Lctn'), 1106);
    desc8.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc8);
    var desc10 = new ActionDescriptor();
    var desc11 = new ActionDescriptor();
    desc11.putDouble(cTID('Rd  '), 255);
    desc11.putDouble(cTID('Grn '), 223.165353834629);
    desc11.putDouble(cTID('Bl  '), 130.926838517189);
    desc10.putObject(cTID('Clr '), sTID("RGBColor"), desc11);
    desc10.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc10.putInteger(cTID('Lctn'), 1587);
    desc10.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc10);
    var desc12 = new ActionDescriptor();
    var desc13 = new ActionDescriptor();
    desc13.putDouble(cTID('Rd  '), 254.000000059605);
    desc13.putDouble(cTID('Grn '), 239.723740518093);
    desc13.putDouble(cTID('Bl  '), 196.229571998119);
    desc12.putObject(cTID('Clr '), sTID("RGBColor"), desc13);
    desc12.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc12.putInteger(cTID('Lctn'), 4096);
    desc12.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc12);
    desc3.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc14 = new ActionDescriptor();
    desc14.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc14.putInteger(cTID('Lctn'), 0);
    desc14.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc14);
    var desc15 = new ActionDescriptor();
    desc15.putUnitDouble(cTID('Opct'), cTID('#Prc'), 30.1960784313725);
    desc15.putInteger(cTID('Lctn'), 1567);
    desc15.putInteger(cTID('Mdpn'), 45);
    list2.putObject(cTID('TrnS'), desc15);
    var desc16 = new ActionDescriptor();
    desc16.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc16.putInteger(cTID('Lctn'), 4096);
    desc16.putInteger(cTID('Mdpn'), 24);
    list2.putObject(cTID('TrnS'), desc16);
    desc3.putList(cTID('Trns'), list2);
    desc2.putObject(cTID('Grad'), cTID('Grdn'), desc3);
    desc1.putObject(cTID('T   '), sTID("gradientLayer"), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Mltp'));
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Soft Sunset");
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 70);
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Move
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Frnt'));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('move'), desc1, dialogMode);
  };

  step1();      // Select
  step2();      // Make
  step3();      // Set
  step4();      // Set
  step5();      // Set
  step6();      // Set
  step7();      // Move
},
};



//=========================================
//                    skySunsetSkyFill.main
//=========================================
//

//skySunsetSkyFill.main = function () {
  //skySunsetSkyFill();
//};

//skySunsetSkyFill.main();

// EOF

//"skySunsetSkyFill.jsx"
// EOF
