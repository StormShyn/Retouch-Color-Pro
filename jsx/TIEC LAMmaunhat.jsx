#target photoshop
//
// TIEC LAMmaunhat.jsx
//

//
// Generated Sun Aug 04 2019 21:01:57 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== mau nhat ==============
//
$._ext_TMN={
run : function maunhat() {
  // Hue/Saturation
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('Clrz'), false);
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putInteger(cTID('LclR'), 2);
    desc2.putInteger(cTID('BgnR'), 15);
    desc2.putInteger(cTID('BgnS'), 45);
    desc2.putInteger(cTID('EndS'), 75);
    desc2.putInteger(cTID('EndR'), 105);
    desc2.putInteger(cTID('H   '), 0);
    desc2.putInteger(cTID('Strt'), -27);
    desc2.putInteger(cTID('Lght'), 20);
    list1.putObject(cTID('Hst2'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putInteger(cTID('LclR'), 3);
    desc3.putInteger(cTID('BgnR'), 75);
    desc3.putInteger(cTID('BgnS'), 105);
    desc3.putInteger(cTID('EndS'), 135);
    desc3.putInteger(cTID('EndR'), 165);
    desc3.putInteger(cTID('H   '), 7);
    desc3.putInteger(cTID('Strt'), -27);
    desc3.putInteger(cTID('Lght'), 23);
    list1.putObject(cTID('Hst2'), desc3);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(sTID('hueSaturation'), desc1, dialogMode);
  };

  step1();      // Hue/Saturation
},
};



//=========================================
//                    maunhat.main
//=========================================
//

//maunhat.main = function () {
  //maunhat();
//};

//maunhat.main();

// EOF

//"TIEC LAMmaunhat.jsx"
// EOF
