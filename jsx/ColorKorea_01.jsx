#target photoshop
//
// ColorKorea_01.jsx
//

//
// Generated Tue Aug 20 2019 01:24:50 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Color Korea - 01 ==============
//
$._ext_C001={
run : function ColorKorea_01() {
  // Duplicate
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Lyr '), cTID('Bckg'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putInteger(cTID('Vrsn'), 5);
    executeAction(cTID('Dplc'), desc1, dialogMode);
  };

  // Camera Raw Filter
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putString(cTID('CMod'), "Filter");
    desc1.putEnumerated(cTID('Sett'), cTID('Sett'), cTID('Cst '));
    desc1.putEnumerated(cTID('WBal'), cTID('WBal'), cTID('AsSh'));
    desc1.putInteger(cTID('Temp'), 0);
    desc1.putInteger(cTID('Tint'), 0);
    desc1.putBoolean(cTID('CtoG'), false);
    desc1.putInteger(cTID('Strt'), -11);
    desc1.putInteger(cTID('Shrp'), 40);
    desc1.putInteger(cTID('LNR '), 0);
    desc1.putInteger(cTID('CNR '), 25);
    desc1.putInteger(cTID('VigA'), 0);
    desc1.putInteger(cTID('BlkB'), 0);
    desc1.putInteger(cTID('RHue'), -4);
    desc1.putInteger(cTID('RSat'), 8);
    desc1.putInteger(cTID('GHue'), 7);
    desc1.putInteger(cTID('GSat'), 39);
    desc1.putInteger(cTID('BHue'), -1);
    desc1.putInteger(cTID('BSat'), 26);
    desc1.putInteger(cTID('Vibr'), 19);
    desc1.putInteger(cTID('HA_R'), 0);
    desc1.putInteger(cTID('HA_O'), 0);
    desc1.putInteger(cTID('HA_Y'), -16);
    desc1.putInteger(cTID('HA_G'), 26);
    desc1.putInteger(cTID('HA_A'), 6);
    desc1.putInteger(cTID('HA_B'), 0);
    desc1.putInteger(cTID('HA_P'), 0);
    desc1.putInteger(cTID('HA_M'), 0);
    desc1.putInteger(cTID('SA_R'), 9);
    desc1.putInteger(cTID('SA_O'), -13);
    desc1.putInteger(cTID('SA_Y'), -59);
    desc1.putInteger(cTID('SA_G'), -65);
    desc1.putInteger(cTID('SA_A'), -52);
    desc1.putInteger(cTID('SA_B'), 6);
    desc1.putInteger(cTID('SA_P'), 16);
    desc1.putInteger(cTID('SA_M'), 36);
    desc1.putInteger(cTID('LA_R'), 1);
    desc1.putInteger(cTID('LA_O'), 56);
    desc1.putInteger(cTID('LA_Y'), -5);
    desc1.putInteger(cTID('LA_G'), 6);
    desc1.putInteger(cTID('LA_A'), 0);
    desc1.putInteger(cTID('LA_B'), 0);
    desc1.putInteger(cTID('LA_P'), 8);
    desc1.putInteger(cTID('LA_M'), 0);
    desc1.putInteger(cTID('STSH'), 55);
    desc1.putInteger(cTID('STSS'), 6);
    desc1.putInteger(cTID('STHH'), 211);
    desc1.putInteger(cTID('STHS'), 13);
    desc1.putInteger(cTID('STB '), 30);
    desc1.putInteger(cTID('PC_S'), 0);
    desc1.putInteger(cTID('PC_D'), 0);
    desc1.putInteger(cTID('PC_L'), 0);
    desc1.putInteger(cTID('PC_H'), 0);
    desc1.putInteger(cTID('PC_1'), 25);
    desc1.putInteger(cTID('PC_2'), 50);
    desc1.putInteger(cTID('PC_3'), 75);
    desc1.putDouble(cTID('ShpR'), 1);
    desc1.putInteger(cTID('ShpD'), 25);
    desc1.putInteger(cTID('ShpM'), 0);
    desc1.putInteger(cTID('PCVA'), 0);
    desc1.putInteger(cTID('GRNA'), 0);
    desc1.putInteger(cTID('CNRD'), 50);
    desc1.putInteger(cTID('CNRS'), 50);
    desc1.putInteger(cTID('LPEn'), 1);
    desc1.putInteger(cTID('MDis'), 0);
    desc1.putInteger(cTID('PerV'), 0);
    desc1.putInteger(cTID('PerH'), 0);
    desc1.putDouble(cTID('PerR'), 0);
    desc1.putInteger(cTID('PerS'), 100);
    desc1.putInteger(cTID('PerA'), 0);
    desc1.putInteger(cTID('PerU'), 0);
    desc1.putDouble(cTID('PerX'), 0);
    desc1.putDouble(cTID('PerY'), 0);
    desc1.putInteger(cTID('AuCA'), 0);
    desc1.putDouble(cTID('Ex12'), -0.11);
    desc1.putInteger(cTID('Cr12'), -11);
    desc1.putInteger(cTID('Hi12'), -14);
    desc1.putInteger(cTID('Sh12'), 66);
    desc1.putInteger(cTID('Wh12'), -69);
    desc1.putInteger(cTID('Bk12'), 17);
    desc1.putInteger(cTID('Cl12'), -7);
    desc1.putInteger(cTID('DfPA'), 0);
    desc1.putInteger(cTID('DPHL'), 30);
    desc1.putInteger(cTID('DPHH'), 70);
    desc1.putInteger(cTID('DfGA'), 0);
    desc1.putInteger(cTID('DPGL'), 40);
    desc1.putInteger(cTID('DPGH'), 60);
    desc1.putInteger(cTID('Dhze'), 0);
    desc1.putInteger(cTID('CrTx'), 0);
    desc1.putInteger(cTID('TMMs'), 0);
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(255);
    list1.putInteger(255);
    desc1.putList(cTID('Crv '), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(255);
    list2.putInteger(255);
    desc1.putList(cTID('CrvR'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(255);
    list3.putInteger(255);
    desc1.putList(cTID('CrvG'), list3);
    var list4 = new ActionList();
    list4.putInteger(0);
    list4.putInteger(0);
    list4.putInteger(255);
    list4.putInteger(255);
    desc1.putList(cTID('CrvB'), list4);
    desc1.putString(cTID('CamP'), "Embedded");
    desc1.putString(cTID('CP_D'), "54650A341B5B5CCAE8442D0B43A92BCE");
    desc1.putInteger(cTID('LPSe'), 2);
    desc1.putString(cTID('LPNa'), "Adobe (Canon EF 85mm f/1.2 L II USM)");
    desc1.putString(cTID('LPFN'), "Canon EOS 5D Mark II (Canon EF 85mm f1.2L II USM) - RAW.lcp");
    desc1.putString(cTID('LPFP'), "1B9204B25A98B63F2DFD385A244207BE");
    desc1.putInteger(cTID('LPDA'), 100);
    desc1.putInteger(cTID('LPCA'), 100);
    desc1.putInteger(cTID('LPVA'), 100);
    desc1.putInteger(cTID('PrVe'), 184549376);
    desc1.putString(cTID('Rtch'), "");
    desc1.putString(cTID('REye'), "");
    desc1.putString(cTID('LCs '), "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\" x:xmptk=\"Adobe XMP Core 5.6-c140 79.160451, 2017/05/06-01:08:21        \">\n <rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n  <rdf:Description rdf:about=\"\"\n    xmlns:crs=\"http://ns.adobe.com/camera-raw-settings/1.0/\">\n   <crs:PaintBasedCorrections>\n    <rdf:Seq>\n     <rdf:li>\n      <rdf:Description\n       crs:What=\"Correction\"\n       crs:CorrectionAmount=\"1.000000\"\n       crs:CorrectionActive=\"true\"\n       crs:LocalExposure=\"0.000000\"\n       crs:LocalSaturation=\"0.000000\"\n       crs:LocalContrast=\"0.000000\"\n       crs:LocalClarity=\"0.000000\"\n       crs:LocalSharpness=\"0.000000\"\n       crs:LocalBrightness=\"0.000000\"\n       crs:LocalToningHue=\"0.000000\"\n       crs:LocalToningSaturation=\"0.000000\"\n       crs:LocalExposure2012=\"0.062500\"\n       crs:LocalContrast2012=\"0.000000\"\n       crs:LocalHighlights2012=\"0.000000\"\n       crs:LocalShadows2012=\"0.000000\"\n       crs:LocalWhites2012=\"0.000000\"\n       crs:LocalBlacks2012=\"0.000000\"\n       crs:LocalClarity2012=\"0.000000\"\n       crs:LocalLuminanceNoise=\"0.000000\"\n       crs:LocalMoire=\"0.000000\"\n       crs:LocalDefringe=\"0.000000\"\n       crs:LocalTemperature=\"0.000000\"\n       crs:LocalTint=\"0.000000\">\n      <crs:CorrectionMasks>\n       <rdf:Seq>\n        <rdf:li>\n         <rdf:Description\n          crs:What=\"Mask/Paint\"\n          crs:MaskValue=\"1.000000\"\n          crs:Radius=\"0.115718\"\n          crs:Flow=\"1.000000\"\n          crs:CenterWeight=\"0.000000\">\n         <crs:Dabs>\n          <rdf:Seq>\n           <rdf:li>d 0.524881 0.418171</rdf:li>\n           <rdf:li>r 0.115721</rdf:li>\n           <rdf:li>d 0.540750 0.443474</rdf:li>\n           <rdf:li>r 0.115718</rdf:li>\n           <rdf:li>d 0.556618 0.468778</rdf:li>\n          </rdf:Seq>\n         </crs:Dabs>\n         </rdf:Description>\n        </rdf:li>\n        <rdf:li>\n         <rdf:Description\n          crs:What=\"Mask/Paint\"\n          crs:MaskValue=\"1.000000\"\n          crs:Radius=\"0.115698\"\n          crs:Flow=\"1.000000\"\n          crs:CenterWeight=\"0.000000\">\n         <crs:Dabs>\n          <rdf:Seq>\n           <rdf:li>d 0.516409 0.378460</rdf:li>\n          </rdf:Seq>\n         </crs:Dabs>\n         </rdf:Description>\n        </rdf:li>\n       </rdf:Seq>\n      </crs:CorrectionMasks>\n      <crs:CorrectionRangeMask\n       crs:Version=\"+1\"\n       crs:Type=\"0\"\n       crs:ColorAmount=\"0.500000\"\n       crs:LumMin=\"0.000000\"\n       crs:LumMax=\"1.000000\"\n       crs:LumFeather=\"0.500000\"\n       crs:DepthMin=\"0.000000\"\n       crs:DepthMax=\"1.000000\"\n       crs:DepthFeather=\"0.500000\"/>\n      </rdf:Description>\n     </rdf:li>\n     <rdf:li>\n      <rdf:Description\n       crs:What=\"Correction\"\n       crs:CorrectionAmount=\"1.000000\"\n       crs:CorrectionActive=\"true\"\n       crs:LocalExposure=\"0.000000\"\n       crs:LocalSaturation=\"0.000000\"\n       crs:LocalContrast=\"0.000000\"\n       crs:LocalClarity=\"0.000000\"\n       crs:LocalSharpness=\"0.000000\"\n       crs:LocalBrightness=\"0.000000\"\n       crs:LocalToningHue=\"0.000000\"\n       crs:LocalToningSaturation=\"0.000000\"\n       crs:LocalExposure2012=\"0.000000\"\n       crs:LocalContrast2012=\"0.390000\"\n       crs:LocalHighlights2012=\"0.000000\"\n       crs:LocalShadows2012=\"0.000000\"\n       crs:LocalWhites2012=\"0.000000\"\n       crs:LocalBlacks2012=\"0.000000\"\n       crs:LocalClarity2012=\"0.000000\"\n       crs:LocalLuminanceNoise=\"0.000000\"\n       crs:LocalMoire=\"0.000000\"\n       crs:LocalDefringe=\"0.000000\"\n       crs:LocalTemperature=\"0.000000\"\n       crs:LocalTint=\"0.000000\">\n      <crs:CorrectionMasks>\n       <rdf:Seq>\n        <rdf:li>\n         <rdf:Description\n          crs:What=\"Mask/Paint\"\n          crs:MaskValue=\"1.000000\"\n          crs:Radius=\"0.180751\"\n          crs:Flow=\"1.000000\"\n          crs:CenterWeight=\"0.000000\">\n         <crs:Dabs>\n          <rdf:Seq>\n           <rdf:li>r 0.180737</rdf:li>\n           <rdf:li>d 0.943996 0.636746</rdf:li>\n           <rdf:li>r 0.180689</rdf:li>\n           <rdf:li>d 0.942228 0.691253</rdf:li>\n           <rdf:li>r 0.180643</rdf:li>\n           <rdf:li>d 0.934967 0.744733</rdf:li>\n           <rdf:li>r 0.180597</rdf:li>\n           <rdf:li>d 0.923198 0.796375</rdf:li>\n           <rdf:li>r 0.180549</rdf:li>\n           <rdf:li>d 0.907921 0.845904</rdf:li>\n           <rdf:li>r 0.180493</rdf:li>\n           <rdf:li>d 0.890529 0.893833</rdf:li>\n           <rdf:li>r 0.180430</rdf:li>\n           <rdf:li>d 0.871068 0.939930</rdf:li>\n           <rdf:li>r 0.180655</rdf:li>\n           <rdf:li>d 0.834940 0.940344</rdf:li>\n           <rdf:li>r 0.180967</rdf:li>\n           <rdf:li>d 0.798869 0.936142</rdf:li>\n           <rdf:li>r 0.181219</rdf:li>\n           <rdf:li>d 0.762672 0.933026</rdf:li>\n           <rdf:li>r 0.181428</rdf:li>\n           <rdf:li>d 0.726419 0.929821</rdf:li>\n           <rdf:li>r 0.181595</rdf:li>\n           <rdf:li>d 0.690076 0.927761</rdf:li>\n           <rdf:li>r 0.181723</rdf:li>\n           <rdf:li>d 0.653664 0.927150</rdf:li>\n           <rdf:li>r 0.181871</rdf:li>\n           <rdf:li>d 0.618637 0.912265</rdf:li>\n           <rdf:li>r 0.181984</rdf:li>\n           <rdf:li>d 0.583223 0.899338</rdf:li>\n           <rdf:li>r 0.181858</rdf:li>\n           <rdf:li>d 0.617852 0.916339</rdf:li>\n           <rdf:li>r 0.181720</rdf:li>\n           <rdf:li>d 0.653412 0.928105</rdf:li>\n           <rdf:li>r 0.181569</rdf:li>\n           <rdf:li>d 0.689524 0.934858</rdf:li>\n           <rdf:li>r 0.181393</rdf:li>\n           <rdf:li>d 0.725787 0.938831</rdf:li>\n           <rdf:li>r 0.181184</rdf:li>\n           <rdf:li>d 0.762050 0.941698</rdf:li>\n           <rdf:li>r 0.180936</rdf:li>\n           <rdf:li>d 0.798292 0.943499</rdf:li>\n           <rdf:li>r 0.180644</rdf:li>\n           <rdf:li>d 0.834497 0.943388</rdf:li>\n           <rdf:li>r 0.180420</rdf:li>\n           <rdf:li>d 0.870624 0.943169</rdf:li>\n           <rdf:li>r 0.180408</rdf:li>\n           <rdf:li>d 0.843121 0.978848</rdf:li>\n           <rdf:li>r 0.180534</rdf:li>\n           <rdf:li>d 0.809847 1.000680</rdf:li>\n           <rdf:li>r 0.180692</rdf:li>\n           <rdf:li>d 0.775565 1.018737</rdf:li>\n           <rdf:li>r 0.180810</rdf:li>\n           <rdf:li>d 0.741243 1.036766</rdf:li>\n           <rdf:li>r 0.180926</rdf:li>\n           <rdf:li>d 0.705918 1.049758</rdf:li>\n           <rdf:li>r 0.181016</rdf:li>\n           <rdf:li>d 0.670378 1.061503</rdf:li>\n           <rdf:li>r 0.181072</rdf:li>\n           <rdf:li>d 0.634814 1.073214</rdf:li>\n           <rdf:li>r 0.181122</rdf:li>\n           <rdf:li>d 0.598763 1.080920</rdf:li>\n           <rdf:li>r 0.181142</rdf:li>\n           <rdf:li>d 0.562699 1.088589</rdf:li>\n           <rdf:li>d 0.526487 1.094511</rdf:li>\n           <rdf:li>r 0.181113</rdf:li>\n           <rdf:li>d 0.490275 1.100393</rdf:li>\n           <rdf:li>r 0.181052</rdf:li>\n           <rdf:li>d 0.454071 1.106233</rdf:li>\n           <rdf:li>r 0.180977</rdf:li>\n           <rdf:li>d 0.417729 1.109359</rdf:li>\n           <rdf:li>r 0.180869</rdf:li>\n           <rdf:li>d 0.381407 1.112440</rdf:li>\n           <rdf:li>r 0.180764</rdf:li>\n           <rdf:li>d 0.345043 1.112571</rdf:li>\n           <rdf:li>r 0.180680</rdf:li>\n           <rdf:li>d 0.308726 1.110595</rdf:li>\n           <rdf:li>r 0.180571</rdf:li>\n           <rdf:li>d 0.272450 1.108573</rdf:li>\n           <rdf:li>r 0.180446</rdf:li>\n           <rdf:li>d 0.236449 1.101742</rdf:li>\n           <rdf:li>r 0.180383</rdf:li>\n           <rdf:li>d 0.200503 1.094865</rdf:li>\n           <rdf:li>r 0.180371</rdf:li>\n           <rdf:li>d 0.164943 1.084640</rdf:li>\n           <rdf:li>r 0.180366</rdf:li>\n           <rdf:li>d 0.130273 1.069041</rdf:li>\n           <rdf:li>r 0.180368</rdf:li>\n           <rdf:li>d 0.096172 1.051010</rdf:li>\n           <rdf:li>r 0.180376</rdf:li>\n           <rdf:li>d 0.062710 1.030632</rdf:li>\n           <rdf:li>r 0.180391</rdf:li>\n           <rdf:li>d 0.029715 1.008827</rdf:li>\n           <rdf:li>r 0.180411</rdf:li>\n           <rdf:li>d -0.002151 0.983641</rdf:li>\n           <rdf:li>r 0.180429</rdf:li>\n           <rdf:li>d -0.030978 0.951042</rdf:li>\n           <rdf:li>r 0.180433</rdf:li>\n           <rdf:li>d -0.053309 0.908301</rdf:li>\n           <rdf:li>r 0.180414</rdf:li>\n           <rdf:li>d -0.063021 0.855980</rdf:li>\n           <rdf:li>r 0.180384</rdf:li>\n           <rdf:li>d -0.027497 0.857956</rdf:li>\n           <rdf:li>r 0.180374</rdf:li>\n           <rdf:li>d 0.008120 0.859933</rdf:li>\n           <rdf:li>r 0.180381</rdf:li>\n           <rdf:li>d 0.043905 0.861852</rdf:li>\n           <rdf:li>r 0.180455</rdf:li>\n           <rdf:li>d 0.079825 0.863723</rdf:li>\n           <rdf:li>r 0.180599</rdf:li>\n           <rdf:li>d 0.115857 0.865553</rdf:li>\n           <rdf:li>r 0.180465</rdf:li>\n           <rdf:li>d 0.120783 0.919506</rdf:li>\n           <rdf:li>r 0.180580</rdf:li>\n           <rdf:li>d 0.111963 0.866632</rdf:li>\n           <rdf:li>r 0.180688</rdf:li>\n           <rdf:li>d 0.107547 0.812527</rdf:li>\n           <rdf:li>r 0.180816</rdf:li>\n           <rdf:li>d 0.104942 0.758123</rdf:li>\n           <rdf:li>r 0.180901</rdf:li>\n           <rdf:li>d 0.099728 0.704079</rdf:li>\n           <rdf:li>r 0.180690</rdf:li>\n           <rdf:li>d 0.077650 0.747000</rdf:li>\n           <rdf:li>r 0.180548</rdf:li>\n           <rdf:li>d 0.064738 0.797690</rdf:li>\n           <rdf:li>r 0.180422</rdf:li>\n           <rdf:li>d 0.064911 0.852097</rdf:li>\n           <rdf:li>r 0.180384</rdf:li>\n           <rdf:li>d 0.075233 0.904335</rdf:li>\n           <rdf:li>r 0.180379</rdf:li>\n           <rdf:li>d 0.095175 0.949913</rdf:li>\n           <rdf:li>r 0.180486</rdf:li>\n           <rdf:li>d 0.122362 0.914807</rdf:li>\n           <rdf:li>r 0.180719</rdf:li>\n           <rdf:li>d 0.132190 0.862618</rdf:li>\n           <rdf:li>r 0.180963</rdf:li>\n           <rdf:li>d 0.136851 0.808646</rdf:li>\n           <rdf:li>r 0.181140</rdf:li>\n           <rdf:li>d 0.138771 0.754187</rdf:li>\n           <rdf:li>r 0.181016</rdf:li>\n           <rdf:li>d 0.142906 0.808432</rdf:li>\n           <rdf:li>r 0.180880</rdf:li>\n           <rdf:li>d 0.149510 0.862100</rdf:li>\n           <rdf:li>r 0.180751</rdf:li>\n           <rdf:li>d 0.160552 0.914093</rdf:li>\n          </rdf:Seq>\n         </crs:Dabs>\n         </rdf:Description>\n        </rdf:li>\n       </rdf:Seq>\n      </crs:CorrectionMasks>\n      <crs:CorrectionRangeMask\n       crs:Version=\"+1\"\n       crs:Type=\"0\"\n       crs:ColorAmount=\"0.500000\"\n       crs:LumMin=\"0.000000\"\n       crs:LumMax=\"1.000000\"\n       crs:LumFeather=\"0.500000\"\n       crs:DepthMin=\"0.000000\"\n       crs:DepthMax=\"1.000000\"\n       crs:DepthFeather=\"0.500000\"/>\n      </rdf:Description>\n     </rdf:li>\n     <rdf:li>\n      <rdf:Description\n       crs:What=\"Correction\"\n       crs:CorrectionAmount=\"1.000000\"\n       crs:CorrectionActive=\"true\"\n       crs:LocalExposure=\"0.000000\"\n       crs:LocalSaturation=\"0.000000\"\n       crs:LocalContrast=\"0.000000\"\n       crs:LocalClarity=\"0.000000\"\n       crs:LocalSharpness=\"0.000000\"\n       crs:LocalBrightness=\"0.000000\"\n       crs:LocalToningHue=\"0.000000\"\n       crs:LocalToningSaturation=\"0.000000\"\n       crs:LocalExposure2012=\"0.000000\"\n       crs:LocalContrast2012=\"0.000000\"\n       crs:LocalHighlights2012=\"-0.250000\"\n       crs:LocalShadows2012=\"0.000000\"\n       crs:LocalWhites2012=\"0.000000\"\n       crs:LocalBlacks2012=\"0.000000\"\n       crs:LocalClarity2012=\"0.000000\"\n       crs:LocalLuminanceNoise=\"0.000000\"\n       crs:LocalMoire=\"0.000000\"\n       crs:LocalDefringe=\"0.000000\"\n       crs:LocalTemperature=\"0.000000\"\n       crs:LocalTint=\"0.000000\">\n      <crs:CorrectionMasks>\n       <rdf:Seq>\n        <rdf:li>\n         <rdf:Description\n          crs:What=\"Mask/Paint\"\n          crs:MaskValue=\"1.000000\"\n          crs:Radius=\"0.206991\"\n          crs:Flow=\"1.000000\"\n          crs:CenterWeight=\"0.000000\">\n         <crs:Dabs>\n          <rdf:Seq>\n           <rdf:li>r 0.207025</rdf:li>\n           <rdf:li>d 0.899449 0.093229</rdf:li>\n           <rdf:li>r 0.207135</rdf:li>\n           <rdf:li>d 0.858089 0.094326</rdf:li>\n           <rdf:li>r 0.207442</rdf:li>\n           <rdf:li>d 0.816695 0.097991</rdf:li>\n           <rdf:li>r 0.207795</rdf:li>\n           <rdf:li>d 0.775145 0.099998</rdf:li>\n           <rdf:li>r 0.208072</rdf:li>\n           <rdf:li>d 0.733495 0.100954</rdf:li>\n           <rdf:li>r 0.208292</rdf:li>\n           <rdf:li>d 0.691782 0.101947</rdf:li>\n           <rdf:li>r 0.208456</rdf:li>\n           <rdf:li>d 0.650011 0.101632</rdf:li>\n           <rdf:li>r 0.208576</rdf:li>\n           <rdf:li>d 0.608200 0.101351</rdf:li>\n           <rdf:li>r 0.208656</rdf:li>\n           <rdf:li>d 0.566363 0.101103</rdf:li>\n           <rdf:li>r 0.208689</rdf:li>\n           <rdf:li>d 0.524532 0.098958</rdf:li>\n           <rdf:li>r 0.208684</rdf:li>\n           <rdf:li>d 0.482696 0.096846</rdf:li>\n           <rdf:li>r 0.208630</rdf:li>\n           <rdf:li>d 0.440964 0.092115</rdf:li>\n           <rdf:li>r 0.208538</rdf:li>\n           <rdf:li>d 0.399251 0.087421</rdf:li>\n           <rdf:li>r 0.208392</rdf:li>\n           <rdf:li>d 0.357756 0.079968</rdf:li>\n           <rdf:li>r 0.208190</rdf:li>\n           <rdf:li>d 0.316538 0.070105</rdf:li>\n           <rdf:li>r 0.207924</rdf:li>\n           <rdf:li>d 0.275662 0.057965</rdf:li>\n           <rdf:li>r 0.207588</rdf:li>\n           <rdf:li>d 0.234975 0.045033</rdf:li>\n           <rdf:li>r 0.207273</rdf:li>\n           <rdf:li>d 0.193494 0.049887</rdf:li>\n           <rdf:li>r 0.207063</rdf:li>\n           <rdf:li>d 0.152151 0.055836</rdf:li>\n           <rdf:li>r 0.207016</rdf:li>\n           <rdf:li>d 0.110766 0.056932</rdf:li>\n           <rdf:li>r 0.207096</rdf:li>\n           <rdf:li>d 0.150695 0.072616</rdf:li>\n           <rdf:li>r 0.207399</rdf:li>\n           <rdf:li>d 0.192179 0.073801</rdf:li>\n           <rdf:li>r 0.207737</rdf:li>\n           <rdf:li>d 0.233766 0.074151</rdf:li>\n           <rdf:li>r 0.208005</rdf:li>\n           <rdf:li>d 0.275427 0.074544</rdf:li>\n           <rdf:li>r 0.208221</rdf:li>\n           <rdf:li>d 0.317130 0.076157</rdf:li>\n           <rdf:li>r 0.208384</rdf:li>\n           <rdf:li>d 0.358892 0.077238</rdf:li>\n           <rdf:li>r 0.208503</rdf:li>\n           <rdf:li>d 0.400691 0.078357</rdf:li>\n           <rdf:li>r 0.208583</rdf:li>\n           <rdf:li>d 0.442508 0.080060</rdf:li>\n           <rdf:li>r 0.208623</rdf:li>\n           <rdf:li>d 0.484340 0.081800</rdf:li>\n           <rdf:li>r 0.208627</rdf:li>\n           <rdf:li>d 0.526172 0.083829</rdf:li>\n           <rdf:li>r 0.208593</rdf:li>\n           <rdf:li>d 0.567992 0.086230</rdf:li>\n           <rdf:li>r 0.208532</rdf:li>\n           <rdf:li>d 0.609695 0.091287</rdf:li>\n           <rdf:li>r 0.208430</rdf:li>\n           <rdf:li>d 0.651374 0.096377</rdf:li>\n           <rdf:li>r 0.208311</rdf:li>\n           <rdf:li>d 0.692534 0.107313</rdf:li>\n           <rdf:li>r 0.208163</rdf:li>\n           <rdf:li>d 0.733180 0.121783</rdf:li>\n           <rdf:li>r 0.207968</rdf:li>\n           <rdf:li>d 0.773673 0.136937</rdf:li>\n           <rdf:li>r 0.207718</rdf:li>\n           <rdf:li>d 0.814043 0.152494</rdf:li>\n           <rdf:li>r 0.207411</rdf:li>\n           <rdf:li>d 0.854062 0.169659</rdf:li>\n           <rdf:li>r 0.207243</rdf:li>\n           <rdf:li>d 0.894543 0.183496</rdf:li>\n           <rdf:li>r 0.207077</rdf:li>\n           <rdf:li>d 0.935351 0.193821</rdf:li>\n           <rdf:li>r 0.206991</rdf:li>\n           <rdf:li>d 0.976457 0.197763</rdf:li>\n          </rdf:Seq>\n         </crs:Dabs>\n         </rdf:Description>\n        </rdf:li>\n       </rdf:Seq>\n      </crs:CorrectionMasks>\n      <crs:CorrectionRangeMask\n       crs:Version=\"+1\"\n       crs:Type=\"0\"\n       crs:ColorAmount=\"0.500000\"\n       crs:LumMin=\"0.000000\"\n       crs:LumMax=\"1.000000\"\n       crs:LumFeather=\"0.500000\"\n       crs:DepthMin=\"0.000000\"\n       crs:DepthMax=\"1.000000\"\n       crs:DepthFeather=\"0.500000\"/>\n      </rdf:Description>\n     </rdf:li>\n     <rdf:li>\n      <rdf:Description\n       crs:What=\"Correction\"\n       crs:CorrectionAmount=\"1.000000\"\n       crs:CorrectionActive=\"true\"\n       crs:LocalExposure=\"0.000000\"\n       crs:LocalSaturation=\"0.000000\"\n       crs:LocalContrast=\"0.000000\"\n       crs:LocalClarity=\"0.000000\"\n       crs:LocalSharpness=\"0.000000\"\n       crs:LocalBrightness=\"0.000000\"\n       crs:LocalToningHue=\"0.000000\"\n       crs:LocalToningSaturation=\"0.000000\"\n       crs:LocalExposure2012=\"0.000000\"\n       crs:LocalContrast2012=\"0.000000\"\n       crs:LocalHighlights2012=\"-0.250000\"\n       crs:LocalShadows2012=\"0.000000\"\n       crs:LocalWhites2012=\"0.000000\"\n       crs:LocalBlacks2012=\"0.000000\"\n       crs:LocalClarity2012=\"0.000000\"\n       crs:LocalLuminanceNoise=\"0.000000\"\n       crs:LocalMoire=\"0.000000\"\n       crs:LocalDefringe=\"0.000000\"\n       crs:LocalTemperature=\"0.000000\"\n       crs:LocalTint=\"0.000000\">\n      <crs:CorrectionMasks>\n       <rdf:Seq>\n        <rdf:li>\n         <rdf:Description\n          crs:What=\"Mask/Paint\"\n          crs:MaskValue=\"1.000000\"\n          crs:Radius=\"0.207014\"\n          crs:Flow=\"1.000000\"\n          crs:CenterWeight=\"0.000000\">\n         <crs:Dabs>\n          <rdf:Seq>\n           <rdf:li>r 0.208073</rdf:li>\n           <rdf:li>d 0.721141 0.085735</rdf:li>\n           <rdf:li>r 0.208265</rdf:li>\n           <rdf:li>d 0.679435 0.083207</rdf:li>\n           <rdf:li>r 0.208415</rdf:li>\n           <rdf:li>d 0.637660 0.082343</rdf:li>\n           <rdf:li>r 0.208538</rdf:li>\n           <rdf:li>d 0.595901 0.085312</rdf:li>\n           <rdf:li>r 0.208614</rdf:li>\n           <rdf:li>d 0.554074 0.086649</rdf:li>\n           <rdf:li>r 0.208645</rdf:li>\n           <rdf:li>d 0.512222 0.086748</rdf:li>\n           <rdf:li>r 0.208640</rdf:li>\n           <rdf:li>d 0.470369 0.087525</rdf:li>\n           <rdf:li>r 0.208597</rdf:li>\n           <rdf:li>d 0.428524 0.088337</rdf:li>\n           <rdf:li>r 0.208512</rdf:li>\n           <rdf:li>d 0.386693 0.088485</rdf:li>\n           <rdf:li>r 0.208386</rdf:li>\n           <rdf:li>d 0.344892 0.088619</rdf:li>\n           <rdf:li>r 0.208207</rdf:li>\n           <rdf:li>d 0.303146 0.087187</rdf:li>\n           <rdf:li>r 0.207980</rdf:li>\n           <rdf:li>d 0.261441 0.086491</rdf:li>\n           <rdf:li>r 0.207687</rdf:li>\n           <rdf:li>d 0.219819 0.084834</rdf:li>\n           <rdf:li>r 0.207323</rdf:li>\n           <rdf:li>d 0.178270 0.083454</rdf:li>\n           <rdf:li>r 0.207067</rdf:li>\n           <rdf:li>d 0.136819 0.082090</rdf:li>\n           <rdf:li>r 0.207014</rdf:li>\n           <rdf:li>d 0.095464 0.082263</rdf:li>\n          </rdf:Seq>\n         </crs:Dabs>\n         </rdf:Description>\n        </rdf:li>\n       </rdf:Seq>\n      </crs:CorrectionMasks>\n      <crs:CorrectionRangeMask\n       crs:Version=\"+1\"\n       crs:Type=\"0\"\n       crs:ColorAmount=\"0.500000\"\n       crs:LumMin=\"0.000000\"\n       crs:LumMax=\"1.000000\"\n       crs:LumFeather=\"0.500000\"\n       crs:DepthMin=\"0.000000\"\n       crs:DepthMax=\"1.000000\"\n       crs:DepthFeather=\"0.500000\"/>\n      </rdf:Description>\n     </rdf:li>\n    </rdf:Seq>\n   </crs:PaintBasedCorrections>\n  </rdf:Description>\n </rdf:RDF>\n</x:xmpmeta>\n");
    desc1.putString(cTID('Upri'), "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\" x:xmptk=\"Adobe XMP Core 5.6-c140 79.160451, 2017/05/06-01:08:21        \">\n <rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n  <rdf:Description rdf:about=\"\"\n    xmlns:crs=\"http://ns.adobe.com/camera-raw-settings/1.0/\"\n   crs:UprightVersion=\"151388160\"\n   crs:UprightCenterMode=\"0\"\n   crs:UprightCenterNormX=\"0.5\"\n   crs:UprightCenterNormY=\"0.5\"\n   crs:UprightFocalMode=\"0\"\n   crs:UprightFocalLength35mm=\"35\"\n   crs:UprightPreview=\"False\"\n   crs:UprightTransformCount=\"6\"/>\n </rdf:RDF>\n</x:xmpmeta>\n");
    desc1.putString(cTID('GuUr'), "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\" x:xmptk=\"Adobe XMP Core 5.6-c140 79.160451, 2017/05/06-01:08:21        \">\n <rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n  <rdf:Description rdf:about=\"\"\n    xmlns:crs=\"http://ns.adobe.com/camera-raw-settings/1.0/\"\n   crs:UprightFourSegmentsCount=\"0\"/>\n </rdf:RDF>\n</x:xmpmeta>\n");
    desc1.putString(cTID('Look'), "");
    desc1.putString(cTID('Pset'), "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\" x:xmptk=\"Adobe XMP Core 5.6-c140 79.160451, 2017/05/06-01:08:21        \">\n <rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n  <rdf:Description rdf:about=\"\"\n    xmlns:crs=\"http://ns.adobe.com/camera-raw-settings/1.0/\">\n   <crs:Preset>\n    <rdf:Description\n     crs:Name=\"DS Phu-Han Quoc\"\n     crs:Amount=\"1.000000\"\n     crs:UUID=\"9BD03849405C0F45A3E344224B0C0AF8\"\n     crs:SupportsAmount=\"false\">\n    <crs:Group>\n     <rdf:Alt>\n      <rdf:li xml:lang=\"x-default\">1 DS Phu Mau Pro 2019</rdf:li>\n     </rdf:Alt>\n    </crs:Group>\n    <crs:Parameters>\n     <rdf:Description\n      crs:Version=\"11.4\"\n      crs:ProcessVersion=\"11.0\"\n      crs:WhiteBalance=\"As Shot\"\n      crs:IncrementalTemperature=\"0\"\n      crs:IncrementalTint=\"0\"\n      crs:Saturation=\"-11\"\n      crs:Sharpness=\"40\"\n      crs:LuminanceSmoothing=\"0\"\n      crs:ColorNoiseReduction=\"25\"\n      crs:VignetteAmount=\"0\"\n      crs:ShadowTint=\"0\"\n      crs:RedHue=\"-4\"\n      crs:RedSaturation=\"+8\"\n      crs:GreenHue=\"+7\"\n      crs:GreenSaturation=\"+39\"\n      crs:BlueHue=\"-1\"\n      crs:BlueSaturation=\"+26\"\n      crs:Vibrance=\"+19\"\n      crs:HueAdjustmentRed=\"0\"\n      crs:HueAdjustmentOrange=\"0\"\n      crs:HueAdjustmentYellow=\"-16\"\n      crs:HueAdjustmentGreen=\"+26\"\n      crs:HueAdjustmentAqua=\"+6\"\n      crs:HueAdjustmentBlue=\"0\"\n      crs:HueAdjustmentPurple=\"0\"\n      crs:HueAdjustmentMagenta=\"0\"\n      crs:SaturationAdjustmentRed=\"+9\"\n      crs:SaturationAdjustmentOrange=\"-13\"\n      crs:SaturationAdjustmentYellow=\"-59\"\n      crs:SaturationAdjustmentGreen=\"-65\"\n      crs:SaturationAdjustmentAqua=\"-52\"\n      crs:SaturationAdjustmentBlue=\"+6\"\n      crs:SaturationAdjustmentPurple=\"+16\"\n      crs:SaturationAdjustmentMagenta=\"+36\"\n      crs:LuminanceAdjustmentRed=\"+1\"\n      crs:LuminanceAdjustmentOrange=\"+56\"\n      crs:LuminanceAdjustmentYellow=\"-5\"\n      crs:LuminanceAdjustmentGreen=\"+6\"\n      crs:LuminanceAdjustmentAqua=\"0\"\n      crs:LuminanceAdjustmentBlue=\"0\"\n      crs:LuminanceAdjustmentPurple=\"+8\"\n      crs:LuminanceAdjustmentMagenta=\"0\"\n      crs:SplitToningShadowHue=\"55\"\n      crs:SplitToningShadowSaturation=\"6\"\n      crs:SplitToningHighlightHue=\"211\"\n      crs:SplitToningHighlightSaturation=\"13\"\n      crs:SplitToningBalance=\"+30\"\n      crs:ParametricShadows=\"0\"\n      crs:ParametricDarks=\"0\"\n      crs:ParametricLights=\"0\"\n      crs:ParametricHighlights=\"0\"\n      crs:ParametricShadowSplit=\"25\"\n      crs:ParametricMidtoneSplit=\"50\"\n      crs:ParametricHighlightSplit=\"75\"\n      crs:SharpenRadius=\"+1.0\"\n      crs:SharpenDetail=\"25\"\n      crs:SharpenEdgeMasking=\"0\"\n      crs:PostCropVignetteAmount=\"0\"\n      crs:GrainAmount=\"0\"\n      crs:ColorNoiseReductionDetail=\"50\"\n      crs:ColorNoiseReductionSmoothness=\"50\"\n      crs:LensManualDistortionAmount=\"0\"\n      crs:AutoLateralCA=\"0\"\n      crs:Exposure2012=\"-0.11\"\n      crs:Contrast2012=\"-11\"\n      crs:Highlights2012=\"-14\"\n      crs:Shadows2012=\"+66\"\n      crs:Whites2012=\"-69\"\n      crs:Blacks2012=\"+17\"\n      crs:Clarity2012=\"-7\"\n      crs:DefringePurpleAmount=\"0\"\n      crs:DefringePurpleHueLo=\"30\"\n      crs:DefringePurpleHueHi=\"70\"\n      crs:DefringeGreenAmount=\"0\"\n      crs:DefringeGreenHueLo=\"40\"\n      crs:DefringeGreenHueHi=\"60\"\n      crs:Dehaze=\"0\"\n      crs:ConvertToGrayscale=\"False\"\n      crs:OverrideLookVignette=\"False\"\n      crs:ToneCurveName2012=\"Linear\"\n      crs:CameraProfile=\"Embedded\"\n      crs:CameraProfileDigest=\"54650A341B5B5CCAE8442D0B43A92BCE\">\n     <crs:ToneCurvePV2012>\n      <rdf:Seq>\n       <rdf:li>0, 0</rdf:li>\n       <rdf:li>255, 255</rdf:li>\n      </rdf:Seq>\n     </crs:ToneCurvePV2012>\n     <crs:ToneCurvePV2012Red>\n      <rdf:Seq>\n       <rdf:li>0, 0</rdf:li>\n       <rdf:li>255, 255</rdf:li>\n      </rdf:Seq>\n     </crs:ToneCurvePV2012Red>\n     <crs:ToneCurvePV2012Green>\n      <rdf:Seq>\n       <rdf:li>0, 0</rdf:li>\n       <rdf:li>255, 255</rdf:li>\n      </rdf:Seq>\n     </crs:ToneCurvePV2012Green>\n     <crs:ToneCurvePV2012Blue>\n      <rdf:Seq>\n       <rdf:li>0, 0</rdf:li>\n       <rdf:li>255, 255</rdf:li>\n      </rdf:Seq>\n     </crs:ToneCurvePV2012Blue>\n     </rdf:Description>\n    </crs:Parameters>\n    </rdf:Description>\n   </crs:Preset>\n  </rdf:Description>\n </rdf:RDF>\n</x:xmpmeta>\n");
    executeAction(sTID('Adobe Camera Raw Filter'), desc1, dialogMode);
  };

  // Make
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putBoolean(sTID("useLegacy"), false);
    desc2.putObject(cTID('Type'), cTID('BrgC'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindDefault"));
    desc2.putObject(cTID('Type'), cTID('Crvs'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindDefault"));
    desc3.putDouble(cTID('Exps'), 0);
    desc3.putDouble(cTID('Ofst'), 0);
    desc3.putDouble(sTID("gammaCorrection"), 1);
    desc2.putObject(cTID('Type'), cTID('Exps'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putClass(cTID('Type'), sTID("vibrance"));
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindDefault"));
    desc3.putBoolean(cTID('Clrz'), false);
    desc2.putObject(cTID('Type'), cTID('HStr'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step8(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc3.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(0);
    desc3.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc3.putList(cTID('HghL'), list3);
    desc3.putBoolean(cTID('PrsL'), true);
    desc2.putObject(cTID('Type'), cTID('ClrB'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step9(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('AdjL'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindDefault"));
    desc2.putObject(cTID('Type'), cTID('SlcC'), desc3);
    desc1.putObject(cTID('Usng'), cTID('AdjL'), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Select
  function step10(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Background copy");
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(sTID("selectionModifier"), sTID("selectionModifierType"), sTID("addToSelectionContinuous"));
    desc1.putBoolean(cTID('MkVs'), false);
    var list1 = new ActionList();
    list1.putInteger(2);
    list1.putInteger(3);
    list1.putInteger(4);
    list1.putInteger(5);
    list1.putInteger(6);
    list1.putInteger(7);
    list1.putInteger(8);
    list1.putInteger(9);
    desc1.putList(cTID('LyrI'), list1);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Make
  function step11(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("layerSection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('From'), ref2);
    desc1.putInteger(sTID("layerSectionStart"), 10);
    desc1.putInteger(sTID("layerSectionEnd"), 11);
    desc1.putString(cTID('Nm  '), "Group 1");
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Set
  function step12(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "BlendColorDS.Phu");
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1(true, true);      // Duplicate
  step2(true, true);      // Camera Raw Filter
  step3();      // Make
  step4();      // Make
  step5();      // Make
  step6();      // Make
  step7();      // Make
  step8();      // Make
  step9();      // Make
  step10();      // Select
  step11();      // Make
  step12();      // Set
},
};



//=========================================
//                    ColorKorea_01.main
//=========================================
//

//ColorKorea_01.main = function () {
  //ColorKorea_01();
//};

//ColorKorea_01.main();

// EOF

//"ColorKorea_01.jsx"
// EOF
