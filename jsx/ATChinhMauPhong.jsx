#target photoshop
//
// ATChinhMauPhong.jsx
//

//
// Generated Tue Aug 13 2019 19:13:25 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Chinh Mau Phong ==============
//
$._ext_ATN06={
run : function ChinhMauPhong() {
  // Selective Color
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Rds '));
    desc2.putUnitDouble(cTID('Mgnt'), cTID('#Prc'), 2);
    desc2.putUnitDouble(cTID('Ylw '), cTID('#Prc'), 54);
    desc2.putUnitDouble(cTID('Blck'), cTID('#Prc'), -4);
    list1.putObject(cTID('ClrC'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Ylws'));
    desc3.putUnitDouble(cTID('Mgnt'), cTID('#Prc'), -45);
    desc3.putUnitDouble(cTID('Ylw '), cTID('#Prc'), 34);
    list1.putObject(cTID('ClrC'), desc3);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Merge Visible
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(sTID('mergeVisible'), undefined, dialogMode);
  };

  step1(true, true);      // Selective Color
  step2();      // Merge Visible
},
};



//=========================================
//                    ChinhMauPhong.main
//=========================================
//

//ChinhMauPhong.main = function () {
  //ChinhMauPhong();
//};

//ChinhMauPhong.main();

// EOF

//"ATChinhMauPhong.jsx"
// EOF
