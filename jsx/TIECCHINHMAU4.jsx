#target photoshop
//
// TIECCHINHMAU4.jsx
//

//
// Generated Mon Aug 05 2019 19:46:21 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== TIEC CHINH MAU 4 ==============
//
$._ext_TTC004={
run : function TIECCHINHMAU4() {
  // Levels
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    desc2.putDouble(cTID('Gmm '), 1.38);
    list1.putObject(cTID('LvlA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Levels
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('AuCo'), true);
    executeAction(cTID('Lvls'), desc1, dialogMode);
  };

  // Fade
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 80);
    desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    executeAction(cTID('Fade'), desc1, dialogMode);
  };

  // Color Balance
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(-21);
    list2.putInteger(0);
    list2.putInteger(0);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(0);
    list3.putInteger(0);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Curves
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Cmps'));
    desc2.putReference(cTID('Chnl'), ref1);
    var list2 = new ActionList();
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Hrzn'), 0);
    desc3.putDouble(cTID('Vrtc'), 0);
    list2.putObject(cTID('Pnt '), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putDouble(cTID('Hrzn'), 94);
    desc4.putDouble(cTID('Vrtc'), 102);
    list2.putObject(cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putDouble(cTID('Hrzn'), 255);
    desc5.putDouble(cTID('Vrtc'), 255);
    list2.putObject(cTID('Pnt '), desc5);
    desc2.putList(cTID('Crv '), list2);
    list1.putObject(cTID('CrvA'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(cTID('Crvs'), desc1, dialogMode);
  };

  // Color Range
  function step6(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Fzns'), 4);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 24.31);
    desc2.putDouble(cTID('A   '), 5);
    desc2.putDouble(cTID('B   '), 4);
    desc1.putObject(cTID('Mnm '), cTID('LbCl'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putDouble(cTID('Lmnc'), 90.19);
    desc3.putDouble(cTID('A   '), 32);
    desc3.putDouble(cTID('B   '), 37);
    desc1.putObject(cTID('Mxm '), cTID('LbCl'), desc3);
    executeAction(sTID('colorRange'), desc1, dialogMode);
  };

  // Feather
  function step7(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Rds '), cTID('#Pxl'), 10);
    executeAction(cTID('Fthr'), desc1, dialogMode);
  };

  // Assign Profile
  function step8(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Dcmn'), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putString(sTID("profile"), "Adobe RGB (1998)");
    executeAction(sTID('assignProfile'), desc1, dialogMode);
  };

  // Color Balance
  function step9(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(0);
    list2.putInteger(0);
    list2.putInteger(0);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(-10);
    list3.putInteger(0);
    list3.putInteger(2);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Hue/Saturation
  function step10(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putBoolean(cTID('Clrz'), false);
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putInteger(cTID('H   '), 0);
    desc2.putInteger(cTID('Strt'), 5);
    desc2.putInteger(cTID('Lght'), 0);
    list1.putObject(cTID('Hst2'), desc2);
    desc1.putList(cTID('Adjs'), list1);
    executeAction(sTID('hueSaturation'), desc1, dialogMode);
  };

  // Selective Color
  function step11(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Rds '));
    desc2.putUnitDouble(cTID('Cyn '), cTID('#Prc'), -7);
    list1.putObject(cTID('ClrC'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Ntrl'));
    desc3.putUnitDouble(cTID('Mgnt'), cTID('#Prc'), -2);
    desc3.putUnitDouble(cTID('Ylw '), cTID('#Prc'), 2);
    list1.putObject(cTID('ClrC'), desc3);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Neat Image
  function step12(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putString(cTID('SfpN'), "RecentProfile.dnp");
    desc1.putString(cTID('SrpN'), "RecentPreset.nfp");
    desc1.putString(cTID('FfpN'), "C:\Program Files\Adobe\Photoshop 7.0\Plug-Ins\PLUGIN\RecentProfile.dnp");
    desc1.putString(cTID('FrpN'), "C:\Program Files\Adobe\Photoshop 7.0\Plug-Ins\PLUGIN\RecentPreset.nfp");
    executeAction(sTID('d9543b0c-3c91-11d4-97bc-acb0d0204936'), desc1, dialogMode);
  };

  // Custom
  function step13(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putInteger(cTID('Scl '), 1);
    desc1.putInteger(cTID('Ofst'), 0);
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(-1);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(-1);
    list1.putInteger(5);
    list1.putInteger(-1);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(-1);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    list1.putInteger(0);
    desc1.putList(cTID('Mtrx'), list1);
    executeAction(cTID('Cstm'), desc1, dialogMode);
  };

  // Fade
  function step14(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 20);
    desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    executeAction(cTID('Fade'), desc1, dialogMode);
  };

  // Set
  function step15(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Set
  function step16(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Rd  '));
    desc1.putReference(cTID('T   '), ref2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Apply Image
  function step17(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), sTID("RGB"));
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Mrgd'));
    desc2.putReference(cTID('T   '), ref1);
    desc2.putEnumerated(cTID('Clcl'), cTID('Clcn'), cTID('SftL'));
    desc2.putBoolean(cTID('PrsT'), true);
    desc1.putObject(cTID('With'), cTID('Clcl'), desc2);
    executeAction(sTID('applyImageEvent'), desc1, dialogMode);
  };

  // Fade
  function step18(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 80);
    desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    executeAction(cTID('Fade'), desc1, dialogMode);
  };

  // Fade
  function step19(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putUnitDouble(cTID('Opct'), cTID('#Prc'), 60);
    desc1.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Nrml'));
    executeAction(cTID('Fade'), desc1, dialogMode);
  };

  // Inverse
  function step20(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    executeAction(cTID('Invs'), undefined, dialogMode);
  };

  // Apply Image
  function step21(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), sTID("RGB"));
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Mrgd'));
    desc2.putReference(cTID('T   '), ref1);
    desc2.putEnumerated(cTID('Clcl'), cTID('Clcn'), cTID('SftL'));
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 50);
    desc2.putBoolean(cTID('PrsT'), true);
    desc1.putObject(cTID('With'), cTID('Clcl'), desc2);
    executeAction(sTID('applyImageEvent'), desc1, dialogMode);
  };

  // Set
  function step22(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(cTID('T   '), cTID('Ordn'), cTID('None'));
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  // Purge
  function step23(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(cTID('null'), cTID('PrgI'), cTID('Al  '));
    executeAction(cTID('Prge'), desc1, dialogMode);
  };

  step1();      // Levels
  step2();      // Levels
  step3();      // Fade
  step4();      // Color Balance
  step5();      // Curves
  step6();      // Color Range
  step7();      // Feather
  step8(true, true);      // Assign Profile
  step9();      // Color Balance
  step10();      // Hue/Saturation
  step11();      // Selective Color
  step12();      // Neat Image
  step13();      // Custom
  step14();      // Fade
  step15();      // Set
  step16();      // Set
  step17();      // Apply Image
  step18();      // Fade
  step19();      // Fade
  step20();      // Inverse
  step21();      // Apply Image
  step22();      // Set
  step23();      // Purge
},
};



//=========================================
//                    TIECCHINHMAU4.main
//=========================================
//

//TIECCHINHMAU4.main = function () {
 // TIECCHINHMAU4();
//};

//TIECCHINHMAU4.main();

// EOF

//"TIECCHINHMAU4.jsx"
// EOF
