#target photoshop
//
// LIGHT8.jsx
//

//
// Generated Wed Jul 31 2019 00:37:30 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== LIGHT 8 ==============
//
$._ext_E013={
run : function LIGHT8() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Golden Hour");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Scrn'));
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), 21.04);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Lnr '));
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 150);
    var desc4 = new ActionDescriptor();
    desc4.putUnitDouble(cTID('Hrzn'), cTID('#Prc'), 10.3296703296703);
    desc4.putUnitDouble(cTID('Vrtc'), cTID('#Prc'), 13.1782945736434);
    desc3.putObject(cTID('Ofst'), cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putString(cTID('Nm  '), "Golden Hour");
    desc5.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc5.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc6 = new ActionDescriptor();
    var desc7 = new ActionDescriptor();
    desc7.putDouble(cTID('Rd  '), 155.000005960464);
    desc7.putDouble(cTID('Grn '), 104.000001400709);
    desc7.putDouble(cTID('Bl  '), 59.003891274333);
    desc6.putObject(cTID('Clr '), sTID("RGBColor"), desc7);
    desc6.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc6.putInteger(cTID('Lctn'), 1819);
    desc6.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc6);
    var desc8 = new ActionDescriptor();
    var desc9 = new ActionDescriptor();
    desc9.putDouble(cTID('Rd  '), 173.996113836765);
    desc9.putDouble(cTID('Grn '), 108.000001162291);
    desc9.putDouble(cTID('Bl  '), 50.0000008195639);
    desc8.putObject(cTID('Clr '), sTID("RGBColor"), desc9);
    desc8.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc8.putInteger(cTID('Lctn'), 2011);
    desc8.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc8);
    var desc10 = new ActionDescriptor();
    var desc11 = new ActionDescriptor();
    desc11.putDouble(cTID('Rd  '), 255);
    desc11.putDouble(cTID('Grn '), 181.996113359928);
    desc11.putDouble(cTID('Bl  '), 86.0000024735928);
    desc10.putObject(cTID('Clr '), sTID("RGBColor"), desc11);
    desc10.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc10.putInteger(cTID('Lctn'), 2479);
    desc10.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc10);
    var desc12 = new ActionDescriptor();
    var desc13 = new ActionDescriptor();
    desc13.putDouble(cTID('Rd  '), 255);
    desc13.putDouble(cTID('Grn '), 161.000005602837);
    desc13.putDouble(cTID('Bl  '), 48.0000009387732);
    desc12.putObject(cTID('Clr '), sTID("RGBColor"), desc13);
    desc12.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc12.putInteger(cTID('Lctn'), 2753);
    desc12.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc12);
    var desc14 = new ActionDescriptor();
    var desc15 = new ActionDescriptor();
    desc15.putDouble(cTID('Rd  '), 187.9961130023);
    desc15.putDouble(cTID('Grn '), 107.003892213106);
    desc15.putDouble(cTID('Bl  '), 44.0000011771917);
    desc14.putObject(cTID('Clr '), sTID("RGBColor"), desc15);
    desc14.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc14.putInteger(cTID('Lctn'), 4096);
    desc14.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc14);
    desc5.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc16 = new ActionDescriptor();
    desc16.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc16.putInteger(cTID('Lctn'), 0);
    desc16.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc16);
    var desc17 = new ActionDescriptor();
    desc17.putUnitDouble(cTID('Opct'), cTID('#Prc'), 83.1372549019608);
    desc17.putInteger(cTID('Lctn'), 2543);
    desc17.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc17);
    var desc18 = new ActionDescriptor();
    desc18.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc18.putInteger(cTID('Lctn'), 4096);
    desc18.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc18);
    desc5.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc5);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  step1();      // Make
},
};



//=========================================
//                    LIGHT8.main
//=========================================
//

//LIGHT8.main = function () {
  //LIGHT8();
//};

//LIGHT8.main();

// EOF

//"LIGHT8.jsx"
// EOF
