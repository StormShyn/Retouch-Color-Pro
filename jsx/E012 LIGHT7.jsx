#target photoshop
//
// LIGHT7.jsx
//

//
// Generated Wed Jul 31 2019 00:37:09 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== LIGHT 7 ==============
//
$._ext_E012={
run : function LIGHT7() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Emerald I");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Scrn'));
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), -90);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Dmnd'));
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 300);
    var desc4 = new ActionDescriptor();
    desc4.putString(cTID('Nm  '), "Emerald I");
    desc4.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc4.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc5 = new ActionDescriptor();
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Rd  '), 85.0038935244083);
    desc6.putDouble(cTID('Grn '), 98.000001758337);
    desc6.putDouble(cTID('Bl  '), 112.000000923872);
    desc5.putObject(cTID('Clr '), sTID("RGBColor"), desc6);
    desc5.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc5.putInteger(cTID('Lctn'), 479);
    desc5.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc5);
    var desc7 = new ActionDescriptor();
    var desc8 = new ActionDescriptor();
    desc8.putDouble(cTID('Rd  '), 87.7976632118225);
    desc8.putDouble(cTID('Grn '), 138.455252945423);
    desc8.putDouble(cTID('Bl  '), 193.000003695488);
    desc7.putObject(cTID('Clr '), sTID("RGBColor"), desc8);
    desc7.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc7.putInteger(cTID('Lctn'), 1436);
    desc7.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc7);
    var desc9 = new ActionDescriptor();
    var desc10 = new ActionDescriptor();
    desc10.putDouble(cTID('Rd  '), 52.6498038321733);
    desc10.putDouble(cTID('Grn '), 137.000007033348);
    desc10.putDouble(cTID('Bl  '), 131.01557135582);
    desc9.putObject(cTID('Clr '), sTID("RGBColor"), desc10);
    desc9.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc9.putInteger(cTID('Lctn'), 2575);
    desc9.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc9);
    var desc11 = new ActionDescriptor();
    var desc12 = new ActionDescriptor();
    desc12.putDouble(cTID('Rd  '), 78.0000029504299);
    desc12.putDouble(cTID('Grn '), 205.000002980232);
    desc12.putDouble(cTID('Bl  '), 195.996112525463);
    desc11.putObject(cTID('Clr '), sTID("RGBColor"), desc12);
    desc11.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc11.putInteger(cTID('Lctn'), 4096);
    desc11.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc11);
    desc4.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc13 = new ActionDescriptor();
    desc13.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc13.putInteger(cTID('Lctn'), 0);
    desc13.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc13);
    var desc14 = new ActionDescriptor();
    desc14.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc14.putInteger(cTID('Lctn'), 4096);
    desc14.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc14);
    desc4.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc4);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Emerald II");
    desc2.putUnitDouble(cTID('Opct'), cTID('#Prc'), 40);
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Scrn'));
    var desc3 = new ActionDescriptor();
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), -61.39);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Dmnd'));
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 260);
    var desc4 = new ActionDescriptor();
    desc4.putString(cTID('Nm  '), "Emerald II");
    desc4.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc4.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc5 = new ActionDescriptor();
    var desc6 = new ActionDescriptor();
    desc6.putDouble(cTID('Rd  '), 85.0038935244083);
    desc6.putDouble(cTID('Grn '), 98.000001758337);
    desc6.putDouble(cTID('Bl  '), 112.000000923872);
    desc5.putObject(cTID('Clr '), sTID("RGBColor"), desc6);
    desc5.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc5.putInteger(cTID('Lctn'), 479);
    desc5.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc5);
    var desc7 = new ActionDescriptor();
    var desc8 = new ActionDescriptor();
    desc8.putDouble(cTID('Rd  '), 87.7976632118225);
    desc8.putDouble(cTID('Grn '), 138.455252945423);
    desc8.putDouble(cTID('Bl  '), 193.000003695488);
    desc7.putObject(cTID('Clr '), sTID("RGBColor"), desc8);
    desc7.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc7.putInteger(cTID('Lctn'), 1436);
    desc7.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc7);
    var desc9 = new ActionDescriptor();
    var desc10 = new ActionDescriptor();
    desc10.putDouble(cTID('Rd  '), 52.6498038321733);
    desc10.putDouble(cTID('Grn '), 137.000007033348);
    desc10.putDouble(cTID('Bl  '), 131.01557135582);
    desc9.putObject(cTID('Clr '), sTID("RGBColor"), desc10);
    desc9.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc9.putInteger(cTID('Lctn'), 2575);
    desc9.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc9);
    var desc11 = new ActionDescriptor();
    var desc12 = new ActionDescriptor();
    desc12.putDouble(cTID('Rd  '), 78.0000029504299);
    desc12.putDouble(cTID('Grn '), 205.000002980232);
    desc12.putDouble(cTID('Bl  '), 195.996112525463);
    desc11.putObject(cTID('Clr '), sTID("RGBColor"), desc12);
    desc11.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc11.putInteger(cTID('Lctn'), 4096);
    desc11.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc11);
    desc4.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc13 = new ActionDescriptor();
    desc13.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc13.putInteger(cTID('Lctn'), 0);
    desc13.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc13);
    var desc14 = new ActionDescriptor();
    desc14.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc14.putInteger(cTID('Lctn'), 4096);
    desc14.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc14);
    desc4.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc4);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Select
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putName(cTID('Lyr '), "Emerald I");
    desc1.putReference(cTID('null'), ref1);
    desc1.putEnumerated(sTID("selectionModifier"), sTID("selectionModifierType"), sTID("addToSelectionContinuous"));
    desc1.putBoolean(cTID('MkVs'), false);
    executeAction(cTID('slct'), desc1, dialogMode);
  };

  // Make
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("layerSection"));
    desc1.putReference(cTID('null'), ref1);
    var ref2 = new ActionReference();
    ref2.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('From'), ref2);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Emerald");
    desc1.putObject(cTID('Usng'), sTID("layerSection"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  // Make
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putClass(cTID('Nw  '), cTID('Chnl'));
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Chnl'), cTID('Chnl'), cTID('Msk '));
    desc1.putReference(cTID('At  '), ref1);
    desc1.putEnumerated(cTID('Usng'), cTID('UsrM'), cTID('RvlA'));
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  step1();      // Make
  step2();      // Make
  step3();      // Select
  step4();      // Make
  step5();      // Make
},
};



//=========================================
//                    LIGHT7.main
//=========================================
//

//LIGHT7.main = function () {
  //LIGHT7();
//};

//LIGHT7.main();

// EOF

//"LIGHT7.jsx"
// EOF
