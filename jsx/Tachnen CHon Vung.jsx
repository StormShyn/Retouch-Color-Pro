#target photoshop
//
// Tachnen CHon Vung.jsx
//

//
// Generated Tue Aug 13 2019 23:07:19 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== Action 10 ==============
//
$._ext_s10={
run : function Action10() {
  // Set
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putProperty(cTID('Chnl'), sTID("selection"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putUnitDouble(cTID('Hrzn'), cTID('#Pxl'), 383);
    desc2.putUnitDouble(cTID('Vrtc'), cTID('#Pxl'), 25);
    desc1.putObject(cTID('T   '), cTID('Pnt '), desc2);
    desc1.putInteger(cTID('Tlrn'), 32);
    desc1.putBoolean(cTID('AntA'), true);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Set
},
};



//=========================================
//                    Action10.main
//=========================================
//

//Action10.main = function () {
  //Action10();
//};

//Action10.main();

// EOF

//"Tachnen CHon Vung.jsx"
// EOF
