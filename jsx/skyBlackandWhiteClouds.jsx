#target photoshop
//
// skyBlackandWhiteClouds.jsx
//

//
// Generated Sun Aug 11 2019 16:01:33 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== sky Black and White Clouds ==============
//
$._ext_sky29={
run : function skyBlackandWhiteClouds() {
  // Duplicate
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putInteger(cTID('Vrsn'), 5);
    executeAction(cTID('Dplc'), desc1, dialogMode);
  };

  // Black & White
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    desc1.putInteger(cTID('Rd  '), 42);
    desc1.putInteger(cTID('Yllw'), 77);
    desc1.putInteger(cTID('Grn '), 29);
    desc1.putInteger(cTID('Cyn '), 60);
    desc1.putInteger(cTID('Bl  '), 3);
    desc1.putInteger(cTID('Mgnt'), 80);
    desc1.putBoolean(sTID("useTint"), false);
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Rd  '), 225.000457763672);
    desc2.putDouble(cTID('Grn '), 211.000671386719);
    desc2.putDouble(cTID('Bl  '), 179.001159667969);
    desc1.putObject(sTID("tintColor"), sTID("RGBColor"), desc2);
    executeAction(sTID('blackAndWhite'), desc1, dialogMode);
  };

  // Set
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Black and White Clouds");
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Duplicate
  step2();      // Black & White
  step3();      // Set
},
};



//=========================================
//                    skyBlackandWhiteClouds.main
//=========================================
//

//skyBlackandWhiteClouds.main = function () {
 // skyBlackandWhiteClouds();
//};

//skyBlackandWhiteClouds.main();

// EOF

//"skyBlackandWhiteClouds.jsx"
// EOF
