#target photoshop
//
// skyCowboySky.jsx
//

//
// Generated Sun Aug 11 2019 15:43:07 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== sky Cowboy Sky  ==============
//
$._ext_sky26={
run : function skyCowboySky() {
  // Duplicate
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    desc1.putInteger(cTID('Vrsn'), 5);
    executeAction(cTID('Dplc'), desc1, dialogMode);
  };

  // Photo Filter
  function step2(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var desc2 = new ActionDescriptor();
    desc2.putDouble(cTID('Lmnc'), 67.06);
    desc2.putDouble(cTID('A   '), 32);
    desc2.putDouble(cTID('B   '), 120);
    desc1.putObject(cTID('Clr '), cTID('LbCl'), desc2);
    desc1.putInteger(cTID('Dnst'), 25);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('photoFilter'), desc1, dialogMode);
  };

  // Color Balance
  function step3(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var list1 = new ActionList();
    list1.putInteger(0);
    list1.putInteger(-2);
    list1.putInteger(-6);
    desc1.putList(cTID('ShdL'), list1);
    var list2 = new ActionList();
    list2.putInteger(5);
    list2.putInteger(0);
    list2.putInteger(-6);
    desc1.putList(cTID('MdtL'), list2);
    var list3 = new ActionList();
    list3.putInteger(0);
    list3.putInteger(-3);
    list3.putInteger(-11);
    desc1.putList(cTID('HghL'), list3);
    desc1.putBoolean(cTID('PrsL'), true);
    executeAction(sTID('colorBalance'), desc1, dialogMode);
  };

  // Selective Color
  function step4(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    desc1.putEnumerated(sTID("presetKind"), sTID("presetKindType"), sTID("presetKindCustom"));
    desc1.putEnumerated(cTID('Mthd'), cTID('CrcM'), cTID('Rltv'));
    var list1 = new ActionList();
    var desc2 = new ActionDescriptor();
    desc2.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Cyns'));
    desc2.putUnitDouble(cTID('Mgnt'), cTID('#Prc'), 7);
    desc2.putUnitDouble(cTID('Ylw '), cTID('#Prc'), 4);
    list1.putObject(cTID('ClrC'), desc2);
    var desc3 = new ActionDescriptor();
    desc3.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Bls '));
    desc3.putUnitDouble(cTID('Mgnt'), cTID('#Prc'), 2);
    desc3.putUnitDouble(cTID('Ylw '), cTID('#Prc'), 6);
    list1.putObject(cTID('ClrC'), desc3);
    var desc4 = new ActionDescriptor();
    desc4.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Whts'));
    desc4.putUnitDouble(cTID('Ylw '), cTID('#Prc'), 16);
    list1.putObject(cTID('ClrC'), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putEnumerated(cTID('Clrs'), cTID('Clrs'), cTID('Ntrl'));
    desc5.putUnitDouble(cTID('Cyn '), cTID('#Prc'), 2);
    desc5.putUnitDouble(cTID('Ylw '), cTID('#Prc'), 8);
    list1.putObject(cTID('ClrC'), desc5);
    desc1.putList(cTID('ClrC'), list1);
    executeAction(sTID('selectiveColor'), desc1, dialogMode);
  };

  // Set
  function step5(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putEnumerated(cTID('Lyr '), cTID('Ordn'), cTID('Trgt'));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Cowboy Sky");
    desc1.putObject(cTID('T   '), cTID('Lyr '), desc2);
    executeAction(cTID('setd'), desc1, dialogMode);
  };

  step1();      // Duplicate
  step2();      // Photo Filter
  step3();      // Color Balance
  step4();      // Selective Color
  step5();      // Set
},
};



//=========================================
//                    skyCowboySky.main
//=========================================
//

//skyCowboySky.main = function () {
 // skyCowboySky();
//};

//skyCowboySky.main();

// EOF

//"skyCowboySky.jsx"
// EOF
