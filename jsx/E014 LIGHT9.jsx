#target photoshop
//
// LIGHT9.jsx
//

//
// Generated Wed Jul 31 2019 00:38:20 GMT+0700
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== LIGHT 9 ==============
//
$._ext_E014={
run : function LIGHT9() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(sTID("contentLayer"));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
    desc2.putString(cTID('Nm  '), "Sunburst");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('Scrn'));
    var desc3 = new ActionDescriptor();
    desc3.putBoolean(cTID('Rvrs'), true);
    desc3.putUnitDouble(cTID('Angl'), cTID('#Ang'), 21.04);
    desc3.putEnumerated(cTID('Type'), cTID('GrdT'), cTID('Rdl '));
    desc3.putUnitDouble(cTID('Scl '), cTID('#Prc'), 250);
    var desc4 = new ActionDescriptor();
    desc4.putUnitDouble(cTID('Hrzn'), cTID('#Prc'), 10.3296703296703);
    desc4.putUnitDouble(cTID('Vrtc'), cTID('#Prc'), 13.1782945736434);
    desc3.putObject(cTID('Ofst'), cTID('Pnt '), desc4);
    var desc5 = new ActionDescriptor();
    desc5.putString(cTID('Nm  '), "Sunburst");
    desc5.putEnumerated(cTID('GrdF'), cTID('GrdF'), cTID('CstS'));
    desc5.putDouble(cTID('Intr'), 4096);
    var list1 = new ActionList();
    var desc6 = new ActionDescriptor();
    var desc7 = new ActionDescriptor();
    desc7.putDouble(cTID('Rd  '), 255);
    desc7.putDouble(cTID('Grn '), 253.996124267578);
    desc7.putDouble(cTID('Bl  '), 249.000091552734);
    desc6.putObject(cTID('Clr '), sTID("RGBColor"), desc7);
    desc6.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc6.putInteger(cTID('Lctn'), 0);
    desc6.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc6);
    var desc8 = new ActionDescriptor();
    var desc9 = new ActionDescriptor();
    desc9.putDouble(cTID('Rd  '), 145.817114710808);
    desc9.putDouble(cTID('Grn '), 8.24124477803707);
    desc9.putDouble(cTID('Bl  '), 191.000003814697);
    desc8.putObject(cTID('Clr '), sTID("RGBColor"), desc9);
    desc8.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc8.putInteger(cTID('Lctn'), 670);
    desc8.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc8);
    var desc10 = new ActionDescriptor();
    var desc11 = new ActionDescriptor();
    desc11.putDouble(cTID('Rd  '), 243.000000715256);
    desc11.putDouble(cTID('Grn '), 200.167315900326);
    desc11.putDouble(cTID('Bl  '), 167.715947628021);
    desc10.putObject(cTID('Clr '), sTID("RGBColor"), desc11);
    desc10.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc10.putInteger(cTID('Lctn'), 1766);
    desc10.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc10);
    var desc12 = new ActionDescriptor();
    var desc13 = new ActionDescriptor();
    desc13.putDouble(cTID('Rd  '), 195.000003576279);
    desc13.putDouble(cTID('Grn '), 92.2840444743633);
    desc13.putDouble(cTID('Bl  '), 16.8249024823308);
    desc12.putObject(cTID('Clr '), sTID("RGBColor"), desc13);
    desc12.putEnumerated(cTID('Type'), cTID('Clry'), cTID('UsrS'));
    desc12.putInteger(cTID('Lctn'), 4096);
    desc12.putInteger(cTID('Mdpn'), 50);
    list1.putObject(cTID('Clrt'), desc12);
    desc5.putList(cTID('Clrs'), list1);
    var list2 = new ActionList();
    var desc14 = new ActionDescriptor();
    desc14.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc14.putInteger(cTID('Lctn'), 0);
    desc14.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc14);
    var desc15 = new ActionDescriptor();
    desc15.putUnitDouble(cTID('Opct'), cTID('#Prc'), 100);
    desc15.putInteger(cTID('Lctn'), 2053);
    desc15.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc15);
    var desc16 = new ActionDescriptor();
    desc16.putUnitDouble(cTID('Opct'), cTID('#Prc'), 0);
    desc16.putInteger(cTID('Lctn'), 4096);
    desc16.putInteger(cTID('Mdpn'), 50);
    list2.putObject(cTID('TrnS'), desc16);
    desc5.putList(cTID('Trns'), list2);
    desc3.putObject(cTID('Grad'), cTID('Grdn'), desc5);
    desc2.putObject(cTID('Type'), sTID("gradientLayer"), desc3);
    desc1.putObject(cTID('Usng'), sTID("contentLayer"), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  step1();      // Make
},
};



//=========================================
//                    LIGHT9.main
//=========================================
//

//LIGHT9.main = function () {
  //LIGHT9();
//};

//LIGHT9.main();

// EOF

//"LIGHT9.jsx"
// EOF
