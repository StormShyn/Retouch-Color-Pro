(function(){function c(){var b=new CSEvent("com.adobe.PhotoshopPersistent","APPLICATION");b.extensionId=d;a.dispatchEvent(b)}var a=new CSInterface,d="com.wallstrom.RetouchingToolkitPanel.extension1";(function(){c();$("#button-brush").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Brush, D&B');")});$("#button-healhard").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Healing Brush, Hard');")});$("#button-healsoft").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Healing Brush, Soft');")});
$("#button-clonehard").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Clone Stamp, Hard');")});$("#button-clonesoft").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Clone Stamp, Soft');")});$("#button-mixbrush").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Mixer Brush, Moist');")});$("#button-eyehelp").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Eye Help');")});$("#button-healing").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Healing Layer');")});
$("#button-dnb").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Dodge & Burn');")});$("#button-dodge").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Dodge');")});$("#button-burn").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Burn');")});$("#button-gray").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Soft Light - 50% Gray');")});$("#button-softlight").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Soft Light - Blank Layer');")});
$("#button-color").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Color Layer');")});$("#button-saturation").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Saturation Layer');")});$("#button-desaturate").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Desaturate');")});$("#button-curves").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionWithRemoveMask('Curves');")});$("#button-colorbalance").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionWithRemoveMask('Color Balance');")});
$("#button-stamp").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Stamp Current & Below');")});$("#button-averagecolor").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Average Color...');")});$("#button-gradientmapmaker").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Gradient Map Maker...');")});$("#button-gradientmappicker").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Gradient Map Picker...');")});$("#button-fs").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Frequency Separation...');")});
$("#button-fshp").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Frequency Separation using HP...');")});$("#button-fsmedian").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Frequency Separation using Median...');")});$("#button-liquify").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Smart Liquify...');")});$("#button-updateso").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Update Smart Object');")});$("#button-ihp").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Band Stop...');")});
$("#button-ihpcontrast").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Band Stop with Contrast...');")});$("#button-bandpass").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Band Pass...');")});$("#button-selection").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Load Channel As Selection');")});$("#button-channel").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Save Selection As Channel');")});$("#button-dodgetool1").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Dodge, Highlights');")});
$("#button-burntool1").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Burn, Shadows');")});$("#button-brushhard").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Brush, Hard');")});$("#button-maskbrush").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Brush, Masking');")});$("#button-selection").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Load Channel As Selection');")});$("#button-channelselection").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Save Selection As Channel');")});
$("#button-refineedge").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionNoError('Refine Edge');")});$("#button-luminosity").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Luminosity');")});$("#button-luminosityshadows").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Luminosity Shadows');")});$("#button-luminositymidtones").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Luminosity Midtones');")});$("#button-luminosityhighlights").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Luminosity Highlights');")});
$("#button-zones").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionNoError('Generate All Zones');")});$("#button-bishadows").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionNoError('Blend-If Shadows');")});$("#button-bimidtones").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionNoError('Blend-If Midtones');")});$("#button-bihighlights").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionNoError('Blend-If Highlights');")});$("#button-biclear").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionNoError('Blend-If Clear');")});
$("#button-goldenratio").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Golden Ratio');")});$("#button-goldentriangle").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Golden Triangle');")});$("#button-goldenspiral").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Golden Spiral');")});$("#button-vanishingpoint").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Vanishing Point...');")});$("#button-generateallchannels").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Generate All Channels');")});
$("#button-channelmixerchannel").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Channel Mixer to Channel...');")});$("#button-channelcolorrange").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Color Range to Channel...');")});$("#button-saturationchannel").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Saturation Channel');")});$("#button-deletechannels").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Delete Channels');")});
$("#button-open").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Open Mask');")});$("#button-close").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Close Mask');")});$("#button-duplicate").click(function(){a.evalScript("$._ext_RetouchingToolkit.runScript('Duplicate Merged');")});$("#button-resizeweb").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Resize for Web');")});$("#button-resizefacebook").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Resize for Facebook');")});
$("#button-resizeinstagram").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Resize for Instagram');")});$("#button-globalsharpen").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Global Sharpen');")});$("#button-sharpendetails").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Sharpen Details');")});$("#button-addnoise").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Add Noise');")});$("#button-cleandocument").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Clean Document');")});
$("#button-setcopyright").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Set Copyright');")});$("#button-saveforweb").click(function(){a.evalScript("$._ext_RetouchingToolkit.runAction('Save for Web');")});$("#button-savecustom1").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionNoError('Save Custom1');")});$("#button-savecustom2").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionNoError('Save Custom2');")});$("#button-savecustom3").click(function(){a.evalScript("$._ext_RetouchingToolkit.runActionNoError('Save Custom3');")})})()})();


var web = function (){
	var cs = new CSInterface();
    cs.evalScript("openurl()");
}

var p01 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + 'AUTO.jsxbin")')
}

var p02 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + 'LOGO.jsxbin")')
}

var lab03 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '3.jsxbin")')
}

var lab04 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '4.jsxbin")')
}

var lab05 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '5.jsxbin")')
}

var lab06 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '6.jsxbin")')
}

var lab07 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '7.jsxbin")')
}

var lab08 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '8.jsxbin")')
}

var lab09 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '9.jsxbin")')
}

var lab10 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '10.jsxbin")')
}

var lab11 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '11.jsxbin")')
}

var lab12 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '12.jsxbin")')
}

var lab13 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '13.jsxbin")')
}

var lab14 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '14.jsxbin")')
}

var lab15 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '15.jsxbin")')
}

var lab16 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '16.jsxbin")')
}

var lab17 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '17.jsxbin")')
}

var lab18 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '18.jsxbin")')
}

var lab19 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '19.jsxbin")')
}

var lab20 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '20.jsxbin")')
}

var lab21 = function () {
	var a = new CSInterface;
    var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    a.evalScript('$.evalFile("' + b + '21.jsxbin")')
}





(function () {
    'use strict';
    $(function () {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: "content"
        });
    });

    $(function () {
        $(document).tooltip({
            position: {
                my: "center top+2"
                , at: "center bottom"
            }
        });
    });

    $(document).ready(
        function () {
            $("html").niceScroll({
                zindex: 9999,
                scrollspeed: 300
            });
        }
    );
    
    var active = $( ".selector" ).accordion( "option", "active" );
    active.css("zindex", "auto");



    var csInterface = new CSInterface();

    // Reloads extension panel
    function reloadPanel() {
        location.reload();
    }
   // logo ref
    $(document).ready(function () {
        $("#i_logo").on("click", function (a) {
            a.preventDefault();
            (new CSInterface).openURLInDefaultBrowser("https://creativemarket.com/Willa-Willa")
        })
    });    
   //update 
    $(document).ready(function () {
        $("#updatePanel").on("click", function (a) {
            a.preventDefault();
            (new CSInterface).openURLInDefaultBrowser("https://creativemarket.com/Willa-Willa")
        })
    });


    var extensionRoot = csInterface.getSystemPath(SystemPath.EXTENSION) + "/jsx/includes/";


    function init() {

        
        themeManager.init();

        $("#btn_reload").click(reloadPanel);
        
        //tools
        $("#btn_dodge").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Tools/Dodge.jsx")');
        });
        $("#btn_burn").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Tools/Burn.jsx")');
        });
        $("#btn_stamp").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Tools/Stamp.jsx")');
        });
        $("#btn_mixbrush").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Tools/MixBrush.jsx")');
        });
        $("#btn_spotheal").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Tools/SpotHeal.jsx")');
        });
        $("#btn_heal").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Tools/Heal.jsx")');
        });
        $("#btn_patch").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Tools/Patch.jsx")');
        });
        $("#btn_colorSampler").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Tools/ColorSampler.jsx")');
        });

        //frequncy separation
        $("#btn_retouch_2B").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/retouch_2B.jsx")');
        });

        $("#btn_retouch_3B").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/retouch_3B.jsx")');
        });

        $("#btn_highlights_recovery").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'HDR/HDR_HighlightsRecovery.jsx")');
        });

        $("#btn_shadows_recovery").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'HDR/HDR_ShadowsRecovery.jsx")');
        });
        $("#btn_HDR").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'HDR/HDR_HDR.jsx")');
        });
        $("#btn_boost_details").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'HDR/HDR_BoostDetails.jsx")');
        });
        $("#btn_clarity").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'HDR/HDR_Clarity.jsx")');
        });
        $("#btn_detail_extractor").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'HDR/HDR_DetailExtractorfor.jsx")');
        });
        $("#btn_lights_detailed").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'HDR/HDR_LightsDetailed.jsx")');
        });
        $("#btn_darks_detiled").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'HDR/HDR_DarksDetailed.jsx")');
        });
        $("#btn_make_it_glow").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'HDR/HDR_Make_It_Glow.jsx")');
        });
        $("#btn_make_it_glow_2").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'HDR/HDR_Make_It_Glowv2.jsx")');
        });
        $("#btn_Retouch_2L_GB").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/Retouch_Retouch_2L_GB.jsx")');
        });
        $("#btn_Retouch_2L_SB").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/Retouch_Retouch_2L_SB.jsx")');
        });
        $("#btn_Retouch_3L_GB").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/Retouch_Retouch_3L_GB.jsx")');
        });
        $("#btn_Retouch_3L_SB").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/Retouch_Retouch_3L_SB.jsx")');
        });
        $("#btn_FastRetouch").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/Retouch_FastRetouch.jsx")');
        });
        $("#btn_HairRetouch").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/Retouch_HairRetouch.jsx")');
        });
        $("#btn_UniformitySkintone").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/Retouch_UniformitySkintone.jsx")');
        });
        $("#btn_BurnDodgeLayers").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/Retouch_Burn_DodgeLayers.jsx")');
        });
        $("#btn_AddTexture").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Retouch/Retouch_AddTexture.jsx")');
        });

        //color
        $("#btn_A1").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_A1.jsx")');
        });
        $("#btn_A2").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_A2.jsx")');
        });
        $("#btn_A3").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_A3.jsx")');
        });
        $("#btn_A4").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_A4.jsx")');
        });
        $("#btn_A5").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_A5.jsx")');
        });
        $("#btn_A6").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_A6.jsx")');
        });
        $("#btn_A7").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_A7.jsx")');
        });
        $("#btn_A8").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_A8.jsx")');
        });
        $("#btn_A9").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_A9.jsx")');
        });
        $("#btn_A10").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_A10.jsx")');
        });
        
        $("#btn_C1").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_C1.jsx")');
        });
        $("#btn_C2").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_C2.jsx")');
        });
        $("#btn_C3").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_C3.jsx")');
        });
        $("#btn_C4").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_C4.jsx")');
        });
        $("#btn_C5").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_C5.jsx")');
        });
        $("#btn_C6").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_C6.jsx")');
        });
        $("#btn_C7").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_C7.jsx")');
        });
        $("#btn_C8").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_C8.jsx")');
        });
        $("#btn_C9").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_C9.jsx")');
        });
        
        $("#btn_E1").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_E1.jsx")');
        });
        $("#btn_E2").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_E2.jsx")');
        });
        $("#btn_E3").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_E3.jsx")');
        });
        $("#btn_E4").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_E4.jsx")');
        });
        $("#btn_E5").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_E5.jsx")');
        });
        $("#btn_E6").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_E6.jsx")');
        });
        $("#btn_E7").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_E7.jsx")');
        });
        
        $("#btn_F1").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_F1.jsx")');
        });
        $("#btn_F2").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_F2.jsx")');
        });
        $("#btn_F3").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_F3.jsx")');
        });
        
        $("#btn_G1").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_G1.jsx")');
        });
        $("#btn_G2").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_G2.jsx")');
        });
        $("#btn_G3").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_G2.jsx")');
        });
        
        $("#btn_HB1").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_HB1.jsx")');
        });
        $("#btn_HB2").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_HB2.jsx")');
        });
        
        $("#btn_KK1").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_KK1.jsx")');
        });
        $("#btn_KK2").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_KK2.jsx")');
        });
        $("#btn_M3").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_M3.jsx")');
        });
        $("#btn_M4").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_M4.jsx")');
        });
        $("#btn_M5").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_M5.jsx")');
        });
        $("#btn_M6").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_M6.jsx")');
        });
        $("#btn_BW").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'VSCO/color_BW.jsx")');
        });
        
        
        $("#btn_optima").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_optima.jsx")');
        });
        $("#btn_Agfa_Portrait").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Agfa_Portrait.jsx")');
        });
        $("#btn_Teal_Orange").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Teal_Orange.jsx")');
        });
        $("#btn_Agfa_Ultra_50").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Agfa_Ultra_50.jsx")');
        });
        $("#btn_Kodak_Portra").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Kodak_Portra.jsx")');
        });
        $("#btn_Kodak_Ektachrome").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Kodak_Ektachrome.jsx")');
        });
        $("#btn_Kodak_Elite").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Kodak_Elite.jsx")');
        });
        $("#btn_Kodak_Ektar").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Kodak_Ektar.jsx")');
        });
        $("#btn_Fuji_t64").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Fuji_t64.jsx")');
        });
        $("#btn_Fuji_Sensia").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Fuji_Sensia.jsx")');
        });
        $("#btn_Teal_Orange_2").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Teal_Orange_2.jsx")');
        });
        $("#btn_Agfa_RSX50").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Agfa_RSX50.jsx")');
        });
        $("#btn_Agfa_Ultra_100").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Agfa_Ultra_100.jsx")');
        });
        $("#btn_Dual_Tone").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Dual_Tone.jsx")');
        });
        $("#btn_Portrait").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Looks/color_Portrait.jsx")');
        });






       


       



        //Instagram

        $("#btn_Lark").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Lark.jsx")');
        });
        $("#btn_Juno").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Juno.jsx")');
        });
        $("#btn_Aden").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Aden.jsx")');
        });
        $("#btn_Inkwell").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Inkwell.jsx")');
        });
        $("#btn_Hefe").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Hefe.jsx")');
        });
        $("#btn_Amaro").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Amaro.jsx")');
        });
        $("#btn_Kelvin").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Kelvin.jsx")');
        });
        $("#btn_Nashville").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Nashville.jsx")');
        });
        $("#btn_Reyes").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Reyes.jsx")');
        });
        $("#btn_Toaster").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Toaster.jsx")');
        });
        $("#btn_Walden").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Walden.jsx")');
        });
        $("#btn_XPro").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_XPro.jsx")');
        });
        $("#btn_Brannan").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Brannan.jsx")');
        });
        $("#btn_Earlybird").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Earlybird.jsx")');
        });
        $("#btn_Moon").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Moon.jsx")');
        });
        $("#btn_Hudson").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Hudson.jsx")');
        });
        $("#btn_Clarendon").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Clarendon.jsx")');
        });
        $("#btn_Gingham").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Gingham.jsx")');
        });
        $("#btn_LoFi").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_LoFi.jsx")');
        });
        $("#btn_Ludwig").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Ludwig.jsx")');
        });
        $("#btn_Crema").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Crema.jsx")');
        });     
        $("#btn_Valencia").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Valencia.jsx")');
        });
        $("#btn_Filmic").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Filmic.jsx")');
        });
        $("#btn_Charmes").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Charmes.jsx")');
        });
        $("#btn_Slumber").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Slumber.jsx")');
        });
        $("#btn_Perpetua").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Perpetua.jsx")');
        });
        $("#btn_Mayfair").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Mayfair.jsx")');
        });
        $("#btn_Rise").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Rise.jsx")');
        });
        $("#btn_Sierra").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Sierra.jsx")');
        });

        $("#btn_Willow").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Willow.jsx")');
        });

        $("#btn_Sinson").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Sinson.jsx")');
        });
        $("#btn_Vesper").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Vesper.jsx")');
        });
        $("#btn_1977").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_1977.jsx")');
        });
        $("#btn_Maven").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Maven.jsx")');
        });
        $("#btn_Ginza").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Ginza.jsx")');
        });
        $("#btn_Skyline").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Ginza.jsx")');
        });
        $("#btn_Dogpatch").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Dogpatch.jsx")');
        });
        $("#btn_Brooklyn").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Brooklyn.jsx")');
        });
        $("#btn_Helena").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Helena.jsx")');
        });
        $("#btn_Ashby").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Ashby.jsx")');
        });
        $("#btn_Sutro").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Instagram/color_Sutro.jsx")');
        });

        //Sharpness
        $("#btn_Resize").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Sharpness/Sharpness_Resize.jsx")');
        });
        $("#btn_Sharpness").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Sharpness/Sharpness_Sharpness.jsx")');
        });
        $("#btn_SharpnessforLessResolution").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Sharpness/Sharpness_SharpnessforLessResolution.jsx")');
        });
        $("#btn_SharpnessforPortrait").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Sharpness/Sharpness_SharpnessforPortrait.jsx")');
        });
        $("#btn_SharpnessforPortraitforLessResolution").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Sharpness/Sharpness_SharpnessforPortraitforLessResolution.jsx")');
        });

        //Saturatiom
        $("#btn_DefaultDesaturation").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Saturation/Saturatiom_DefaultDesaturation.jsx")');
        });
        $("#btn_DefaultSaturation").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Saturation/Saturatiom_DefaultSaturation.jsx")');
        });
        $("#btn_Desaturateheavilysaturated").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Saturation/Saturatiom_Desaturateheavilysaturated.jsx")');
        });
        $("#btn_Saturateslightlysaturated").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Saturation/Saturatiom_Saturateslightlysaturated.jsx")');
        });
        $("#btn_Saturationpainting").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Saturation/Saturatiom_Saturationpainting.jsx")');
        });
        $("#btn_SelectiveDesaturation").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Saturation/Saturatiom_SelectiveDesaturation.jsx")');
        });
        $("#btn_SelectiveSaturation").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Saturation/Saturatiom_SelectiveSaturation.jsx")');
        });
        
        
        $("#btn_UniformitySkintone").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Color/color_UniformitySkintone.jsx")');
        });
        $("#btn_WhiteBalance").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Color/color_WhiteBalance.jsx")');
        });
        $("#btn_Vintage").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Color/color_Vintage.jsx")');
        });
        $("#btn_NaturalColor").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Color/color_NaturalColor.jsx")');
        });
        $("#btn_Red_Cyan").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Color/color_Red_Cyan.jsx")');
        });
        $("#btn_GradingMap").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Color/color_GradingMap.jsx")');
        });
        $("#btn_ShadowsColor").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Color/color_ShadowsColor.jsx")');
        });
        $("#btn_HighlightsColor").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Color/color_HighlightsColor.jsx")');
        });
        $("#btn_ColorClone").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Color/color_ColorClone.jsx")');
        });
        $("#btn_ExclusionGrading").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Color/color_ExclusionGrading.jsx")');
        });
        $("#btn_MidTonesToneContract").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Contrast/Contrast_MidTonesToneContract.jsx")');
        });
        $("#btn_MidTonesColorContract").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Contrast/Contrast_MidTonesColorContract.jsx")');
        });
        $("#btn_MatteEfect").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Contrast/Contrast_MatteEfect.jsx")');
        });
        $("#btn_AutoHistogram").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Contrast/Contrast_AutoHistogram.jsx")');
        });
        $("#btn_MoreContrast").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Contrast/Contrast_MoreContrast.jsx")');
        });
        $("#btn_MakeMoreBrighter").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Contrast/Contrast_MakeMoreBrighter.jsx")');
        });
        $("#btn_FilmicContrast").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Contrast/Contrast_FilmicContrast.jsx")');
        });
        $("#btn_Vignette").click(function () {
            csInterface.evalScript('evalFile("' + extensionRoot + 'Contrast/Contrast_Vignette.jsx")');
        });
    }
    init();

}());



(function () {
    function e() {
        var b = new CSEvent("com.adobe.PhotoshopPersistent", "APPLICATION");
        b.extensionId = "PS Film Look";
        a.dispatchEvent(b)
    }

    var a = new CSInterface;
    (function () {
        themeManager.init();
        e();
        

        $("#fanpage").click(function () {
            var a = new CSInterface, c = "/", d = -1 < window.navigator.platform.toLowerCase().indexOf("win");
            d && (c = a.getSystemPath(SystemPath.COMMON_FILES).substring(0, 3));
            a = "/usr/bin/open";
            d && (a = c + "Windows/explorer.exe");
            window.cep.process.createProcess(a, "https://www.facebook.com/pspanel")
        });

$("#Bejing1").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Bejing1.js")') });
$("#Bejing10").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Bejing10.js")') });
$("#Bejing2").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Bejing2.js")') });
$("#Bejing3").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Bejing3.js")') });
$("#Bejing4").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Bejing4.js")') });
$("#Bejing5").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Bejing5.js")') });
$("#Bejing6").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Bejing6.js")') });
$("#Bejing7").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Bejing7.js")') });
$("#Bejing8").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Bejing8.js")') });
$("#Bejing9").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Bejing9.js")') });
$("#Budapest1").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Budapest1.js")') });
$("#Budapest10").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Budapest10.js")') });
$("#Budapest2").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Budapest2.js")') });
$("#Budapest3").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Budapest3.js")') });
$("#Budapest4").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Budapest4.js")') });
$("#Budapest5").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Budapest5.js")') });
$("#Budapest6").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Budapest6.js")') });
$("#Budapest7").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Budapest7.js")') });
$("#Budapest8").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Budapest8.js")') });
$("#Budapest9").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Budapest9.js")') });
$("#Jeju1").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Jeju1.js")') });
$("#Jeju10").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Jeju10.js")') });
$("#Jeju2").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Jeju2.js")') });
$("#Jeju3").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Jeju3.js")') });
$("#Jeju4").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Jeju4.js")') });
$("#Jeju5").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Jeju5.js")') });
$("#Jeju6").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Jeju6.js")') });
$("#Jeju7").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Jeju7.js")') });
$("#Jeju8").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Jeju8.js")') });
$("#Jeju9").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Jeju9.js")') });
$("#London1").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'London1.js")') });
$("#London10").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'London10.js")') });
$("#London2").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'London2.js")') });
$("#London3").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'London3.js")') });
$("#London4").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'London4.js")') });
$("#London5").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'London5.js")') });
$("#London6").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'London6.js")') });
$("#London7").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'London7.js")') });
$("#London8").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'London8.js")') });
$("#London9").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'London9.js")') });
$("#Paris1").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Paris1.js")') });
$("#Paris10").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Paris10.js")') });
$("#Paris2").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Paris2.js")') });
$("#Paris3").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Paris3.js")') });
$("#Paris4").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Paris4.js")') });
$("#Paris5").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Paris5.js")') });
$("#Paris6").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Paris6.js")') });
$("#Paris7").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Paris7.js")') });
$("#Paris8").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Paris8.js")') });
$("#Paris9").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Paris9.js")') });
$("#Portland1").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Portland1.js")') });
$("#Portland10").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Portland10.js")') });
$("#Portland2").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Portland2.js")') });
$("#Portland3").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Portland3.js")') });
$("#Portland4").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Portland4.js")') });
$("#Portland5").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Portland5.js")') });
$("#Portland6").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Portland6.js")') });
$("#Portland7").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Portland7.js")') });
$("#Portland8").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Portland8.js")') });
$("#Portland9").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Portland9.js")') });
$("#Tokyo1").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Tokyo1.js")') });
$("#Tokyo10").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Tokyo10.js")') });
$("#Tokyo2").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Tokyo2.js")') });
$("#Tokyo3").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Tokyo3.js")') });
$("#Tokyo4").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Tokyo4.js")') });
$("#Tokyo5").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Tokyo5.js")') });
$("#Tokyo6").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Tokyo6.js")') });
$("#Tokyo7").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Tokyo7.js")') });
$("#Tokyo8").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Tokyo8.js")') });
$("#Tokyo9").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Tokyo9.js")') });
$("#Wedding1").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Wedding1.js")') });
$("#Wedding10").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Wedding10.js")') });
$("#Wedding2").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Wedding2.js")') });
$("#Wedding3").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Wedding3.js")') });
$("#Wedding4").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Wedding4.js")') });
$("#Wedding5").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Wedding5.js")') });
$("#Wedding6").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Wedding6.js")') });
$("#Wedding7").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Wedding7.js")') });
$("#Wedding8").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Wedding8.js")') });
$("#Wedding9").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Wedding9.js")') });

        

        $("#YellowHighlights").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'YellowHighlights.jsx")') });
        $("#YellowShadows").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'YellowShadows.jsx")') });
        $("#BlueHighlights").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'BlueHighlights.jsx")') });
        $("#BlueShadows").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'BlueShadows.jsx")') });
        $("#RedHighlights").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'RedHighlights.jsx")') });
        $("#RedShadows").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'RedShadows.jsx")') });
        $("#GreenHighlights").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'GreenHighlights.jsx")') });
        $("#GreenShadows").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'GreenShadows.jsx")') });
        $("#PurpleHighlights").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'PurpleHighlights.jsx")') });
        $("#PurpleShadows").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'PurpleShadows.jsx")') });
        $("#OrangeShadow").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'OrangeShadow.jsx")') });
        $("#CreamHighlights").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'CreamHighlights.jsx")') });
        $("#Vignette").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Vignette.jsx")') });
        $("#ShadowRecovery").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'ShadowRecovery.jsx")') });
        $("#HighlightRecovery").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'HighlightRecovery.jsx")') });
        $("#sharpen").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'sharpen.jsx")') });
        $("#grain").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'grain.jsx")') });
        $("#Fade").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'Fade.jsx")') });
        $("#ColorFixer").click(function () { var b = a.getSystemPath(SystemPath.EXTENSION) + "/jsx/"; a.evalScript('$.evalFile("' + b + 'ColorFixer.jsx")') });




        
    })()
})();
