(function(){if("FLPR"!=(new CSInterface).hostEnvironment.appName){var a=new CSInterface,b=a.getSystemPath(SystemPath.EXTENSION)+"/jsx/";a.evalScript('$._ext.evalFiles("'+b+'")')}})();

function onLoaded() {
    var csInterface = new CSInterface();
    updateThemeWithAppSkinInfo(csInterface.hostEnvironment.appSkinInfo);
    // Update the color of the panel when the theme color of the product changed.
    csInterface.addEventListener(CSInterface.THEME_COLOR_CHANGED_EVENT, onAppThemeColorChanged);


//    var lumButtonNames = ['CreateLuminosityMasks', 'DeleteLuminosityMasks', 'CreateBrightMasks', 'DeleteBrightMasks', 'CreateDarkMasks', 'DeleteDarkMasks', 'CreateMidtoneMasks', 'DeleteMidtoneMasks'];
//    lumButtonNames.forEach(function(name)
//        {
//            $("#"+name).click(function()
//                {
//                    csInterface.evalScript('$._ext.' + name.substr(0, 1).toLowerCase() + name.substring(1) +'()', function(msg) 
//                    {
//                        console.log('$._ext.' + name.substr(0, 1).toLowerCase() + name.substring(1) +'() : ' + msg);
//                    });
//                });
//        });

    var buttonNames = [	
{id: "button01", script: "01.js"}, 
{id: "button02", script: "02.js"}, 
{id: "button03", script: "03.js"}, 
{id: "button04", script: "04.js"}, 
{id: "button05", script: "05.js"}, 
{id: "button06", script: "06.js"}, 
{id: "button07", script: "07.js"}, 
{id: "button08", script: "08.js"}, 
{id: "button09", script: "09.js"}, 
{id: "button10", script: "10.js"}, 
{id: "buttonA1", script: "A1.js"}, 
{id: "buttonA2", script: "A2.js"}, 
{id: "buttonA3", script: "A3.js"}, 
{id: "buttonA4", script: "A4.js"}, 
{id: "buttonA5", script: "A5.js"}, 
{id: "buttonA6", script: "A6.js"}, 
{id: "buttonA7", script: "A7.js"}, 
{id: "buttonA8", script: "A8.js"}, 
{id: "buttonA9", script: "A9.js"}, 
{id: "buttonA10", script: "A10.js"}, 
{id: "buttonACG", script: "ACG.js"}, 
{id: "buttonB1", script: "B1.js"}, 
{id: "buttonB2", script: "B2.js"}, 
{id: "buttonB3", script: "B3.js"}, 
{id: "buttonB4", script: "B4.js"}, 
{id: "buttonB5", script: "B5.js"}, 
{id: "buttonB6", script: "B6.js"}, 
{id: "buttonC1", script: "C1.js"}, 
{id: "buttonC2", script: "C2.js"}, 
{id: "buttonC3", script: "C3.js"}, 
{id: "buttonC4", script: "C4.js"}, 
{id: "buttonC5", script: "C5.js"}, 
{id: "buttonC6", script: "C6.js"}, 
{id: "buttonC7", script: "C7.js"}, 
{id: "buttonC8", script: "C8.js"}, 
{id: "buttonC9", script: "C9.js"}, 
{id: "buttonE1", script: "E1.js"}, 
{id: "buttonE2", script: "E2.js"}, 
{id: "buttonE3", script: "E3.js"}, 
{id: "buttonE4", script: "E4.js"}, 
{id: "buttonE5", script: "E5.js"}, 
{id: "buttonE6", script: "E6.js"}, 
{id: "buttonE7", script: "E7.js"}, 
{id: "buttonE8", script: "E8.js"}, 
{id: "buttonF1", script: "F1.js"}, 
{id: "buttonF2", script: "F2.js"}, 
{id: "buttonF3", script: "F3.js"}, 
{id: "buttonG1", script: "G1.js"}, 
{id: "buttonG2", script: "G2.js"}, 
{id: "buttonG3", script: "G3.js"}, 
{id: "buttonH1", script: "H1.js"}, 
{id: "buttonH2", script: "H2.js"}, 
{id: "buttonH3", script: "H3.js"}, 
{id: "buttonH4", script: "H4.js"}, 
{id: "buttonH5", script: "H5.js"}, 
{id: "buttonH6", script: "H6.js"}, 
{id: "buttonHB1", script: "HB1.js"}, 
{id: "buttonHB2", script: "HB2.js"}, 
{id: "buttonJ1", script: "J1.js"}, 
{id: "buttonJ2", script: "J2.js"}, 
{id: "buttonJ3", script: "J3.js"}, 
{id: "buttonJ4", script: "J4.js"}, 
{id: "buttonJ5", script: "J5.js"}, 
{id: "buttonJ6", script: "J6.js"}, 
{id: "buttonK1", script: "K1.js"}, 
{id: "buttonk2", script: "k2.js"}, 
{id: "buttonk3", script: "k3.js"}, 
{id: "buttonKK1", script: "KK1.js"}, 
{id: "buttonKK2", script: "KK2.js"}, 
{id: "buttonLV1", script: "LV1.js"}, 
{id: "buttonLV2", script: "LV2.js"}, 
{id: "buttonLV3", script: "LV3.js"}, 
{id: "buttonM1", script: "M1.js"}, 
{id: "buttonM2", script: "M2.js"}, 
{id: "buttonM3", script: "M3.js"}, 
{id: "buttonM4", script: "M4.js"}, 
{id: "buttonM5", script: "M5.js"}, 
{id: "buttonM6", script: "M6.js"}, 
{id: "buttonN1", script: "N1.js"}, 
{id: "buttonN2", script: "N2.js"}, 
{id: "buttonN3", script: "N3.js"}, 
{id: "buttonP1", script: "P1.js"}, 
{id: "buttonP2", script: "P2.js"}, 
{id: "buttonP3", script: "P3.js"}, 
{id: "buttonP4", script: "P4.js"}, 
{id: "buttonP5", script: "P5.js"}, 
{id: "buttonP6", script: "P6.js"}, 
{id: "buttonP7", script: "P7.js"}, 
{id: "buttonP8", script: "P8.js"}, 
{id: "buttonP9", script: "P9.js"}, 
{id: "buttonQ1", script: "Q1.js"}, 
{id: "buttonQ2", script: "Q2.js"}, 
{id: "buttonQ3", script: "Q3.js"}, 
{id: "buttonQ4", script: "Q4.js"}, 
{id: "buttonQ5", script: "Q5.js"}, 
{id: "buttonQ6", script: "Q6.js"}, 
{id: "buttonQ7", script: "Q7.js"}, 
{id: "buttonQ8", script: "Q8.js"}, 
{id: "buttonQ9", script: "Q9.js"}, 
{id: "buttonQ10", script: "Q10.js"}, 
{id: "buttonS1", script: "S1.js"}, 
{id: "buttonS2", script: "S2.js"}, 
{id: "buttonS3", script: "S3.js"}, 
{id: "buttonS4", script: "S4.js"}, 
{id: "buttonS5", script: "S5.js"}, 
{id: "buttonS6", script: "S6.js"}, 
{id: "buttonSE1", script: "SE1.js"}, 
{id: "buttonSE2", script: "SE2.js"}, 
{id: "buttonSE3", script: "SE3.js"}, 
{id: "buttonT1", script: "T1.js"}, 
{id: "buttonT2", script: "T2.js"}, 
{id: "buttonT3", script: "T3.js"}, 
{id: "buttonX1", script: "X1.js"}, 
{id: "buttonX2", script: "X2.js"}, 
{id: "buttonX3", script: "X3.js"}, 
{id: "buttonX4", script: "X4.js"}, 
{id: "buttonX5", script: "X5.js"}, 
{id: "buttonX6", script: "X6.js"},
{id: "fade", script: "Fade.jsx"},
{id: "vignettte", script: "Vignette.jsx"}, 
{id: "highlight-recovery", script: "HighlightRecovery.jsx"},
{id: "shadow-recovery", script: "ShadowRecovery.jsx"},
{id: "sharpen", script: "sharpen.jsx"},
{id: "grain", script: "grain.jsx"},

/************************/

{id: "purple", script: "PurpleShadows.jsx"},
{id: "red", script: "RedShadows.jsx"},
{id: "orange", script: "OrangeShadow.jsx"},
{id: "yellow", script: "YellowShadows.jsx"},
{id: "green", script: "GreenShadows.jsx"},
{id: "blue", script: "BlueShadows.jsx"},
{id: "purple-h", script: "PurpleHighlights.jsx"},
{id: "red-h", script: "RedHighlights.jsx"},
{id: "cream-h", script: "CreamHighlights.jsx"},
{id: "yellow-h", script: "YellowHighlights.jsx"},
{id: "green-h", script: "GreenHighlights.jsx"},
{id: "blue-h", script: "BlueHighlights.jsx"}



/************************/

];

    buttonNames.forEach(function(button) 
        {
            $("#" + button.id).click(function()
            {
                loadJSX(button.script);
            });
        });
    
    $("#aboutlink").click(function()
        {
            csInterface.openURLInDefaultBrowser('https://www.facebook.com/minhtuan.photograph');
        });
}



/**
 * Update the theme with the AppSkinInfo retrieved from the host product.
 */
function updateThemeWithAppSkinInfo(appSkinInfo) {
	
    //Update the background color of the panel
    var panelBackgroundColor = appSkinInfo.panelBackgroundColor.color;
    document.body.bgColor = toHex(panelBackgroundColor);
        
    var styleId = "ppstyle";
    
    var csInterface = new CSInterface();
	var appName = csInterface.hostEnvironment.appName;
    
    if(appName == "PHXS"){
	    addRule(styleId, "button, select, input[type=button], input[type=submit]", "border-radius:3px;");
	}
	if(appName == "PHXS" || appName == "PPRO" || appName == "PRLD") {
		////////////////////////////////////////////////////////////////////////////////////////////////
		// NOTE: Below theme related code are only suitable for Photoshop.                            //
		// If you want to achieve same effect on other products please make your own changes here.    //
		////////////////////////////////////////////////////////////////////////////////////////////////
		
	    
	    var gradientBg = "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, 40) + " , " + toHex(panelBackgroundColor, 10) + ");";
	    var gradientDisabledBg = "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, 15) + " , " + toHex(panelBackgroundColor, 5) + ");";
	    var boxShadow = "-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);";
	    var boxActiveShadow = "-webkit-box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.6);";
	    
	    var isPanelThemeLight = panelBackgroundColor.red > 127;
        var buttonBg;
        var buttonHoverBg;
	    var fontColor, disabledFontColor;
	    var borderColor;
	    var inputBackgroundColor;
	    var gradientHighlightBg;

	    if(isPanelThemeLight) {
            buttonBg = "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, -40) + " , " + toHex(panelBackgroundColor, -70) + ");";
            buttonHoverBg = "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, -10) + " , " + toHex(panelBackgroundColor, -40) + ");";
	    	fontColor = "#000000;";
            titleColor = toHex(panelBackgroundColor, -140) ;
            subtitleColor =toHex(panelBackgroundColor, -80) ;
	    	disabledFontColor = "color:" + toHex(panelBackgroundColor, -70) + ";";
	    	borderColor = "border-color: " + toHex(panelBackgroundColor, -90) + ";";
	    	inputBackgroundColor = toHex(panelBackgroundColor, 54) + ";";
	    	gradientHighlightBg = "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, -40) + " , " + toHex(panelBackgroundColor,-50) + ");";
	    } else {
	    	fontColor = "#282828;";
            titleColor = toHex(panelBackgroundColor, 150) ;
            subtitleColor =toHex(panelBackgroundColor, 60) ;
            buttonBg = "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, 150) + " , " + toHex(panelBackgroundColor, 80) + ");";
            buttonHoverBg = "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, 200) + " , " + toHex(panelBackgroundColor, 120) + ");";
	    	disabledFontColor = "color:" + toHex(panelBackgroundColor, 100) + ";";
	    	borderColor = "border-color: " + toHex(panelBackgroundColor, -45) + ";";
	    	inputBackgroundColor = toHex(panelBackgroundColor, -20) + ";";
	    	gradientHighlightBg = "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, -20) + " , " + toHex(panelBackgroundColor, -30) + ");";
	    }
	    
	
	    //Update the default text style with pp values
	    
	    addRule(styleId, "h1", "color:" + titleColor + "; ");
	    addRule(styleId, "h3", "font-size:" + 16 + "px" + "; color:" + subtitleColor + "; ");
	    addRule(styleId, ".default", "font-size:" + appSkinInfo.baseFontSize + "px" + "; color:" + fontColor + "; background-color:" + toHex(panelBackgroundColor) + ";");
	    addRule(styleId, "button, select, input[type=text], input[type=button], input[type=submit]", borderColor);    
	    addRule(styleId, "select, input[type=button], input[type=submit]", gradientBg);    
	    addRule(styleId, "button", buttonBg);    
	    addRule(styleId, "button:hover", buttonHoverBg);    
	    addRule(styleId, "button, select, input[type=button], input[type=submit]", boxShadow);
	    addRule(styleId, "button:enabled:active, input[type=button]:enabled:active, input[type=submit]:enabled:active", gradientHighlightBg);
	    addRule(styleId, "button:enabled:active, input[type=button]:enabled:active, input[type=submit]:enabled:active", boxActiveShadow);
	    addRule(styleId, "[disabled]", gradientDisabledBg);
	    addRule(styleId, "[disabled]", disabledFontColor);
	    addRule(styleId, "input[type=text]", "padding:1px 3px;");
	    addRule(styleId, "input[type=text]", "background-color: " + inputBackgroundColor) + ";";
	    addRule(styleId, "input[type=text]:focus", "background-color: #ffffff;");
	    addRule(styleId, "input[type=text]:focus", "color: #000000;");
	    
	} else {
		// For AI, ID and FL use old implementation	
		addRule(styleId, ".default", "font-size:" + appSkinInfo.baseFontSize + "px" + "; color:" + reverseColor(panelBackgroundColor) + "; background-color:" + toHex(panelBackgroundColor, 20));
	    addRule(styleId, "button", "border-color: " + toHex(panelBgColor, -50));
	}
}

function addRule(stylesheetId, selector, rule) {
    var stylesheet = document.getElementById(stylesheetId);
    
    if (stylesheet) {
        stylesheet = stylesheet.sheet;
           if( stylesheet.addRule ){
               stylesheet.addRule(selector, rule);
           } else if( stylesheet.insertRule ){
               stylesheet.insertRule(selector + ' { ' + rule + ' }', stylesheet.cssRules.length);
           }
    }
}


function reverseColor(color, delta) {
    return toHex({red:Math.abs(255-color.red), green:Math.abs(255-color.green), blue:Math.abs(255-color.blue)}, delta);
}

/**
 * Convert the Color object to string in hexadecimal format;
 */
function toHex(color, delta) {
    function computeValue(value, delta) {
        var computedValue = !isNaN(delta) ? value + delta : value;
        if (computedValue < 0) {
            computedValue = 0;
        } else if (computedValue > 255) {
            computedValue = 255;
        }

        computedValue = computedValue.toString(16);
        return computedValue.length == 1 ? "0" + computedValue : computedValue;
    }

    var hex = "";
    if (color) {
        with (color) {
             hex = computeValue(red, delta) + computeValue(green, delta) + computeValue(blue, delta);
        };
    }
    return "#" + hex;
}

function onAppThemeColorChanged(event) {
    // Should get a latest HostEnvironment object from application.
    var skinInfo = JSON.parse(window.__adobe_cep__.getHostEnvironment()).appSkinInfo;
    // Gets the style information such as color info from the skinInfo, 
    // and redraw all UI controls of your extension according to the style info.
    updateThemeWithAppSkinInfo(skinInfo);
} 



    
/**
 * Load JSX file into the scripting context of the product. All the jsx files in 
 * folder [ExtensionRoot]/jsx will be loaded. 
 */
function loadJSX(filename) {
    var csInterface = new CSInterface();
    var extensionRoot = csInterface.getSystemPath(SystemPath.EXTENSION) + "/jsx/";
    console.log('$._ext.evalFile("' + extensionRoot + filename+'")')
    csInterface.evalScript('$._ext.evalFile("' + extensionRoot + filename+'")', function(msg) 
    {
        console.log(msg);
    });
}

function evalScript(script, callback) {
    new CSInterface().evalScript(script, callback);
}
