﻿#target photoshop
//
// 50Grey.jsx
//

//
// Generated Sun Mar 15 2015 23:10:55 GMT+0200
//

cTID = function(s) { return app.charIDToTypeID(s); };
sTID = function(s) { return app.stringIDToTypeID(s); };

//
//==================== 50Grey ==============
//
$._ext_50grey={
		run : function _50Grey() {
  // Make
  function step1(enabled, withDialog) {
    if (enabled != undefined && !enabled)
      return;
    var dialogMode = (withDialog ? DialogModes.ALL : DialogModes.NO);
    var desc1 = new ActionDescriptor();
    var ref1 = new ActionReference();
    ref1.putClass(cTID('Lyr '));
    desc1.putReference(cTID('null'), ref1);
    var desc2 = new ActionDescriptor();
//~     desc2.putString(cTID('Nm  '), "Dodge&Burn (%50 Grey)");
    desc2.putString(cTID('Nm  '), "Deepen & Lighten (50% Gray)");
    desc2.putEnumerated(cTID('Md  '), cTID('BlnM'), cTID('SftL'));
    desc2.putBoolean(cTID('FlNt'), true);
    desc1.putObject(cTID('Usng'), cTID('Lyr '), desc2);
    executeAction(cTID('Mk  '), desc1, dialogMode);
  };

  step1();      // Make
		},
};



//=========================================
//                    _50Grey.main
//=========================================
//

//_50Grey.main = function () {
//  _50Grey();
//};
//
//_50Grey.main();
//
//// EOF
//
//"50Grey.jsx"
//// EOF
